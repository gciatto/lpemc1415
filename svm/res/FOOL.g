grammar FOOL;

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/
 
 @header {
package it.unibo.gciatto.sdt.parsing;

import static it.unibo.gciatto.sdt.typing.Types.function;
import it.unibo.gciatto.sdt.adt.*;
import it.unibo.gciatto.sdt.adt.impl.*;
import it.unibo.gciatto.sdt.scoping.*;
import it.unibo.gciatto.sdt.scoping.impl.*;
import it.unibo.gciatto.sdt.typing.*;
import it.unibo.gciatto.sdt.typing.impl.*;
}

 @lexer::header {
package it.unibo.gciatto.sdt.parsing;
}

@members{
private IScope mScope = new Scope(0);

private boolean isMethodInvocation(ISymbolData symbol) {
	return symbol != null 
	&& symbol.getSymbolType() == SymbolType.FUNC 
	&& symbol.getDefinitionNode() instanceof IMethodNode;
}
}

dec	returns [List<IASTNode<?>> nodes]
	:	{ 
			$nodes = new ArrayList<IASTNode<?>>(); 
		}
		(VAR i=ID COLON t=type ASS e=exp SEMIC 
		{
			final ISymbolData data = mScope.pushSymbol($i.text, $t.type);
			VarNode vnode = new VarNode(data, $e.node);
			$nodes.add(vnode);
		}
	| 	FUN i=ID COLON t=type
		{
			ISymbolData data = mScope.pushSymbol($i.text, function($t.type), SymbolType.FUNC);
			FunNode fnode = new FunNode(data, null);
			$nodes.add(fnode);
			
			mScope.incLevel();
		}
             	LPAR 
		(argi=ID COLON argt=type
		{
			data = mScope.pushSymbol($argi.text, $argt.type);
			DeclarationNode decnode = new DeclarationNode(data);
			fnode.addParam(decnode);
		}
			(COMMA argi=ID COLON argt=type
			{
				data = mScope.pushSymbol($argi.text, $argt.type);
				decnode = new DeclarationNode(data);
				fnode.addParam(decnode);
			}
		    )*
		)? 
		RPAR
			(LET d=dec IN)? e=exp SEMIC
			{
				if (d != null) {
					for (final IASTNode<?> dec : $d.nodes) {
						fnode.addDeclaration(dec);
					}
				}
				fnode.setExpression($e.node);
				mScope.decLevel();
				d = null;
			}
		)*          
	;
	
cllist  returns [List<IClassNode> classes]
	: 	{
			$classes = new ArrayList<IClassNode>();
		}
		(CLASS id = ID
		{
			IClassNode cnode = new ClassNode($id.text);
			ISymbolData data = mScope.pushSymbol($id.text, cnode.getType(), SymbolType.CLASS);
			mScope.incLevel();
		}
			(EXTENDS superId = ID
			{
				cnode = new ClassNode(data.getSymbol(), Types.findDefinition($superId.text));
			}
			)? { $classes.add(cnode); }
				
				LPAR (i = ID COLON t = basic
				{
					data = mScope.pushSymbol($i.text, $t.type);
					IFieldNode fnode = new FieldNode(data);
					cnode.addField(fnode);
				}
				(COMMA i = ID COLON t = basic
				{
					data = mScope.pushSymbol($i.text, $t.type);
					fnode = new FieldNode(data);
					cnode.addField(fnode);
				}
				)* )? RPAR    
			              CLPAR
			                 (
			                 FUN i = ID COLON t = basic LPAR 
			                 {
			                 	data = mScope.pushSymbol($i.text, function($t.type), SymbolType.FUNC);
						IMethodNode mnode = new MethodNode(data, cnode);
						cnode.addMethod(mnode);
						
						mScope.incLevel();
			                 }
			                 	(argi = ID COLON argt = type 
			                 	{
			                 		data = mScope.pushSymbol($argi.text, $argt.type);
							DeclarationNode anode = new DeclarationNode(data);
							mnode.addParam(anode);
			                 	}
			                 		(COMMA argi = ID COLON argt = type
			                 		{
			                 			data = mScope.pushSymbol($argi.text, $argt.type);
								anode = new DeclarationNode(data);
								mnode.addParam(anode);
			                 		}
			                 		)* 
			                 	)? 
			                 RPAR
				            (LET
				            	(VAR vari = ID COLON vart = basic ASS varv = exp SEMIC
				            	{
				            		data = mScope.pushSymbol($vari.text, $vart.type);
							IVariableNode anode = new VarNode(data, $varv.node);
							mnode.addParam(anode);
				            	}
				            	)* IN )? e = exp SEMIC
				            	{
					            	mnode.setExpression((IASTNode<Object>) $e.node);
							mScope.decLevel();
				            	}
			        	 	)*                
			              CRPAR
			              {
					mScope.decLevel();
			              }
			              )*;

type    returns [IType type]
	:  	b = basic 
		{
			$type = $b.type; 
		}
	| 	a = arrow
		{
			$type = $a.type; 
		} 
        ;

basic   returns [IType type]
	: INT { $type = Types.INTEGER; }
        | BOOL { $type = Types.BOOLEAN; }  		      	
 	| id = ID { $type = Types.findType($id.text); }                        
 	;  
 	  
arrow 	returns [IFunctionType type]
	: { 
		final IFunctionType ft = new FunctionType();
		$type = ft;
	} 
	LPAR 
	(
		arg = type { ft.addParamType($arg.type); }
		(
			COMMA arg = type { ft.addParamType($arg.type); }
		)* 
	)? 
	RPAR ARROW 
		res = basic 
		{ 
			ft.setReturnType($res.type); 
		}
 	; 

prog	returns[IASTNode<?> node]
	:	
		{
			final LetInNode linode = new LetInNode(mScope, null, null, null);
			$node = new ProgramNode((IASTNode<Object>) linode);
		}
		(e = exp SEMIC
		{
			linode.setExpression($e.node);
		}
	| 	LET c=cllist d=dec IN e=exp SEMIC 
		{
			linode.addClasses($c.classes);
			linode.addDeclarations($d.nodes);
			linode.setExpression($e.node);
		})
	;

exp	returns[IASTNode<?> node]
	:	t1 = term 
		{	
			$node = $t1.node;
		}
		(
			PLUS t2 = term 
			{
				$node = new SumNode($node, $t2.node);
			}
		| 	MINUS t3 = term
			{
				$node = new SubNode($node, $t3.node);
			}
		| 	OR t4 = term
			{
				$node = new OrNode($node, $t4.node);
			}
		| 	XOR t5 = term
			{
				$node = new XorNode($node, $t5.node);
			}
		)*
		;

term	returns[IASTNode<?> node]
	:	f1 = fact 
		{
			$node = $f1.node;
		}
		(
			MULT f2 = fact 
			{
				$node = new MultNode($node, $f2.node);
			}
		| 	DIV f3 = fact 
			{
				$node = new DivNode($node, $f3.node);
			}
		| 	AND f4 = fact 
			{
				$node = new AndNode($node, $f4.node);
			}
		)*
	;

	
fact	returns[IASTNode<?> node]
	:	v1 = value 
		{
			$node = $v1.node;
		}
		(
			EQ v2 = value 
			{
				$node = new EqualsNode($node, $v2.node);
			}
		| 	GE v3 = value 
			{
				$node = new GreaterEqualsNode($node, $v3.node);
			}
		| 	LE v4 = value 
			{
				$node = new LowerEqualsNode($node, $v4.node);
			}
		| 	GT v5 = value 
			{
				$node = new GreaterNode($node, $v5.node);
			}
		| 	LW v6 = value 
			{
				$node = new LowerNode($node, $v6.node);
			}
		| 	NE v7 = value 
			{
				$node = new NotEqualsNode($node, $v7.node);
			}
		)*
	;
	
value	returns[IASTNode<?> node]
	: 	n = INTEGER 
		{
			$node = new IntegerNode(Integer.parseInt($n.text));
		}
	|	b = TRUE 
		{
			$node = new BooleanNode(true);
		}
	|	b = FALSE 
		{
			$node = new BooleanNode(false);
		}
	| 	IF c = exp THEN CLPAR et = exp CRPAR ELSE CLPAR ef = exp CRPAR 
		{
			$node = new TernaryNode($c.node, $et.node, $ef.node);
		}
	|	LPAR e = exp RPAR 
		{
			$node = $e.node;
		}
	|	NOT e = exp
		{
			$node = new NotNode($e.node);
		}
	|	PRINT LPAR e = exp RPAR
		{
			$node = new PrintNode($e.node);
		}
	/*| 	ID ( LPAR (expr (COMMA expr)* )? RPAR 
	| 	DOT ID LPAR (expr (COMMA expr)* )? RPAR )?	*/
	|	i=ID 
		{
			IdNode inode = new IdNode(mScope.peekSymbol($i.text), mScope.getLevel());
			$node = inode;
		}
			(LPAR 
			{
				final ISymbolData data = mScope.peekSymbol($i.text);
				CallNode cnode;
				if (isMethodInvocation(data)) {
					final IMethodNode def = (IMethodNode) data.getDefinitionNode();
					cnode = (CallNode) def.getCallNode(null, mScope.getLevel());
				} else {
					cnode = new CallNode(data, mScope.getLevel());
				}
				$node = cnode;
			}
				(arg=exp 
				{
					cnode.addParam($arg.node);
				}
					(COMMA arg=exp
					{
						cnode.addParam($arg.node);
					}
					)*
				)?
				RPAR
				{
					cnode.validate();
				}
			|	DOT i1 = ID LPAR 
				{
					//Dot
					final IClassNode def = Types.getDefinition(inode.getType());
					final IMethodNode met = def.getMethodByName($i1.text);
					final IMethodCallNode mcnode = met.getCallNode(inode, mScope.getLevel());
					$node = mcnode;
				}
					(arg = exp
					{
						mcnode.addParam((IASTNode<Object>) $arg.node);
					}
						(
							COMMA arg = exp
							{
								mcnode.addParam((IASTNode<Object>) $arg.node);
							}
						)* 
					{
						mcnode.validate();
					}
				)? RPAR
			)?
	| 	NULL
		{
			$node = new NullNode(mScope);
		}    
	| 	NEW id = ID LPAR 
		{
			final IConstructorCallNode cnode = Types.findDefinition($id.text).getConstructor().getCallNode(mScope.getLevel());
			$node = cnode;
		}
			(arg = exp
			{
				cnode.addParam((IASTNode<Object>) $arg.node);
			} 
				(COMMA arg = exp
				{
					cnode.addParam((IASTNode<Object>) $arg.node);
				}
				)* )? RPAR 
				{
					cnode.validate();
				}
	;
/*
* Lexer rules
*/

PLUS  	: '+' ;
MINUS   : '-' ;
MULT    : '*' ;
DIV 	: '/' ;
LPAR	: '(' ;
RPAR	: ')' ;
CLPAR	: '{' ;
CRPAR	: '}' ;
SEMIC 	: ';' ;
COLON   : ':' ; 
COMMA	: ',' ;
DOT	: '.' ;
XOR	: 'xor';
OR	: 'or' | '||';
AND	: 'and' | '&&';
NOT	: 'not' ;
GE	: '>=' ;
LE	: '<=' ;
GT	: '>' ;
LW	: '<' ;
EQ	: '==' ;
NE	: '!=' ;	
ASS	: '=' ;
TRUE	: 'true' ;
FALSE	: 'false' ;
IF	: 'if' ;
THEN	: 'then';
ELSE	: 'else' ;
PRINT	: 'print' ;
LET     : 'let' ;	
IN      : 'in' ;	
VAR     : 'var' ;
FUN	: 'fun' ; 
CLASS	: 'class' ; 
EXTENDS : 'extends' ;	
NEW 	: 'new' ;	
NULL    : 'null' ;	  
INT	: 'int' ;
BOOL	: 'bool' ;
ARROW   : '->' ; 	
INTEGER : (('1'..'9')('0'..'9')*) | '0' ; 
ID  	: ('a'..'z'|'A'..'Z')('a'..'z' | 'A'..'Z' | '0'..'9')* ;
WHITESP : ( '\t' | ' ' | '\r' | '\n' )+    { skip(); } ;

COMMENT  : '/*' (options {greedy=false;} : .)* '*/' { skip(); } ;

