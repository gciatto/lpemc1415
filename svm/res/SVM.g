grammar SVM;

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/
 
@header {
package it.unibo.gciatto.vm.svm.parsing;

import it.unibo.gciatto.vm.ICompiler;
import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.svm.SVMCompiler;
}



@lexer::header {
package it.unibo.gciatto.vm.svm.parsing;
}

@members {
private ICompiler mCompiler = new SVMCompiler();
private String mLabel = null;

public ICompiler getCompiler() { return mCompiler; }

public void setCompiler(ICompiler compiler) { mCompiler = compiler; }

private IInstruction putInstruction(String name, Object... args) {
	final IInstruction i = mCompiler.putInstruction(name, args);
	if (mLabel != null) {
		mCompiler.assignLabel(mLabel, i);
		mLabel = null;
	}
	return i;
}
}

assembly: ( push1 = PUSH number1 = NUMBER { putInstruction($push1.text, Integer.parseInt($number1.text)); }
		//push NUMBER on the stack
	  | push2 = PUSH label1 = LABEL { putInstruction($push2.text, $label1.text); }
	  	//push the location address pointed by LABEL on the stack	     
	  | pop = POP { putInstruction($pop.text); }
	  	//pop the top of the stack 
	  | add = ADD { putInstruction($add.text); }
	  	//replace the two values on top of the stack with their sum
	  | sub = SUB { putInstruction($sub.text); }
	  	//pop the two values v1 and v2 (respectively) and push v2-v1
	  | mult = MULT { putInstruction($mult.text); }
	  	//replace the two values on top of the stack with their product	
	  | div = DIV	{ putInstruction($div.text); }
	  	//pop the two values v1 and v2 (respectively) and push v2 DIV v1
	  | sw = STOREW { putInstruction($sw.text); }
	  	//pop two values: 
	  	//  the second one is written at the memory address pointed by the first one
	  | lw = LOADW  { putInstruction($lw.text); }
		//read the content of the memory cell pointed by the top of the stack
	  	//  and replace the top of the stack with such value
	  | lbl = LABEL COL  { mLabel = $lbl.text; }
	  	//LABEL points at the location of the subsequent instruction
	  | b = BRANCH label2 = LABEL { putInstruction($b.text, $label2.text); }
	  	//jump at the instruction pointed by LABEL
	  | beq = BRANCHEQ label3 = LABEL   { putInstruction($beq.text, $label3.text); }
	  	//pop two values and jump if they are equal
	  | bless = BRANCHLESS label4 = LABEL  { putInstruction($bless.text, $label4.text); }
	  	//pop two values and jump if the second one is less or equal to the first one
	  | js = JS  { putInstruction($js.text); }
	  	//pop one value from the stack:
		//  copy the instruction pointer in the RA register and jump to the popped value    
	  | lra = LOADRA { putInstruction($lra.text); }
	  	//push in the stack the content of the RA register   
	  | sra = STORERA { putInstruction($sra.text); }
	  	//pop the top of the stack and copy it in the RA register     
	  | lrv = LOADRV { putInstruction($lrv.text); }
	        //push in the stack the content of the RV register    
	  | srv = STORERV { putInstruction($srv.text); }
	      //pop the top of the stack and copy it in the RV register    
	  | lfp = LOADFP { putInstruction($lfp.text); }
	        //push in the stack the content of the FP register   
	  | sfp = STOREFP { putInstruction($sfp.text); }
	       //pop the top of the stack and copy it in the FP register    
	  | cfp = COPYFP  { putInstruction($cfp.text); }
	  	//copy in the FP register the currest stack pointer    
	  | lhp = LOADHP  { putInstruction($lhp.text); }
	 	//push in the stack the content of the HP register    
	  | shp = STOREHP    { putInstruction($shp.text); } 
	  	//pop the top of the stack and copy it in the HP register    
	  | print = PRINT { putInstruction($print.text); } 
	       //visualize the top of the stack without removing it   
	  | halt = HALT { putInstruction($halt.text); }  
	  	//interrupt the execution    
	  | beless = BRANCHEQLESS { putInstruction($beless.text); }
	  | bemore = BRANCHEQMORE { putInstruction($bemore.text); }
	  | bmore = BRANCHMORE { putInstruction($bmore.text); }
	  | bneq = BRANCHNOTEQ { putInstruction($bneq.text); }
	  | and = AND { putInstruction($and.text); }
	  | or = OR { putInstruction($or.text); }
	  | xor = XOR { putInstruction($xor.text); }
	  )* 
 ;
 	 
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

PUSH	 : 'push' ; 	
POP	 : 'pop' ; 	
ADD	 : 'add' ;  	
SUB	 : 'sub' ;	
MULT	 : 'mult' ;  	
DIV	 : 'div' ;	
STOREW	 : 'sw' ; 	
LOADW	 : 'lw' ;	
BRANCH	 : 'b' ;	
BRANCHEQ : 'beq' ;	
BRANCHLESS:'bless' ;	
JS	 : 'js' ;	
LOADRA	 : 'lra' ;	
STORERA  : 'sra' ;	 
LOADRV	 : 'lrv' ;	
STORERV  : 'srv' ;	
LOADFP	 : 'lfp' ;	
STOREFP	 : 'sfp' ;	
COPYFP   : 'cfp' ;      
LOADHP	 : 'lhp' ;	
STOREHP	 : 'shp' ;	
PRINT	 : 'print' ;	
HALT	 : 'halt' ;	
AND	: 'and';
OR	: 'or';
XOR	: 'xor';
BRANCHNOTEQ : 'bneq' ;	
BRANCHEQLESS:'beless' ;	
BRANCHEQMORE:'bemore' ;	
BRANCHMORE:'bmore' ;	

COL	 : ':' ;
LABEL	 : ('a'..'z'|'A'..'Z'|'_'|'$')('a'..'z' | 'A'..'Z' | '0'..'9'|'_'|'$')* ;
NUMBER	 : '0' | ('-')?(('1'..'9')('0'..'9')*) ;

WHITESP  : ( '\t' | ' ' | '\r' | '\n' )+    { skip(); } ;

COMMENT  : '[' (options {greedy=false;} : .)* ']' { skip(); } ;


