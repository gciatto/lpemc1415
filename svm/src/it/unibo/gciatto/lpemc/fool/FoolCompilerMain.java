package it.unibo.gciatto.lpemc.fool;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.parsing.FOOLLexer;
import it.unibo.gciatto.sdt.parsing.FOOLParser;
import it.unibo.gciatto.vm.ICompiler;
import it.unibo.gciatto.vm.IProgram;
import it.unibo.gciatto.vm.svm.SVMCompiler;

import java.io.File;
import java.io.FileWriter;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;

public class FoolCompilerMain {
	private static boolean getFiles(final String[] args) {
		if (args == null || args.length < 1) {
			System.err.println("Usage:\n\tfoolc <in_path> [-o <out_path>]");
			return false;
		} else {
			mInputFile = new File(args[0]);
			mInputFile = new File(mInputFile.getAbsolutePath());

			if (!mInputFile.exists() || !mInputFile.isFile()) {
				System.err.printf("File not found: %s", mInputFile.getAbsolutePath());
				return false;
			}

			if (args.length >= 3) {
				if (args[1].equalsIgnoreCase("-o")) {
					mOutputFile = new File(args[2]);
				}
			} else {
				String fname = mInputFile.getName();
				final int dotIndex = fname.lastIndexOf(".");
				fname = fname.substring(0, dotIndex);
				final String folder = mInputFile.getParent();
				mOutputFile = new File(folder, fname + ".svm");
			}

			return true;
		}
	}

	public static void main(final String[] args) throws Throwable {
		if (getFiles(args)) {
			final ANTLRStringStream stream = new ANTLRFileStream(mInputFile.getAbsolutePath()); // new ANTLRFileStream(code);
			final FOOLLexer lexer = new FOOLLexer(stream);
			final CommonTokenStream token = new CommonTokenStream(lexer);
			final FOOLParser parser = new FOOLParser(token);
			final IASTNode<?> node = parser.prog();
			final ICompiler compiler = new SVMCompiler();
			final IProgram prog = node.generate(compiler).compile();
			final FileWriter writer = new FileWriter(mOutputFile);
			writer.write(prog.toString());
			writer.close();
			System.out.println("Done!");
		}

	}

	private static File mInputFile, mOutputFile;

}
