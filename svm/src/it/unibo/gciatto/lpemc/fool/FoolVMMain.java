package it.unibo.gciatto.lpemc.fool;

import it.unibo.gciatto.utils.SizeUnit;
import it.unibo.gciatto.vm.ICompiler;
import it.unibo.gciatto.vm.IProgram;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.impl.Ram32Bit;
import it.unibo.gciatto.vm.stack.IStack;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;
import it.unibo.gciatto.vm.stack.impl.Stack32Bit;
import it.unibo.gciatto.vm.svm.SVMCompiler;
import it.unibo.gciatto.vm.svm.SVMCpu;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.antlr.runtime.RecognitionException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

public class FoolVMMain {

	private static void error(final Options options) {
		final HelpFormatter helper = new HelpFormatter();
		helper.printHelp("foolvm [OPTIONS] <in_path>", options);
	}

	@SuppressWarnings("static-access")
	public static void main(final String[] args) throws Throwable {
		OptionBuilder.withArgName("size");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("VM Ram size (default unit is KiB, default size is 4 KiB)");
		final Option memsize = OptionBuilder.create("memsize");
		OptionBuilder.withArgName("unit");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("Unit for ram size. Choose one in [B, KB, KiB, MB, MiB] case un-sensintive");
		final Option sizeunit = OptionBuilder.create("u");
		OptionBuilder.withDescription("Shows VM evolution step by step");
		final Option verbose = OptionBuilder.create("v");

		final Options options = new Options();
		options.addOption(memsize);
		options.addOption(sizeunit);
		options.addOption(verbose);

		if (args.length > 0) {
			final CommandLineParser parser = new GnuParser();
			try {
				final CommandLine line = parser.parse(options, args);
				set(line);
				run();

			} catch (final Exception pex) {
				error(options);
			}
		} else {
			error(options);
		}
	}
	
	private static void run() throws IOException, RecognitionException {
		if (!mVerbose) {
			Logger.getGlobal().setLevel(Level.OFF);
		}
		final ICompiler compiler = new SVMCompiler();
		final IProgram prog = compiler.compile(mInputFile);
		final IRandomAccessMemory mem = new Ram32Bit(mMemSize, mSizeUnit);
		final IStack stack = new Stack32Bit(mem, IStack.Direction.POSITIVE, prog.getLength());
		final IStackBasedCpu cpu = new SVMCpu(mem, stack);
		prog.execute(cpu);
	}

	private static void set(final CommandLine line) {
		if (line.getArgs().length > 0) {
			mInputFile = new File(line.getArgs()[0]);
			mInputFile = new File(mInputFile.getAbsolutePath());
			if (!mInputFile.exists()) {
				throw new IllegalStateException("File not found");
			}
			if (line.hasOption("v")) {
				mVerbose = true;
			}
			if (line.hasOption("memsize")) {
				final String size = line.getOptionValue("memsize");
				mMemSize = Integer.parseInt(size);
			}
			if (line.hasOption("u")) {
				final String unit = line.getOptionValue("u");
				final List<SizeUnit> units = Arrays.asList(SizeUnit.values());
				mSizeUnit = units.stream().filter(u -> u.name().equalsIgnoreCase(unit)).collect(Collectors.toList()).get(0);
			}
		} else {
			throw new IllegalStateException("You have to specify a file to run");
		}
	}
	
	private static File mInputFile;

	private static int mMemSize = 4;

	private static SizeUnit mSizeUnit = SizeUnit.KiB;

	private static boolean mVerbose = false;

	private FoolVMMain() {
		// TODO Auto-generated constructor stub
	}
}
