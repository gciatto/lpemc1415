package it.unibo.gciatto.sdt.adt;

import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.utils.tree.ITreeNode;
import it.unibo.gciatto.vm.ICompiler;

public interface IASTNode<T> extends ITreeNode<T> {
	T evaluate() throws TypeException;
	
	ICompiler generate(ICompiler compiler);

	@Override
	IASTNode<T> getChild(int index);
	
	// List<? extends IASTNode<T>> getChildren();
	
	@Override
	IASTNode<T> getParent();
	
	IType getType();
}
