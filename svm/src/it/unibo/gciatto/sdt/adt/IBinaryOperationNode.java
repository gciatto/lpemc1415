package it.unibo.gciatto.sdt.adt;

public interface IBinaryOperationNode<T> extends IASTNode<T> {
	
	public abstract IASTNode<?> getFirstValue();
	
	public abstract IASTNode<?> getSecondValue();
	
}