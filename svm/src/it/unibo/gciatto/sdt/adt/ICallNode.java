package it.unibo.gciatto.sdt.adt;

import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.IType;

import java.util.List;

public interface ICallNode<T> extends IASTNode<T>, IScopedNode<T> {
	
	public abstract void addParam(IASTNode<T> expression);
	
	public abstract List<IType> getActualTypes();
	
	public abstract String getName();
	
	public abstract boolean validate() throws TypeException;

}