package it.unibo.gciatto.sdt.adt;

public interface IClassEntryNode extends IASTNode<Object> {
	
	String getName();
	
	@Override
	public abstract IClassNode getParent();
	
}