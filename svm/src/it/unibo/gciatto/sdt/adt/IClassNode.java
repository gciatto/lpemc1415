package it.unibo.gciatto.sdt.adt;

import it.unibo.gciatto.sdt.typing.IType;

import java.util.Iterator;
import java.util.List;

public interface IClassNode extends IASTNode<Object> {
	void addField(IFieldNode field);

	void addMethod(IMethodNode method);

	IConstructorNode getConstructor();

	List<IClassEntryNode> getEntries();

	IFieldNode getFieldByName(String name);

	List<IFieldNode> getFields();

	Iterator<IClassNode> getHierarchyIterator();

	IType getInstancesType();

	IMethodNode getMethodByName(String name);

	List<IMethodNode> getMethods();

	String getName();

	IClassNode getSuperClass();

	int getSymbolIndex(String name);

	boolean hasSymbol(String name);
	
}
