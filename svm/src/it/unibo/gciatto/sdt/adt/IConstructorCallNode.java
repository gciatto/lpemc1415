package it.unibo.gciatto.sdt.adt;

public interface IConstructorCallNode extends ICallNode<Object> {
	IClassNode getOverClass();
}
