package it.unibo.gciatto.sdt.adt;

public interface IConstructorNode extends IFunctionNode<Object> {
	final String FUNCTION_NAME = "$init$";

	@Override
	IConstructorCallNode getCallNode(int currentNestingLevel);

	@Override
	IClassNode getParent();
}
