package it.unibo.gciatto.sdt.adt;

public interface IDeclarationNode<T> extends IScopedNode<T> {
	
	public abstract String getName();
	
}