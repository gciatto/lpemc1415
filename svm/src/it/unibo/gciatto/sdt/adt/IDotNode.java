package it.unibo.gciatto.sdt.adt;

import it.unibo.gciatto.sdt.exception.TypeException;

public interface IDotNode extends IASTNode<Object> {
	IIdNode<?> getLeft();

	IASTNode<?> getRight();

	boolean validate() throws TypeException;
}
