package it.unibo.gciatto.sdt.adt;

public interface IFieldNode extends IDeclarationNode<Object>, IClassEntryNode {}
