package it.unibo.gciatto.sdt.adt;

import it.unibo.gciatto.sdt.typing.IFunctionType;

public interface IFunctionNode<T> extends ILetInNode<T>, IScopedNode<T> {
	
	public abstract void addParam(IDeclarationNode<T> dec);
	
	public String generateFunctionLabel();

	ICallNode getCallNode(int currentNestingLevel);

	public abstract String getName();
	
	@Override
	public IFunctionType getType();
	
}