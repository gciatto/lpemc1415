package it.unibo.gciatto.sdt.adt;

import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.sdt.typing.impl.Type;

import java.util.List;

public interface ILetInNode<T> extends IASTNode<T> {
	
	public static final IType TYPE_LET_IN = new Type("$let_in$");
	
	void addClass(IClassNode clazz);
	
	void addClasses(List<IClassNode> classes);
	
	public abstract void addDeclaration(IASTNode<T> declaration);
	
	void addDeclarations(List<IASTNode<T>> declarations);
	
	public abstract List<IClassNode> getClasses();
	
	public abstract List<IASTNode<T>> getDeclarations();
	
	public abstract IASTNode<T> getExpression();

	public int getVariableIndexByName(String name);
	
	public abstract void insertDeclaration(IASTNode<T> declaration, int index);
	
	public abstract boolean isExpressionSet();
	
	public abstract void setExpression(IASTNode<T> node);

	// void adjustScope(IScope scope);
	
}