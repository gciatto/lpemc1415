package it.unibo.gciatto.sdt.adt;

public interface IMethodCallNode extends ICallNode<Object> {
	IIdNode<?> getOverObject();
}
