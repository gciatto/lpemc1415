package it.unibo.gciatto.sdt.adt;

public interface IMethodNode extends IFunctionNode<Object>, IClassEntryNode {
	final String FIRST_PARAM_NAME = "$obj$";
	
	IMethodCallNode getCallNode(IIdNode<?> overObject, int currentNestingLevel);
}
