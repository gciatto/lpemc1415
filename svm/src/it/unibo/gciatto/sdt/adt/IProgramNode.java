package it.unibo.gciatto.sdt.adt;

import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.sdt.typing.impl.Type;

public interface IProgramNode extends IASTNode<Object> {

	public static final IType TYPE_PROGRAM = new Type("$program$");

	public abstract ILetInNode<Object> getBody();

	public abstract void setBody(ILetInNode<Object> body);

}