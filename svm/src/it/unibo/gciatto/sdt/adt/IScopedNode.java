package it.unibo.gciatto.sdt.adt;

import it.unibo.gciatto.sdt.scoping.ISymbolData;

public interface IScopedNode<T> extends IASTNode<T> {
	// FIXME remove?
	int getNestingLevel();

	ISymbolData getSymbolData();
}
