package it.unibo.gciatto.sdt.adt;

public interface ITernaryNode extends IASTNode<Object> {
	
	public abstract IASTNode<?> getCondition();
	
	public abstract IASTNode<?> getExprFalse();
	
	public abstract IASTNode<?> getExprTrue();
	
	public abstract void setCondition(IASTNode<Object> condition);
	
	public abstract void setExprFalse(IASTNode<Object> exprFalse);
	
	public abstract void setExprTrue(IASTNode<Object> exprTrue);
	
}