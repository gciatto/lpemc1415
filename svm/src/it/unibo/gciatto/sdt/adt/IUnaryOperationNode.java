package it.unibo.gciatto.sdt.adt;

public interface IUnaryOperationNode<T> extends IASTNode<T> {
	
	public abstract IASTNode<?> getFirstValue();
	
}