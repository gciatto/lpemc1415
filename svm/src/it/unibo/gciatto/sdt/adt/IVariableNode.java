package it.unibo.gciatto.sdt.adt;

public interface IVariableNode<T> extends IDeclarationNode<T> {
	
	public abstract IASTNode<T> getExpression();
	
}