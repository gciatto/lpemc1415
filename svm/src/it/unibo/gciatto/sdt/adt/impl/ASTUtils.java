package it.unibo.gciatto.sdt.adt.impl;

import java.util.ArrayList;

public class ASTUtils {
	
	public static int booleanToInt(final boolean value) {
		if (value) {
			return 1;
		} else {
			return 0;
		}
	}

	public static BooleanNode[] fromBooleanValues(final boolean... values) {
		final ArrayList<BooleanNode> nodes = new ArrayList<BooleanNode>(values.length);
		for (final boolean v : values) {
			nodes.add(new BooleanNode(v));
		}
		return nodes.toArray(new BooleanNode[0]);
	}

	public static IntegerNode[] fromIntegerValues(final int... values) {
		final ArrayList<IntegerNode> nodes = new ArrayList<IntegerNode>(values.length);
		for (final int v : values) {
			nodes.add(new IntegerNode(v));
		}
		return nodes.toArray(new IntegerNode[0]);
	}

	public static boolean intToBoolean(final int value) {
		if (value == 1) {
			return true;
		} else if (value == 0) {
			return false;
		} else {
			throw new IllegalArgumentException(String.format("Cannot convert %s to boolean", value));
		}
	}
	
	private ASTUtils() {
		throw new IllegalStateException();
	}
	
}
