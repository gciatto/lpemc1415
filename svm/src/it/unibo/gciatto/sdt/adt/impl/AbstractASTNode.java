package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.scoping.ISymbolData;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.utils.tree.ITreeNode;
import it.unibo.gciatto.utils.tree.impl.TreeNode;
import it.unibo.gciatto.vm.ICompiler;
import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IInstructionPutListener;
import it.unibo.gciatto.vm.InstructionPutListener;

public abstract class AbstractASTNode<T> extends TreeNode<T> implements IASTNode<T> {

	protected static String generateLabel() {
		return String.format("label%s", mLabelNumber++);
	}

	protected static IInstructionPutListener generateLabeller(final String label) {
		return new InstructionPutListener() {
			
			@Override
			public void onNextPutting(final ICompiler compiler, final int futureAddress, final IInstruction instruction, final Object... args) {
				instruction.setLabel(label);
			}

		};
	}

	protected static String[] generateLabels(final int howMany) {
		final String[] labels = new String[howMany];
		for (int i = 0; i < howMany; i++) {
			labels[i] = generateLabel();
		}
		return labels;
	}

	private static int mLabelNumber = 0;

	protected static final String LABEL_MAIN = "$main$";
	
	public static final String NULL_NAME = "$null$";

	public AbstractASTNode() {
		super();
	}
	
	public AbstractASTNode(final int childrenCount) {
		super(childrenCount);
	}
	
	public AbstractASTNode(final int childrenCount, final ITreeNode<T> parent) {
		super(childrenCount, parent);
	}
	
	public AbstractASTNode(final T value) {
		super(value);
	}
	
	public AbstractASTNode(final T value, final int childrenCount) {
		super(value, childrenCount);
	}
	
	public AbstractASTNode(final T value, final int childrenCount, final ITreeNode<T> parent) {
		super(value, childrenCount, parent);
	}
	
	public AbstractASTNode(final T value, final ITreeNode<T> parent) {
		super(value, parent);
	}
	
	protected int calculateNestingDifference(final int current, final ISymbolData data) {
		final int d = current - (data == null ? 0 : data.getLevel());
		return d;
	}
	
	protected String childrenToString(final int i) {
		if (countChildren() == 0) {
			return "";
		}
		if (i == countChildren() - 1) {
			return getChild(i).toString();
		} else {
			return String.format("%s,%s", getChild(i), childrenToString(i + 1));
		}

	}
	
	@Override
	public abstract T evaluate() throws TypeException;
	
	protected ICompiler generateALNavigation(final ICompiler compiler, final int currentNestingLevel, final ISymbolData definitionData) {
		compiler.putInstruction("lfp");
		final int diff = calculateNestingDifference(currentNestingLevel, definitionData);
		for (int i = 0; i < diff /* - 1 */; i++) {
			compiler.putInstruction("lw");
		}
		return compiler;
	}
	
	@Override
	public IASTNode<T> getChild(final int index) {
		return (IASTNode<T>) super.getChild(index);
	}
	
	@Override
	public IASTNode<T> getParent() {
		return (IASTNode<T>) super.getParent();
	}

	@Override
	public abstract IType getType();
}
