package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.adt.IBinaryOperationNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.sdt.typing.Types;
import it.unibo.gciatto.vm.ICompiler;

public abstract class BinaryOperationNode extends AbstractASTNode<Object> implements IBinaryOperationNode<Object> {
	private IType mType;

	public BinaryOperationNode(final boolean value1, final boolean value2) {
		this(new BooleanNode(value1), new BooleanNode(value2));
	}
	
	@SuppressWarnings({
			"rawtypes", "unchecked"
	})
	public BinaryOperationNode(final IASTNode<?> value1, final IASTNode<?> value2) {
		if (checkType(value1.getType(), value2.getType())) {
			addChild((IASTNode) value1);
			addChild((IASTNode) value2);
		}
		init(value1, value2);
	}
	
	public BinaryOperationNode(final int value1, final int value2) {
		this(new IntegerNode(value1), new IntegerNode(value2));
	}
	
	protected boolean checkType(final IType type1, final IType type2) throws TypeException {
		if (Types.INTEGER.isSupertypeOf(type1) || Types.INTEGER.isSupertypeOf(type2)) {
			return true;
		}
		throw new TypeException(Types.INTEGER);
	}
	
	@Override
	public abstract Object evaluate() throws TypeException;
	
	@Override
	public ICompiler generate(final ICompiler compiler) {
		getFirstValue().generate(compiler);
		getSecondValue().generate(compiler);
		return compiler;
	}

	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.IBinaryOperationNode#getFirstValue()
	 */
	@Override
	public IASTNode<?> getFirstValue() {
		return getChild(0);
	}

	protected abstract String getOpName();
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.IBinaryOperationNode#getSecondValue()
	 */
	@Override
	public IASTNode<?> getSecondValue() {
		return getChild(1);
	}

	@Override
	public IType getType() {
		return mType;
	}
	
	protected void init(final IASTNode<?> value1, final IASTNode<?> value2) {
		
		if (Types.INTEGER.isSupertypeOf(value1.getType()) && Types.INTEGER.isSupertypeOf(value2.getType())) {
			if (Types.BOOLEAN.isSameTypeOf(value1.getType()) && Types.BOOLEAN.isSameTypeOf(value1.getType())) {
				setType(Types.BOOLEAN);
			} else {
				setType(Types.INTEGER);
			}
		}
	}
	
	protected void setType(final IType type) {
		mType = type;
	}

	@Override
	public String toString() {
		return String.format("%s(%s):%s", getOpName(), childrenToString(0), getType());
	}
}
