package it.unibo.gciatto.sdt.adt.impl;

import static it.unibo.gciatto.sdt.typing.Types.BOOLEAN;
import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.IType;

import java.util.Objects;

public class BooleanNode extends IntegerNode {

	public BooleanNode(final boolean value) {
		super(ASTUtils.booleanToInt(value));
	}
	
	public BooleanNode(final IASTNode<Boolean> node) {
		this(node.evaluate());
	}

	@Override
	public Integer evaluate() throws TypeException {
		return getValue();
	}
	
	public Boolean getBooleanValue() {
		final Integer value = getValue();
		if (Objects.nonNull(value)) {
			return ASTUtils.intToBoolean(value);
		}
		return null;
	}

	@Override
	public IType getType() {
		return BOOLEAN;
	}

	@Override
	public String toString() {
		return String.format("bool(%s)", getValue());
	}
}
