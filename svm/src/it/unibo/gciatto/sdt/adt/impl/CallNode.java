package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.adt.ICallNode;
import it.unibo.gciatto.sdt.adt.IDeclarationNode;
import it.unibo.gciatto.sdt.adt.IFunctionNode;
import it.unibo.gciatto.sdt.adt.ILetInNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.scoping.ISymbolData;
import it.unibo.gciatto.sdt.typing.IFunctionType;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.vm.ICompiler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CallNode<T> extends AbstractASTNode<T> implements ICallNode<T> {
	private final String mName;
	private final IFunctionType mType;
	private final ISymbolData mData;
	private final int mNestingLevel;
	
	public CallNode(final ISymbolData data, final int currentNestinLevel, final IASTNode<T>... actuals) {
		mData = data;
		mName = data.getSymbol();
		if (data.getType() instanceof IFunctionType) {
			mType = (IFunctionType) data.getType();
		} else {
			throw new IllegalStateException();
		}
		mNestingLevel = currentNestinLevel;
		if (actuals.length > 0) {
			getChildren().addAll(Arrays.asList(actuals));
		}
	}
	
	public CallNode(final String name, final IFunctionType type, final int currentNestinLevel, final IASTNode<T>... actuals) {
		mName = name;
		mType = type;
		mData = null;
		mNestingLevel = currentNestinLevel;
		if (actuals.length > 0) {
			getChildren().addAll(Arrays.asList(actuals));
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ICallNode#addParam(it.unibo.gciatto.sdt.adt.IASTNode)
	 */
	@Override
	public void addParam(final IASTNode<T> expression) {
		addChild(expression);
	}
	
	@Override
	public T evaluate() throws TypeException {
		return null;
	}
	
	@Override
	public ICompiler generate(final ICompiler compiler) {
		compiler.putInstruction("lfp");
		
		for (int i = countChildren() - 1; i >= 0; i--) {
			getChild(i).generate(compiler);
		}
		generateFirstParameter(compiler);

		return generateCall(compiler);
		
	}
	
	protected ICompiler generateCall(final ICompiler compiler) {
		final IASTNode<?> def = getDefinitionNode();
		
		if (def instanceof IFunctionNode) {
			final IFunctionNode<?> funDef = (IFunctionNode<?>) def;
			
			generateALNavigation(compiler, mNestingLevel, mData);
			
			compiler.putInstruction("push", funDef.generateFunctionLabel());
			
		} else if (def instanceof IDeclarationNode) {
			final String[] labels = generateLabels(3);
			
			final ILetInNode<?> funDef = (ILetInNode<?>) def.getParent();
			final int callOffset = funDef.getVariableIndexByName(mName);
			// compiler.putInstruction("lfp"); //FP**1 corrente
			// compiler.putInstruction("push", getNestingLevel()); //Nesting level dello scope corrente
			//
			// generateALNavigation(compiler, mNestingLevel, mData);
			//
			// compiler.putInstruction("push", callOffset + 1);
			// compiler.putInstruction("add");
			// compiler.putInstruction("lw"); //Recupero il nesting level della funzione chiamata
			// compiler.putInstruction("sub"); //Faccio la differenza di nesting
			//
			// //Inizia un loop dove uso il registro RV come registro d'appoggio
			// //in questo contesto il valore precedente di RV è inutile.
			// compiler.setNextPutListener(generateLabeller(labels[0])); //Label loop
			// compiler.putInstruction("srv");
			// compiler.putInstruction("lrv");
			// compiler.putInstruction("push", 0);
			// //Se la differenza è maggiore di zero vado a labels[1]
			// compiler.putInstruction("bmore", labels[1]);
			// //altrimenti vado a labels[2]
			// compiler.putInstruction("b", labels[2]);
			//
			// //Se la differenza è maggiore di zero...
			// compiler.setNextPutListener(generateLabeller(labels[1])); //Label ifMore
			// compiler.putInstruction("lw"); //AL = CL precedente = lw(FP**1)
			// compiler.putInstruction("lrv");
			// compiler.putInstruction("push", 1);
			// compiler.putInstruction("sub"); //Decremento la differenza di 1
			// compiler.putInstruction("b", labels[0]); // Ricomincio il ciclo
			//
			// //Se la differenza è minore o uguale a zero...
			// compiler.setNextPutListener(generateLabeller(labels[2]));
			//
			// generateALNavigation(compiler, mNestingLevel, mData);
			generateALNavigation(compiler, mNestingLevel, mData);
			
			compiler.putInstruction("srv");
			compiler.putInstruction("lrv");
			
			compiler.putInstruction("push", callOffset + 1);
			compiler.putInstruction("add");
			compiler.putInstruction("lw");
			
			compiler.putInstruction("lrv");
			compiler.putInstruction("push", callOffset);
			
			compiler.putInstruction("add");
			compiler.putInstruction("lw");
		}
		compiler.putInstruction("js");
		
		return compiler;
	}
	
	protected ICompiler generateFirstParameter(final ICompiler compiler) {
		return compiler;
	}

	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ICallNode#getActualTypes()
	 */
	@Override
	public List<IType> getActualTypes() {
		final List<IType> actuals = new ArrayList<IType>(countChildren());
		for (int i = 0; i < countChildren(); i++) {
			actuals.add(getChild(i).getType());
		}
		return actuals;
	}
	
	protected IASTNode<?> getDefinitionNode() {
		return mData.getDefinitionNode();
	}
	
	protected IFunctionType getFunctionType() {
		return mType;
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ICallNode#getName()
	 */
	@Override
	public String getName() {
		return mName;
	}
	
	@Override
	public int getNestingLevel() {
		return mNestingLevel;
	}
	
	@Override
	public ISymbolData getSymbolData() {
		return mData;
	}
	
	@Override
	public IType getType() {
		return mType.getReturnType();
	}
	
	@Override
	public String toString() {
		return String.format("call(%s:(%s)->%s)", mName, childrenToString(0), getType());
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ICallNode#validate()
	 */
	@Override
	public boolean validate() throws TypeException {
		final List<IType> paramTypes = mType.getParamsTypes();
		return validate(paramTypes);
	}
	
	protected boolean validate(final List<IType> paramTypes) throws TypeException {
		final List<IType> actualTypes = getActualTypes();
		if (paramTypes.size() == actualTypes.size()) {
			for (int i = 0; i < countChildren(); i++) {
				if (!paramTypes.get(i).isSupertypeOf(actualTypes.get(i))) {
					throw new TypeException(paramTypes.get(i), actualTypes.get(i));
				}
			}
			return true;
		} else {
			throw new TypeException(mType);
		}
	}
	
}
