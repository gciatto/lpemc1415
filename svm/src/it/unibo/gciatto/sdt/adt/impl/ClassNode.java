package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IClassEntryNode;
import it.unibo.gciatto.sdt.adt.IClassNode;
import it.unibo.gciatto.sdt.adt.IConstructorNode;
import it.unibo.gciatto.sdt.adt.IFieldNode;
import it.unibo.gciatto.sdt.adt.IMethodNode;
import it.unibo.gciatto.sdt.exception.NotDeclaredException;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.sdt.typing.Types;
import it.unibo.gciatto.sdt.typing.impl.Type;
import it.unibo.gciatto.vm.ICompiler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class ClassNode extends AbstractASTNode<Object> implements IClassNode {
	private final IClassNode mSuperClass;
	private final List<IFieldNode> mFileds = new LinkedList<IFieldNode>();
	private final List<IMethodNode> mMethods = new LinkedList<IMethodNode>();
	private final String mName;
	private final IType mType, mMetaType;
	private IConstructorNode mConstructor;
	
	public ClassNode(final String name) {
		this(name, ObjectClassNode.getInstance());
	}
	
	public ClassNode(final String name, final IClassNode superClass) {
		mName = name;
		mSuperClass = superClass;
		final int initialSize = superClass == null ? 0 : superClass.getType().getSize();
		mMetaType = new Type(String.format("$class_%s$", mName), initialSize);
		mType = Types.putDefition(this, superClass == null ? new Type(name) : new Type(name, mSuperClass.getInstancesType()));
		setConstructor(new ConstructorNode(this));
	}
	
	@Override
	public void addField(final IFieldNode field) {
		final boolean needToInc = !hasSymbol(field.getName());
		mFileds.add(field);
		insertChild(field, 0);
		if (needToInc) {
			getType().setSize(getType().getSize() + field.getType().getSize());
		}
		// FIXME find a better way to do this
		setConstructor(new ConstructorNode(this));
	}
	
	@Override
	public void addMethod(final IMethodNode method) {
		final boolean needToInc = !hasSymbol(method.getName());
		mMethods.add(method);
		addChild(method);
		if (needToInc) {
			getType().setSize(getType().getSize() + method.getType().getSize());
		}
	}
	
	@Override
	public Object evaluate() throws TypeException {
		return null;
	}
	
	@Override
	public ICompiler generate(final ICompiler compiler) {
		getConstructor().generate(compiler);
		getMethods().forEach(m -> m.generate(compiler));
		return compiler;
	}
	
	@Override
	public IConstructorNode getConstructor() {
		return mConstructor;
	}
	
	@Override
	public List<IClassEntryNode> getEntries() {
		final List<IClassEntryNode> superEntries = mSuperClass == null ? null : mSuperClass.getEntries();
		final ArrayList<IClassEntryNode> entries = new ArrayList<IClassEntryNode>(mMethods.size() + mFileds.size() + (superEntries != null ? superEntries.size() : 0));
		final List<IClassEntryNode> added = new LinkedList<IClassEntryNode>();
		if (superEntries != null) {
			for (final IClassEntryNode entry : superEntries) {
				if (hasEntry(entry)) {
					final IClassEntryNode en = getEntry(entry);
					entries.add(en);
					added.add(en);
				} else {
					entries.add(entry);
				}
			}
		}
		for (final IFieldNode entry : mFileds) {
			if (!added.contains(entry)) {
				entries.add(entry);
			} else {
				added.remove(entry);
			}
		}
		for (final IMethodNode entry : mMethods) {
			if (!added.contains(entry)) {
				entries.add(entry);
			} else {
				added.remove(entry);
			}
		}
		return entries;
	}
	
	protected IClassEntryNode getEntry(final IClassEntryNode entry) {
		final List<? extends IClassEntryNode> entries;
		if (entry instanceof IFieldNode) {
			entries = mFileds.stream().filter(f -> f.getName().equals(entry.getName())).limit(1).collect(Collectors.toList());

		} else {
			entries = mMethods.stream().filter(m -> m.getName().equals(entry.getName())).limit(1).collect(Collectors.toList());
		}
		return entries.get(0);
	}
	
	@Override
	public IFieldNode getFieldByName(final String name) {
		for (final IFieldNode f : getFields()) {
			if (f.getName().equals(name)) {
				return f;
			}
		}
		throw new NotDeclaredException(name);
	}
	
	@Override
	public List<IFieldNode> getFields() {
		final ArrayList<IFieldNode> fields = new ArrayList<IFieldNode>(mFileds.size() + (mSuperClass == null ? 0 : mSuperClass.getFields().size()));
		final Set<IFieldNode> added = new HashSet<IFieldNode>();
		if (mSuperClass != null) {
			for (final IFieldNode field : mSuperClass.getFields()) {
				final List<IFieldNode> overrides = mFileds.stream().filter(f -> f.getName().equals(field.getName())).collect(Collectors.toList());
				if (overrides.isEmpty()) {
					fields.add(field);
				} else {
					fields.add(overrides.get(0));
					added.add(overrides.get(0));
				}
			}
		}
		for (final IFieldNode field : mFileds) {
			if (!added.contains(field)) {
				fields.add(field);
			}
		}
		return fields;
	}
	
	@Override
	public Iterator<IClassNode> getHierarchyIterator() {
		return new HierarchyIterator(this);
	}
	
	@Override
	public IType getInstancesType() {
		return mType;
	}
	
	@Override
	public IMethodNode getMethodByName(final String name) {
		for (final IMethodNode m : getMethods()) {
			if (m.getName().equals(name)) {
				return m;
			}
		}
		throw new NotDeclaredException(name);
	}
	
	@Override
	public List<IMethodNode> getMethods() {
		final ArrayList<IMethodNode> methods = new ArrayList<IMethodNode>(mMethods.size() + (mSuperClass == null ? 0 : mSuperClass.getMethods().size()));
		final Set<IMethodNode> added = new HashSet<IMethodNode>();
		if (mSuperClass != null) {
			for (final IMethodNode method : mSuperClass.getMethods()) {
				final List<IMethodNode> overrides = mMethods.stream().filter(f -> f.getName().equals(method.getName())).collect(Collectors.toList());
				if (overrides.isEmpty()) {
					methods.add(method);
				} else {
					methods.add(overrides.get(0));
					added.add(overrides.get(0));
				}
			}
		}
		for (final IMethodNode method : mMethods) {
			if (!added.contains(method)) {
				methods.add(method);
			}
		}
		return methods;
	}

	@Override
	public String getName() {
		return mName;
	}
	
	@Override
	public IClassNode getSuperClass() {
		return mSuperClass;
	}

	@Override
	public int getSymbolIndex(final String name) {
		final int base = 0;
		int i = base;
		// for (final IFieldNode f : getFields()) {
		// if (f.getName().equals(name)) {
		// return i;
		// }
		// i += f.getType().getSize();
		// }
		// for (final IMethodNode m : getMethods()) {
		// if (m.getName().equals(name)) {
		// return i;
		// }
		// i += m.getType().getSize();
		// }
		for (final IClassEntryNode entry : getEntries()) {
			if (entry.getName().equals(name)) {
				return i;
			}
			i += entry.getType().getSize();
		}
		throw new IllegalArgumentException();
	}

	@Override
	public IType getType() {
		return mMetaType;
	}

	protected boolean hasEntry(final IClassEntryNode entry) {
		final List<? extends IClassEntryNode> entries;
		if (entry instanceof IFieldNode) {
			entries = mFileds.stream().filter(f -> f.getName().equals(entry.getName())).limit(1).collect(Collectors.toList());

		} else {
			entries = mMethods.stream().filter(m -> m.getName().equals(entry.getName())).limit(1).collect(Collectors.toList());
		}
		return !entries.isEmpty();
	}

	@Override
	public boolean hasSymbol(final String name) {
		final boolean cond1 = getFields().stream().anyMatch(f -> f.getName().equals(name));
		final boolean cond2 = getMethods().stream().anyMatch(m -> m.getName().equals(name));
		return cond1 | cond2;
	}
	
	protected void setConstructor(final IConstructorNode constructor) {
		final int index = mFileds.size();
		if (mConstructor != null) {
			removeChild(index);
		}
		mConstructor = constructor;
		insertChild(constructor, index);
	}
	
	@Override
	public String toString() {
		return String.format("class(%s extends %s, constructor:%s, fields%s, methods%s)", getName(), getSuperClass() == null ? "none" : getSuperClass().getName(), getConstructor(), getFields(), getMethods());
	}
}

class HierarchyIterator implements Iterator<IClassNode> {
	private IClassNode mClass;
	
	public HierarchyIterator(final IClassNode start) {
		mClass = start;
	}

	@Override
	public boolean hasNext() {
		return Objects.nonNull(mClass);
	}

	@Override
	public IClassNode next() {
		final IClassNode output = mClass;
		mClass = mClass.getSuperClass();
		return output;
	}

}
