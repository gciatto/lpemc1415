package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.adt.IClassNode;
import it.unibo.gciatto.sdt.adt.IConstructorCallNode;

public class ConstructorCallNode extends CallNode<Object> implements IConstructorCallNode {
	private final IClassNode mOverClass;

	// public ConstructorCallNode(ISymbolData data, int currentNestinLevel, IASTNode<Object>... actuals) {
	// super(data, currentNestinLevel, actuals);
	// // TODO Auto-generated constructor stub
	// }
	
	@SafeVarargs
	public ConstructorCallNode(final IClassNode overClass, final int currentNestinLevel, final IASTNode<Object>... actuals) {
		super(overClass.getConstructor().getName(), overClass.getConstructor().getType(), currentNestinLevel, actuals);
		mOverClass = overClass;
	}
	
	@Override
	protected IASTNode<?> getDefinitionNode() {
		return mOverClass.getConstructor();
	}
	
	@Override
	public IClassNode getOverClass() {
		return mOverClass;
	}
	
}
