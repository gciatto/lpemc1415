package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.adt.IClassNode;
import it.unibo.gciatto.sdt.adt.IConstructorCallNode;
import it.unibo.gciatto.sdt.adt.IConstructorNode;
import it.unibo.gciatto.sdt.adt.IDeclarationNode;
import it.unibo.gciatto.sdt.adt.IFieldNode;
import it.unibo.gciatto.sdt.adt.IMethodNode;
import it.unibo.gciatto.vm.ICompiler;

public class ConstructorNode extends FunNode<Object> implements IConstructorNode {
	
	public ConstructorNode(final IClassNode clazz) {
		super(FUNCTION_NAME, clazz.getInstancesType());
		for (final IFieldNode f : clazz.getFields()) {
			if (f.getName().equals(ObjectClassNode.SELF_FIELD_NAME) || f.getName().equals(ObjectClassNode.SIZE_FIELD_NAME)) {
				continue;
			}
			addParam(new DeclarationNode<Object>(f.getName(), f.getType()));
		}
	}

	@Override
	protected ICompiler generateFunctionBody(final ICompiler compiler) {
		// Decremento l'HP della sizeof dell'oggetto
		compiler.putInstruction("lhp");
		compiler.putInstruction("push", getParent().getType().getSize());
		compiler.putInstruction("sub");
		compiler.putInstruction("shp");
		
		// Risultato della chiamata al costruttore: un puntatore all'oggetto
		compiler.putInstruction("lhp");
		
		compiler.putInstruction("lhp");
		compiler.putInstruction("lhp");
		compiler.putInstruction("sw");
		
		// Campo 1: $size$
		compiler.putInstruction("push", getParent().getType().getSize());
		compiler.putInstruction("lhp");
		compiler.putInstruction("push", 1);
		compiler.putInstruction("add");
		compiler.putInstruction("sw");
		
		int index = 2; // ho già scritto 2 word
		
		for (final IASTNode<?> d : getDeclarations()) {
			
			final IDeclarationNode<?> dec = (IDeclarationNode<?>) d;
			final int argSize = dec.getType().getSize();
			final int argIndex = getVariableIndexByName(dec.getName());
			final int fieldIndex = getParent().getSymbolIndex(dec.getName());
			for (int i = 0; i < argSize; i++) {
				// carica sullo stack la word i-esima dell'arg corrente
				compiler.putInstruction("lfp");
				compiler.putInstruction("push", argIndex + i);
				compiler.putInstruction("add");
				compiler.putInstruction("lw");
				
				// carica sullo stack l'indirizzo in cui scrivere la i-esima word dell'arg corrente
				compiler.putInstruction("lhp");
				compiler.putInstruction("push", fieldIndex + i);
				compiler.putInstruction("add");
				
				// scrive la word
				compiler.putInstruction("sw");
			}
			index += argSize;
			
		}
		
		for (final IMethodNode m : getParent().getMethods()) {
			final int methodIndex = getParent().getSymbolIndex(m.getName());
			compiler.putInstruction("push", m.generateFunctionLabel());
			compiler.putInstruction("lhp");
			compiler.putInstruction("push", methodIndex);
			compiler.putInstruction("add");
			compiler.putInstruction("sw");
			
			// compiler.putInstruction("push", m.getNestingLevel());
			compiler.putInstruction("lfp");
			// compiler.putInstruction("lw");
			compiler.putInstruction("lhp");
			compiler.putInstruction("push", methodIndex + 1);
			compiler.putInstruction("add");
			compiler.putInstruction("sw");
			
			index += 2;
		}
		
		if (index != getParent().getType().getSize()) {
			throw new IllegalStateException();
		}
		
		return compiler;
	}
	
	@Override
	public String generateFunctionLabel() {
		return String.format("%s_%s", getParent().getName(), getName());
	}
	
	@Override
	public IConstructorCallNode getCallNode(final int currentNestingLevel) {
		return new ConstructorCallNode(getParent(), currentNestingLevel);
	}

	@Override
	public IClassNode getParent() {
		return (IClassNode) super.getParent();
	}

}
