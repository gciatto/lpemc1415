package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IDeclarationNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.scoping.ISymbolData;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.vm.ICompiler;

public class DeclarationNode<T> extends AbstractASTNode<T> implements IDeclarationNode<T> {
	private final String mName;
	private final IType mType;
	private final ISymbolData mData;

	public DeclarationNode(final ISymbolData data) {
		mName = data.getSymbol();
		mType = data.getType();
		mData = data;
		data.setDefinitionNode(this);
	}

	public DeclarationNode(final String name, final IType type) {
		mName = name;
		mType = type;
		mData = null;
	}
	
	@Override
	public T evaluate() throws TypeException {
		return null;
	}
	
	@Override
	public ICompiler generate(final ICompiler compiler) {
		return compiler;
	}

	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.IDeclarationNode#getName()
	 */
	@Override
	public String getName() {
		return mName;
	}
	
	@Override
	public int getNestingLevel() {
		return mData.getLevel();
	}

	@Override
	public ISymbolData getSymbolData() {
		return mData;
	}
	
	@Override
	public IType getType() {
		return mType;
	}

	@Override
	public String toString() {
		return String.format("dec(%s:%s)", mName, mType);
	}
	
}
