package it.unibo.gciatto.sdt.adt.impl;

import static it.unibo.gciatto.sdt.typing.Types.BOOLEAN;
import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.vm.ICompiler;

import java.util.Objects;

public class EqualsNode extends BinaryOperationNode {
	
	public EqualsNode(final boolean value1, final boolean value2) {
		super(value1, value2);
	}
	
	public EqualsNode(final IASTNode<?> value1, final IASTNode<?> value2) {
		super(value1, value2);
	}
	
	public EqualsNode(final int value1, final int value2) {
		super(value1, value2);
	}
	
	@Override
	protected boolean checkType(final IType type1, final IType type2) throws TypeException {
		if (type1.isSupertypeOf(type2) || type2.isSupertypeOf(type1)) {
			return true;
		} else {
			throw new TypeException(type1, type2);
		}
	}
	
	@Override
	public Object evaluate() throws TypeException {
		final Object val1 = getFirstValue().evaluate();
		final Object val2 = getSecondValue().evaluate();
		if (Objects.nonNull(val1) && Objects.nonNull(val2)) {
			setValue(val1.equals(val2));
		} else {
			setValue(null);
		}
		return getValue();
	}

	@Override
	public ICompiler generate(final ICompiler compiler) {
		super.generate(compiler);

		final String[] labels = generateLabels(2);

		compiler.putInstruction("beq", labels[0]);
		compiler.putInstruction("push", 0);
		compiler.putInstruction("b", labels[1]);
		compiler.setNextPutListener(generateLabeller(labels[0]));
		compiler.putInstruction("push", 1);
		compiler.setNextPutListener(generateLabeller(labels[1]));

		return compiler;
	}

	@Override
	protected String getOpName() {
		return "equal";
	}

	@Override
	protected void init(final IASTNode<?> value1, final IASTNode<?> value2) {
		setType(BOOLEAN);
	}
	
}
