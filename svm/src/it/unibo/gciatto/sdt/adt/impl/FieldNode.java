package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IClassNode;
import it.unibo.gciatto.sdt.adt.IFieldNode;
import it.unibo.gciatto.sdt.scoping.ISymbolData;
import it.unibo.gciatto.sdt.typing.IType;

public class FieldNode extends DeclarationNode<Object> implements IFieldNode {

	public FieldNode(final ISymbolData data) {
		super(data);
	}
	
	public FieldNode(final String name, final IType type) {
		super(name, type);
	}

	@Override
	public IClassNode getParent() {
		return (IClassNode) super.getParent();
	}
	
}
