package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.adt.ICallNode;
import it.unibo.gciatto.sdt.adt.IDeclarationNode;
import it.unibo.gciatto.sdt.adt.IFunctionNode;
import it.unibo.gciatto.sdt.adt.ILetInNode;
import it.unibo.gciatto.sdt.adt.IVariableNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.scoping.ISymbolData;
import it.unibo.gciatto.sdt.typing.IFunctionType;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.sdt.typing.impl.FunctionType;
import it.unibo.gciatto.vm.ICompiler;

public class FunNode<T> extends LetInNode<T> implements IFunctionNode<T> {
	private final String mName;
	private final IFunctionType mType;
	private final ISymbolData mData;
	private int mParamsCount = 0;
	private int mParamsSize = 0;

	public FunNode(final ISymbolData data) {
		this(data, null);
	}
	
	public FunNode(final ISymbolData data, final LetInNode<T> body) {
		mData = data;
		mName = data.getSymbol();
		if (data.getType() instanceof IFunctionType) {
			mType = (IFunctionType) data.getType();
		} else {
			throw new IllegalStateException();
		}
		mData.setDefinitionNode(this);
		if (body != null) {
			setBody(body);
		}
	}

	public FunNode(final String name, final IFunctionType type) {
		mName = name;
		mType = type;
		mData = null;
	}
	
	public FunNode(final String name, final IFunctionType type, final LetInNode<T> body) {
		mName = name;
		mType = type;
		mData = null;
		if (body != null) {
			setBody(body);
		}
	}

	public FunNode(final String name, final IType returnType, final IType... paramTypes) {
		this(name, new FunctionType(returnType, paramTypes));
	}
	
	public FunNode(final String name, final IType returnType, final LetInNode<T> body, final IType... paramTypes) {
		this(name, new FunctionType(returnType, paramTypes), body);
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.IFunctionNode#addParam(it.unibo.gciatto.sdt.adt.impl.DeclarationNode)
	 */
	@Override
	public void addParam(final IDeclarationNode<T> dec) {
		mType.addParamType(dec.getType());
		insertDeclaration(dec, mParamsCount + (isExpressionSet() ? 1 : 0));
		mParamsCount++;
		mParamsSize += dec.getType().getSize();
	}
	
	protected int countParams() {
		return mParamsCount;
	}
	
	@Override
	public T evaluate() throws TypeException {
		return null;
	}

	@Override
	public ICompiler generate(final ICompiler compiler) {
		int variablesSize = 0;

		getDeclarations().forEach((final IASTNode<?> n) -> {
			if (n instanceof IFunctionNode) {
				n.generate(compiler);
			}
		});

		compiler.setNextPutListener(generateLabeller(generateFunctionLabel()));
		compiler.putInstruction("cfp");
		compiler.putInstruction("lra");

		for (final IASTNode<?> n : getDeclarations()) {
			if (n instanceof IVariableNode) {
				n.generate(compiler);
				variablesSize += n.getType().getSize();
			}

		}

		generateFunctionBody(compiler);

		compiler.putInstruction("srv");

		for (int i = 0; i < variablesSize; i++) {
			compiler.putInstruction("pop");
		}

		compiler.putInstruction("sra");

		compiler.putInstruction("pop");

		for (int i = 0; i < mParamsSize; i++) {
			compiler.putInstruction("pop");
		}

		compiler.putInstruction("sfp");

		compiler.putInstruction("lrv");
		compiler.putInstruction("lra");
		compiler.putInstruction("js");

		return compiler;
	}
	
	protected ICompiler generateFunctionBody(final ICompiler compiler) {
		getExpression().generate(compiler);
		return compiler;
	}

	@Override
	public String generateFunctionLabel() {
		return String.format("%s%s", getName(), getSymbolData().getLevel());
	}

	/*
	 * This is here for compatibility reasons
	 */
	@Deprecated
	public ILetInNode<T> getBody() {
		return this;
	}

	@Override
	public ICallNode getCallNode(final int currentNestingLevel) {
		// TODO implement
		throw new IllegalStateException("not implemented");
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.IFunctionNode#getName()
	 */
	@Override
	public String getName() {
		return mName;
	}

	@Override
	public int getNestingLevel() {
		return mData.getLevel();
	}

	@Override
	public ISymbolData getSymbolData() {
		return mData;
	}

	@Override
	public IFunctionType getType() {
		return mType;
	}

	@Override
	public int getVariableIndexByName(final String name) {
		int varIndex = 2;
		int argIndex = 0;
		for (final IASTNode<?> n : getDeclarations()) {
			if (n instanceof IVariableNode) {
				final IVariableNode<?> v = (IVariableNode<Object>) n;
				if (v.getName().equals(name)) {
					return varIndex;
				}
				varIndex += v.getType().getSize();
			} else if (n instanceof IDeclarationNode) {
				final IDeclarationNode<?> a = (IDeclarationNode<Object>) n;
				argIndex -= a.getType().getSize();
				if (a.getName().equals(name)) {
					return argIndex;
				}
			}
		}
		throw new RuntimeException("variable %s not found");
	}
	
	/*
	 * This is here for compatibility reasons
	 */
	@Deprecated
	public void setBody(final LetInNode<T> body) {
		int i = mParamsCount + (isExpressionSet() ? 1 : 0);
		for (int j = i; j < countChildren(); j++) {
			removeChild(i);
		}
		if (body.isExpressionSet()) {
			setExpression(body.getExpression());
		}
		for (i = body.isExpressionSet() ? 1 : 0; i < body.countChildren(); i++) {
			addDeclaration(body.getChild(i));
		}
	}

	@Override
	public void setExpression(final IASTNode<T> node) {
		if (!getType().getReturnType().isSupertypeOf(node.getType())) {
			throw new TypeException(getType().getReturnType(), node.getType());
		}
		super.setExpression(node);
	}

	@Override
	public String toString() {
		return String.format("fun(%s:%s)[%s]{%s}", mName, mType, letToString(getDeclarations(), 0), getExpression());
	}
}
