package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IClassNode;
import it.unibo.gciatto.sdt.adt.IDeclarationNode;
import it.unibo.gciatto.sdt.adt.IFieldNode;
import it.unibo.gciatto.sdt.adt.IFunctionNode;
import it.unibo.gciatto.sdt.adt.IIdNode;
import it.unibo.gciatto.sdt.adt.ILetInNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.scoping.ISymbolData;
import it.unibo.gciatto.sdt.scoping.SymbolType;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.vm.ICompiler;

public class IdNode<T> extends AbstractASTNode<T> implements IIdNode<T> {
	private final String mName;
	private final IType mType;
	private final ISymbolData mData;
	private final int mNestingLevel;

	public IdNode(final ISymbolData data, final int currentNestingLevel) {
		mData = data;
		mName = data.getSymbol();
		mType = data.getType();
		mNestingLevel = currentNestingLevel;
	}
	
	protected IdNode(final String name, final IType type, final int currentNestingLevel) {
		mName = name;
		mType = type;
		mNestingLevel = currentNestingLevel;
		mData = null;
	}
	
	@Override
	public T evaluate() throws TypeException {
		return null;
	}
	
	@Override
	public ICompiler generate(final ICompiler compiler) {
		if (getSymbolData().getSymbolType() == SymbolType.VAR) {
			if (mData.getDefinitionNode() instanceof IFieldNode) {
				final IFieldNode definition = (IFieldNode) mData.getDefinitionNode();
				final IClassNode clazz = definition.getParent();
				
				compiler.putInstruction("lfp");
				compiler.putInstruction("push", ObjectClassNode.getInstance().getInstancesType().getSize());
				compiler.putInstruction("sub");
				compiler.putInstruction("lw");
				compiler.putInstruction("push", clazz.getSymbolIndex(getName()));
				compiler.putInstruction("add");
				compiler.putInstruction("lw");
			} else if (mData.getDefinitionNode().getParent() instanceof ILetInNode) {
				final ILetInNode<?> definition = (ILetInNode<?>) mData.getDefinitionNode().getParent();
				final int index = definition.getVariableIndexByName(mName);
				compiler.putInstruction("push", index);
				
				generateALNavigation(compiler, mNestingLevel, mData);
				
				compiler.putInstruction("add");
				compiler.putInstruction("lw");
			}
		} else {
			if (mData.getDefinitionNode() instanceof IFunctionNode) {
				final IFunctionNode<?> definition = (IFunctionNode<?>) mData.getDefinitionNode();
				
				compiler.putInstruction("push", definition.generateFunctionLabel());
				
				// compiler.putInstruction("push", definition.getSymbolData().getLevel());
				compiler.putInstruction("lfp");
				// compiler.putInstruction("lw");
			} else if (mData.getDefinitionNode() instanceof IDeclarationNode) {
				final IDeclarationNode<?> definition = (IDeclarationNode<?>) mData.getDefinitionNode();
				final ILetInNode<?> inFunction = (ILetInNode<?>) definition.getParent();
				final int decIndex = inFunction.getVariableIndexByName(definition.getName());
				
				for (int i = 0; i < definition.getType().getSize(); i++) {
					// TODO check this
					generateALNavigation(compiler, mNestingLevel, mData);
					compiler.putInstruction("push", decIndex + i);
					compiler.putInstruction("add");
					compiler.putInstruction("lw");
				}

				// //compiler.putInstruction("lfp");
				// //TODO check this
				// generateALNavigation(compiler, mNestingLevel, mData);
				// compiler.putInstruction("push", decIndex);
				// compiler.putInstruction("add");
				// compiler.putInstruction("lw");
				// //compiler.putInstruction("lfp");
				// //check this
				// generateALNavigation(compiler, mNestingLevel, mData);
				// compiler.putInstruction("push", decIndex + 1);
				// compiler.putInstruction("add");
				// compiler.putInstruction("lw");
			} else {
				throw new IllegalStateException("undesigned case!");
			}
		}

		return compiler;

	}
	
	@Override
	public String getName() {
		return mName;
	}
	
	@Override
	public int getNestingLevel() {
		return mNestingLevel;
	}
	
	@Override
	public ISymbolData getSymbolData() {
		return mData;
	}
	
	@Override
	public IType getType() {
		return mType;
	}

	@Override
	public String toString() {
		return String.format("id(%s:%s)", mName, mType);
	}
	
}
