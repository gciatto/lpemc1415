package it.unibo.gciatto.sdt.adt.impl;

import static it.unibo.gciatto.sdt.typing.Types.INTEGER;
import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.vm.ICompiler;

public class IntegerNode extends AbstractASTNode<Integer> implements IASTNode<Integer> {

	public IntegerNode(final IASTNode<Integer> node) {
		this(node.evaluate());
	}
	
	public IntegerNode(final int value) {
		super(new Integer(value));
	}

	@Override
	public Integer evaluate() throws TypeException {
		return getValue();
	}

	@Override
	public ICompiler generate(final ICompiler compiler) {
		compiler.putInstruction("push", getValue());
		return compiler;
	}

	@Override
	public IType getType() {
		return INTEGER;
	}
	
	@Override
	public String toString() {
		return String.format("int(%s)", getValue());
	}
	
}
