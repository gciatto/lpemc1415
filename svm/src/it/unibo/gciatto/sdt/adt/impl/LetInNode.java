package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.adt.IClassNode;
import it.unibo.gciatto.sdt.adt.IDeclarationNode;
import it.unibo.gciatto.sdt.adt.IFunctionNode;
import it.unibo.gciatto.sdt.adt.IIdNode;
import it.unibo.gciatto.sdt.adt.ILetInNode;
import it.unibo.gciatto.sdt.adt.IVariableNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.scoping.IScope;
import it.unibo.gciatto.sdt.scoping.ISymbolData;
import it.unibo.gciatto.sdt.scoping.SymbolType;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.sdt.typing.Types;
import it.unibo.gciatto.utils.tree.ITreeNode;
import it.unibo.gciatto.vm.ICompiler;

import java.util.LinkedList;
import java.util.List;

public class LetInNode<T> extends AbstractASTNode<T> implements ILetInNode<T> {

	private boolean mIsExpressionSet = false;
	
	public LetInNode() {
		this(null, null, null);
	}
	
	public LetInNode(final IASTNode<T> expression) {
		this(null, null, expression);
	}

	public LetInNode(final IScope scope, final List<IClassNode> classes, final List<IASTNode<T>> declarations, final IASTNode<T> expression) {
		if (expression != null) {
			addChild(expression);
		}
		if (scope != null) {
			adjustScope(scope);
		}
		if (declarations != null && !declarations.isEmpty()) {
			declarations.forEach(d -> addChild(d));
		}
		
		if (classes != null && !classes.isEmpty()) {
			classes.forEach(c -> addChild((ITreeNode<T>) c));
		}
		mIsExpressionSet = expression != null;
	}
	
	public LetInNode(final List<IASTNode<T>> declarations, final IASTNode<T> expression) {
		this(null, declarations, expression);
	}
	
	public LetInNode(final List<IClassNode> classes, final List<IASTNode<T>> declarations, final IASTNode<T> expression) {
		this(null, classes, declarations, expression);
	}
	
	@Override
	public void addClass(final IClassNode clazz) {
		insertChild((ITreeNode<T>) clazz, countChildren());
	}
	
	@Override
	public void addClasses(final List<IClassNode> classes) {
		classes.forEach(c -> addClass(c));
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ILetInNode#addDeclaration(it.unibo.gciatto.sdt.adt.IASTNode)
	 */
	@Override
	public void addDeclaration(final IASTNode<T> declaration) {
		addChild(declaration);
	}

	@Override
	public void addDeclarations(final List<IASTNode<T>> declarations) {
		declarations.forEach(d -> addDeclaration(d));
	}
	
	private void adjustScope(final IScope scope) {
		final ObjectClassNode obj = ObjectClassNode.getInstance();
		
		scope.pushSymbol(ObjectClassNode.OBJECT_CLASS_NAME, obj.getType(), SymbolType.CLASS);
		scope.incLevel();
		scope.pushSymbol(ObjectClassNode.SELF_FIELD_NAME, Types.INTEGER);
		scope.pushSymbol(ObjectClassNode.SIZE_FIELD_NAME, Types.INTEGER);
		scope.pushSymbol(ObjectClassNode.ADDR_METHOD_NAME, Types.function(Types.INTEGER, obj.getInstancesType()), SymbolType.FUNC);
		scope.pushSymbol(ObjectClassNode.SIZE_METHOD_NAME, Types.function(Types.INTEGER, obj.getInstancesType()), SymbolType.FUNC);
		scope.decLevel();
		addChild((ITreeNode<T>) obj);

		// final ISymbolData data = scope.pushSymbol(NULL_NAME, Types.NULL_TYPE);
		// addDeclaration((IASTNode<T>) generateNull(data, scope.getLevel()));
	}
	
	protected int countFunctions() {
		int count = 0;
		for (final IASTNode<?> n : getDeclarations()) {
			if (n instanceof IFunctionNode) {
				count++;
			}
		}
		return count;
	}

	protected int countVariables() {
		int count = 0;
		for (final IASTNode<?> n : getDeclarations()) {
			if (n instanceof IVariableNode) {
				count++;
			}
		}
		return count;
	}
	
	@Override
	public T evaluate() throws TypeException {
		return null;
	}
	
	@Override
	public ICompiler generate(final ICompiler compiler) {

		// Prima inserisco tutte le funzioni definite
		getClasses().forEach(c -> c.generate(compiler));
		
		getDeclarations().forEach((final IASTNode<?> n) -> {
			if (n instanceof IFunctionNode) {
				n.generate(compiler);
			}
		});

		// Poi genero l'AR del main
		compiler.setNextPutListener(generateLabeller(LABEL_MAIN));
		compiler.putInstruction("push", 0); // RV

		// Non ci sono parametri da pushare

		// Pusho i valori delle variabili
		getDeclarations().forEach((final IASTNode<?> n) -> {
			if (!(n instanceof IFunctionNode)) {
				n.generate(compiler);
			}
		});
		// Alla fine di questo ciclo sullo stack ci sono i valori delle variabili

		// Genera il codice dell'espressione del main
		getExpression().generate(compiler);

		return compiler;
	}

	private IVariableNode<Object> generateNull(final ISymbolData data, final int currentScopeLevel) {
		/*
		 * final IConstructorCallNode ccn = ObjectClassNode.getInstance().getConstructor().getCallNode(currentScopeLevel); ccn.validate();
		 */
		final IVariableNode<Object> var = new VarNode<Object>(data, null) {

			@Override
			public ICompiler generate(final ICompiler compiler) {
				compiler.putInstruction("lhp");
				compiler.putInstruction("push", 2);
				compiler.putInstruction("sub");
				compiler.putInstruction("shp");
				compiler.putInstruction("lhp");
				compiler.putInstruction("lhp");
				compiler.putInstruction("sw");
				compiler.putInstruction("push", 2);
				compiler.putInstruction("lhp");
				compiler.putInstruction("push", 1);
				compiler.putInstruction("add");
				compiler.putInstruction("sw");
				compiler.putInstruction("lhp");
				return compiler;
			}
			
		};
		return var;
	}

	@Override
	public List<IClassNode> getClasses() {
		final List<IClassNode> list = new LinkedList<IClassNode>();
		getChildren().forEach(c -> {
			if (c instanceof IClassNode) {
				list.add((IClassNode) c);
			}
		});
		return list;
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ILetInNode#getDeclarations()
	 */
	@Override
	public List<IASTNode<T>> getDeclarations() {
		final List<IASTNode<T>> list = new LinkedList<IASTNode<T>>();
		getChildren().forEach(c -> {
			if (c instanceof IDeclarationNode || c instanceof IFunctionNode) {
				if (c instanceof IIdNode) {
					return;
				}
				list.add((IASTNode<T>) c);
			}
		});
		return list;
	}

	protected List<IASTNode<T>> getEverything() {
		final List<IASTNode<T>> list = new LinkedList<IASTNode<T>>();
		getChildren().forEach(c -> {
			if (c instanceof IDeclarationNode || c instanceof IFunctionNode || c instanceof IClassNode) {
				list.add((IASTNode<T>) c);
			}
		});
		return list;
	}

	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ILetInNode#getExpression()
	 */
	@Override
	public IASTNode<T> getExpression() {
		return mIsExpressionSet ? getChild(0) : null;
	}

	@Override
	public IType getType() {
		return TYPE_LET_IN;
	}

	@Override
	public int getVariableIndexByName(final String name) {
		int index = 2;
		for (final IASTNode<?> n : getDeclarations()) {
			if (n instanceof IVariableNode) {
				final IVariableNode<?> v = (IVariableNode<Object>) n;
				if (v.getName().equals(name)) {
					return index;
				}
				index += v.getType().getSize();
			}
			
		}
		throw new RuntimeException("variable %s not found");
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ILetInNode#insertDeclaration(it.unibo.gciatto.sdt.adt.IASTNode, int)
	 */
	@Override
	public void insertDeclaration(final IASTNode<T> declaration, final int index) {
		insertChild(declaration, index);
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ILetInNode#isExpressionSet()
	 */
	@Override
	public boolean isExpressionSet() {
		return mIsExpressionSet;
	}

	protected String letToString(final List<IASTNode<T>> let, final int i) {
		if (let.size() == 0) {
			return "no declaration";
		}
		if (i == let.size() - 1) {
			return let.get(i).toString();
		} else {
			return String.format("%s,%s", let.get(i), letToString(let, i + 1));
		}

	}

	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ILetInNode#setExpression(it.unibo.gciatto.sdt.adt.IASTNode)
	 */
	@Override
	public void setExpression(final IASTNode<T> node) {
		if (mIsExpressionSet) {
			removeChild(0);
		}
		if (node != null) {
			insertChild(node, 0);
		}
		mIsExpressionSet = node != null;
	}

	@Override
	public String toString() {
		return String.format("let(%s)in(%s):%s", letToString(getEverything(), 0), getExpression(), getType());
	}

	protected int variablesSize() {
		int count = 0;
		for (final IASTNode<?> n : getDeclarations()) {
			if (n instanceof IVariableNode) {
				count += n.getType().getSize();
			}
		}
		return count;
	}

}
