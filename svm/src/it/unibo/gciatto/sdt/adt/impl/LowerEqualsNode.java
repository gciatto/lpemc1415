package it.unibo.gciatto.sdt.adt.impl;

import static it.unibo.gciatto.sdt.typing.Types.BOOLEAN;
import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.vm.ICompiler;

import java.util.Objects;

public class LowerEqualsNode extends BinaryOperationNode {
	
	public LowerEqualsNode(final boolean value1, final boolean value2) {
		super(value1, value2);
	}
	
	public LowerEqualsNode(final IASTNode<?> value1, final IASTNode<?> value2) {
		super(value1, value2);
	}
	
	public LowerEqualsNode(final int value1, final int value2) {
		super(value1, value2);
	}
	
	@Override
	public Object evaluate() throws TypeException {
		final Integer val1 = (Integer) getFirstValue().evaluate();
		final Integer val2 = (Integer) getSecondValue().evaluate();
		if (Objects.nonNull(val1) && Objects.nonNull(val2)) {
			setValue(val1 <= val2);
		} else {
			setValue(null);
		}
		return getValue();
	}
	
	@Override
	public ICompiler generate(final ICompiler compiler) {
		super.generate(compiler);

		final String[] labels = generateLabels(2);

		compiler.putInstruction("beless", labels[0]);
		compiler.putInstruction("push", 0);
		compiler.putInstruction("b", labels[1]);
		compiler.setNextPutListener(generateLabeller(labels[0]));
		compiler.putInstruction("push", 1);
		compiler.setNextPutListener(generateLabeller(labels[1]));

		return compiler;
	}

	@Override
	protected String getOpName() {
		return "lower_equal";
	}
	
	@Override
	protected void init(final IASTNode<?> value1, final IASTNode<?> value2) {
		setType(BOOLEAN);
	}
}
