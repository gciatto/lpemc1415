package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.adt.IIdNode;
import it.unibo.gciatto.sdt.adt.IMethodCallNode;
import it.unibo.gciatto.sdt.adt.IMethodNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.scoping.ISymbolData;
import it.unibo.gciatto.sdt.typing.IFunctionType;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.vm.ICompiler;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MethodCallNode extends CallNode<Object> implements IMethodCallNode {
	protected static IASTNode<Object>[] arrayAddFirstElement(final IASTNode<Object> first, final IASTNode<Object>... others) {
		if (first != null) {
			final List<IASTNode<Object>> list = new LinkedList<IASTNode<Object>>(Arrays.asList(others));
			list.add(0, first);
			return list.toArray(new IASTNode[0]);
		} else {
			return others;
		}
	}

	private final IIdNode<?> mOverObj;
	
	private final IMethodNode mDefinition;
	
	@SafeVarargs
	public MethodCallNode(final IIdNode<?> overObject, final ISymbolData data, final int currentNestinLevel, final IASTNode<Object>... actuals) {
		super(data, currentNestinLevel, arrayAddFirstElement((IASTNode<Object>) overObject, actuals));
		mOverObj = overObject;
		checkType();
		mDefinition = null;
	}

	@SafeVarargs
	public MethodCallNode(final IMethodNode definition, final IIdNode<?> overObject, final String name, final IFunctionType type, final int currentNestinLevel, final IASTNode<Object>... actuals) {
		super(name, type, currentNestinLevel, arrayAddFirstElement((IASTNode<Object>) overObject, actuals));
		mOverObj = overObject;
		mDefinition = definition;
		if (mOverObj == null) {
			getFunctionType().insertParamType(0, mDefinition.getParent().getInstancesType());
		}
		checkType();
	}
	
	protected boolean checkType() throws TypeException {
		// TODO implement
		return true;
	}

	@Override
	protected ICompiler generateCall(final ICompiler compiler) {
		// Uso RA come registro d'appoggio per il riferimento all'oggetto su cui chiamo il metodo
		compiler.putInstruction("sra");
		compiler.putInstruction("lra");
		
		final String[] labels = generateLabels(3);
		final int methodIndex = mDefinition.getParent().getSymbolIndex(getName());

		// compiler.putInstruction("lfp"); //FP**1 corrente (AL)
		// compiler.putInstruction("push", getNestingLevel()); //Nesting level dello scope corrente
		//
		// compiler.putInstruction("lra");
		// compiler.putInstruction("push", methodIndex + 1);
		// compiler.putInstruction("add");
		// compiler.putInstruction("lw"); //Recupero il nesting level della funzione chiamata
		// compiler.putInstruction("sub"); //Faccio la differenza di nesting
		//
		// //Inizia un loop dove uso il registro RV come registro d'appoggio
		// //in questo contesto il valore precedente di RV è inutile.
		// compiler.setNextPutListener(generateLabeller(labels[0])); //Label loop
		// compiler.putInstruction("srv");
		// compiler.putInstruction("lrv");
		// compiler.putInstruction("push", 0);
		// //Se la differenza è maggiore di zero vado a labels[1]
		// compiler.putInstruction("bmore", labels[1]);
		// //altrimenti vado a labels[2]
		// compiler.putInstruction("b", labels[2]);
		//
		// //Se la differenza è maggiore di zero...
		// compiler.setNextPutListener(generateLabeller(labels[1])); //Label ifMore
		// compiler.putInstruction("lw"); //AL = CL precedente = lw(FP**1)
		// compiler.putInstruction("lrv");
		// compiler.putInstruction("push", 1);
		// compiler.putInstruction("sub"); //Decremento la differenza di 1
		// compiler.putInstruction("b", labels[0]); // Ricomincio il ciclo
		//
		// //Se la differenza è minore o uguale a zero...
		// compiler.setNextPutListener(generateLabeller(labels[2]));
		compiler.putInstruction("push", methodIndex + 1);
		compiler.putInstruction("lra");
		compiler.putInstruction("add");
		compiler.putInstruction("lw");
		
		compiler.putInstruction("push", methodIndex);
		compiler.putInstruction("lra");
		compiler.putInstruction("add");
		compiler.putInstruction("lw"); // Pusho l'indirizzo della funzione
		
		compiler.putInstruction("js");
		
		return compiler;
	}

	@Override
	protected ICompiler generateFirstParameter(final ICompiler compiler) {
		if (mOverObj == null) {
			compiler.putInstruction("lfp");
			compiler.putInstruction("push", mDefinition.getParent().getInstancesType().getSize());
			compiler.putInstruction("sub");
			compiler.putInstruction("lw");
		}
		return compiler;
	}
	
	@Override
	public List<IType> getActualTypes() {
		final List<IType> res = super.getActualTypes();
		if (mOverObj == null && mDefinition != null) {
			res.add(0, mDefinition.getParent().getInstancesType());
		}
		return res;
	}

	@Override
	public IIdNode<?> getOverObject() {
		return mOverObj;
	}

	@Override
	public boolean validate() throws TypeException {
		final List<IType> paramTypes = mDefinition.getType().getParamsTypes();
		return validate(paramTypes);
	}
	
}
