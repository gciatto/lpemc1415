package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IClassNode;
import it.unibo.gciatto.sdt.adt.IIdNode;
import it.unibo.gciatto.sdt.adt.IMethodCallNode;
import it.unibo.gciatto.sdt.adt.IMethodNode;
import it.unibo.gciatto.sdt.scoping.ISymbolData;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.sdt.typing.Types;
import it.unibo.gciatto.vm.ICompiler;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MethodNode extends FunNode<Object> implements IMethodNode {
	
	protected static IType[] arrayAddFirstElement(final IType first, final IType... others) {
		final List<IType> list = new LinkedList<IType>(Arrays.asList(others));
		list.add(0, first);
		return list.toArray(new IType[0]);
	}

	private boolean mGenerated = false;
	
	public MethodNode(final ISymbolData data, final IClassNode clazz) {
		super(data);
		addParam(new DeclarationNode<Object>(FIRST_PARAM_NAME, clazz.getInstancesType()));
	}

	public MethodNode(final String name, final IClassNode clazz, final IType returnType, final IType... paramTypes) {
		super(name, returnType, arrayAddFirstElement(clazz.getInstancesType(), paramTypes));
		
	}

	@Override
	public ICompiler generate(final ICompiler compiler) {
		if (!mGenerated) {
			mGenerated = true;
			return super.generate(compiler);
		} else {
			return compiler;
		}
		
	}
	
	@Override
	public String generateFunctionLabel() {
		return String.format("%s_%s", getParent().getName(), getName());
	}
	
	@Override
	public IMethodCallNode getCallNode(final IIdNode<?> overObject, final int currentNestingLevel) {
		return new MethodCallNode(this, overObject, getName(), Types.function(getType().getReturnType()), currentNestingLevel);
	}

	@Override
	public IClassNode getParent() {
		return (IClassNode) super.getParent();
	}
}
