package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.vm.ICompiler;

import java.util.Objects;

public class MultNode extends BinaryOperationNode {
	
	public MultNode(final IASTNode<?> value1, final IASTNode<?> value2) {
		super(value1, value2);
	}
	
	public MultNode(final int value1, final int value2) {
		super(value1, value2);
	}

	@Override
	public Object evaluate() throws TypeException {
		final Integer val1 = (Integer) getFirstValue().evaluate();
		final Integer val2 = (Integer) getSecondValue().evaluate();
		if (Objects.nonNull(val1) && Objects.nonNull(val2)) {
			setValue(val1 * val2);
		} else {
			setValue(null);
		}
		return getValue();
	}

	@Override
	public ICompiler generate(final ICompiler compiler) {
		super.generate(compiler);
		compiler.putInstruction("mult");
		return compiler;
	}
	
	@Override
	protected String getOpName() {
		return "mult";
	}
}
