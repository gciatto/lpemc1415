package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.sdt.typing.Types;
import it.unibo.gciatto.vm.ICompiler;

public class NotNode extends UnaryOperationNode {

	public NotNode(final boolean value) {
		super(value);
	}

	public NotNode(final IASTNode<?> value) {
		super(value);
	}

	public NotNode(final int value) {
		super(value);
	}

	@Override
	protected boolean checkType(final IType type1) throws TypeException {
		if (!Types.BOOLEAN.isSupertypeOf(type1)) {
			throw new TypeException(Types.BOOLEAN, type1);
		}
		return true;
	}

	@Override
	public Object evaluate() throws TypeException {
		final Integer val = (Integer) getFirstValue().getValue();
		if (val != null) {
			final boolean boolVal = ASTUtils.intToBoolean(val);
			
			setValue(!boolVal);
		} else {
			setValue(null);
		}
		return getValue();
	}

	@Override
	public ICompiler generate(final ICompiler compiler) {
		super.generate(compiler);
		compiler.putInstruction("push", 1);
		compiler.putInstruction("xor");
		return compiler;
	}

	@Override
	protected String getOpName() {
		return "not";
	}
	
	@Override
	public IType getType() {
		return Types.BOOLEAN;
	}
}
