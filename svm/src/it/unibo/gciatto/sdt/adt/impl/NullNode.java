package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.INullNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.scoping.IScope;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.sdt.typing.Types;
import it.unibo.gciatto.vm.ICompiler;

public class NullNode extends IdNode<Object> implements INullNode {
	
	public NullNode(final IScope scope) {
		super(NULL_NAME, Types.NULL_TYPE, scope.getLevel());
	}
	
	@Override
	public Object evaluate() throws TypeException {
		return "null";
	}
	
	@Override
	public ICompiler generate(final ICompiler compiler) {
		compiler.putInstruction("push", -1);
		return compiler;
	}

	@Override
	public IType getType() {
		return Types.NULL_TYPE;
	}
	
}
