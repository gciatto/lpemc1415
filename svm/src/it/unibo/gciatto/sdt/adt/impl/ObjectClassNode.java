package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.typing.Types;
import it.unibo.gciatto.vm.ICompiler;

public class ObjectClassNode extends ClassNode {
	public static ObjectClassNode getInstance() {
		return INSTANCE;
	}

	public static final String OBJECT_CLASS_NAME = "Object";
	public static final String SELF_FIELD_NAME = "$this$";
	public static final String SIZE_FIELD_NAME = "$size$";
	public static final String ADDR_METHOD_NAME = "getAddress";
	
	public static final String SIZE_METHOD_NAME = "getSize";

	private static final ObjectClassNode INSTANCE = new ObjectClassNode();

	private ObjectClassNode() {
		super(OBJECT_CLASS_NAME, null);
		addField(new FieldNode(SELF_FIELD_NAME, Types.INTEGER));
		addField(new FieldNode(SIZE_FIELD_NAME, Types.INTEGER));
		addMethod(generateGetAddress());
		addMethod(generateGetSize());
	}
	
	protected MethodNode generateGetAddress() {
		final MethodNode method = new MethodNode(ADDR_METHOD_NAME, this, Types.INTEGER) {

			@Override
			protected ICompiler generateFunctionBody(final ICompiler compiler) {
				compiler.putInstruction("lfp");
				compiler.putInstruction("push", ObjectClassNode.getInstance().getInstancesType().getSize());
				compiler.putInstruction("sub");
				compiler.putInstruction("lw");
				compiler.putInstruction("push", getSymbolIndex(SELF_FIELD_NAME));
				compiler.putInstruction("add");
				compiler.putInstruction("lw");
				return compiler;
			}

			@Override
			public int getNestingLevel() {
				return 1;
			}
			
		};
		return method;
	}
	
	protected MethodNode generateGetSize() {
		final MethodNode method = new MethodNode(SIZE_METHOD_NAME, this, Types.INTEGER) {

			@Override
			protected ICompiler generateFunctionBody(final ICompiler compiler) {
				compiler.putInstruction("lfp");
				compiler.putInstruction("push", ObjectClassNode.getInstance().getInstancesType().getSize());
				compiler.putInstruction("sub");
				compiler.putInstruction("lw");
				compiler.putInstruction("push", getSymbolIndex(SIZE_FIELD_NAME));
				compiler.putInstruction("add");
				compiler.putInstruction("lw");
				return compiler;
			}

			@Override
			public int getNestingLevel() {
				return 1;
			}

		};
		return method;
	}

}
