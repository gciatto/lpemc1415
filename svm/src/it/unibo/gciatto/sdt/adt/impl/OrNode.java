package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.sdt.typing.Types;
import it.unibo.gciatto.vm.ICompiler;

import java.util.Objects;

public class OrNode extends BinaryOperationNode {
	
	public OrNode(final boolean value1, final boolean value2) {
		super(value1, value2);
	}
	
	public OrNode(final IASTNode<?> value1, final IASTNode<?> value2) {
		super(value1, value2);
	}
	
	@Override
	protected boolean checkType(final IType type1, final IType type2) throws TypeException {
		if (!type1.isSameTypeOf(Types.BOOLEAN)) {
			throw new TypeException(Types.BOOLEAN, type1);
		}
		if (!type2.isSameTypeOf(Types.BOOLEAN)) {
			throw new TypeException(Types.BOOLEAN, type2);
		}
		return true;
	}
	
	@Override
	public Object evaluate() throws TypeException {
		final Integer val1 = (Integer) getFirstValue().evaluate();
		final Integer val2 = (Integer) getSecondValue().evaluate();
		if (Objects.nonNull(val1) && Objects.nonNull(val2)) {
			setValue(ASTUtils.intToBoolean(val1 | val2));
		} else {
			setValue(null);
		}
		return getValue();
	}

	@Override
	public ICompiler generate(final ICompiler compiler) {
		super.generate(compiler);
		compiler.putInstruction("or");
		return compiler;
	}

	@Override
	protected String getOpName() {
		return "or";
	}
	
	@Override
	protected void init(final IASTNode<?> value1, final IASTNode<?> value2) {
		setType(Types.BOOLEAN);
	}
}
