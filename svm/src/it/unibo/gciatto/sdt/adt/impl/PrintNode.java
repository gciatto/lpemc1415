package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.sdt.typing.impl.Type;
import it.unibo.gciatto.vm.ICompiler;

public class PrintNode extends UnaryOperationNode {
	
	public static final IType PRINT = new Type("$print$");
	
	public PrintNode(final boolean value) {
		super(value);
	}
	
	public PrintNode(final IASTNode<?> value) {
		super(value);
	}
	
	public PrintNode(final int value) {
		super(value);
	}
	
	@Override
	public Object evaluate() throws TypeException {
		final Object eval = getFirstValue().evaluate();
		System.out.println(eval);
		setValue(eval);
		return eval;
	}

	@Override
	public ICompiler generate(final ICompiler compiler) {
		super.generate(compiler);
		compiler.putInstruction("print");

		return compiler;
	}

	@Override
	protected String getOpName() {
		return "print";
	}
	
	@Override
	public IType getType() {
		return getFirstValue().getType();
	}
	
}
