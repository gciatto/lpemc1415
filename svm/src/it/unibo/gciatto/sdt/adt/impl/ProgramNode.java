package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.adt.ILetInNode;
import it.unibo.gciatto.sdt.adt.IProgramNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.vm.ICompiler;

public class ProgramNode extends AbstractASTNode<Object> implements IProgramNode {

	public ProgramNode() {
		super(1);
	}
	
	public ProgramNode(final IASTNode<Object> child) {
		super(1);
		addChild(child);
	}
	
	@Override
	public Object evaluate() throws TypeException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public ICompiler generate(final ICompiler compiler) {
		compiler.putInstruction("b", LABEL_MAIN);
		getBody().generate(compiler);
		compiler.putInstruction("halt");
		return compiler;
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.IProgramNode#getBody()
	 */
	@Override
	public ILetInNode<Object> getBody() {
		return (ILetInNode<Object>) getChild(0);
	}

	@Override
	public IType getType() {
		return TYPE_PROGRAM;
	}

	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.IProgramNode#setBody(it.unibo.gciatto.sdt.adt.IASTNode)
	 */
	@Override
	public void setBody(final ILetInNode<Object> body) {
		if (countChildren() > 0) {
			removeChild(0);
		}
		addChild(body);
	}
	
	@Override
	public String toString() {
		return String.format("program(%s)", getBody());
	}
	
}
