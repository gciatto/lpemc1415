package it.unibo.gciatto.sdt.adt.impl;

import static it.unibo.gciatto.sdt.typing.Types.BOOLEAN;
import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.adt.ITernaryNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.utils.tree.ITreeNode;
import it.unibo.gciatto.vm.ICompiler;

public class TernaryNode extends AbstractASTNode<Object> implements IASTNode<Object>, ITernaryNode {
	public static void main(final String args[]) {
		final TernaryNode t = new TernaryNode(true, 1, 2);
		System.out.println(t.evaluate());
	}

	private IASTNode<?> mCondition, mExprTrue, mExprFalse;

	private IType mType;
	
	public TernaryNode(final boolean condition, final boolean ifTrue, final boolean ifFalse) {
		this(new BooleanNode(condition), new BooleanNode(ifTrue), new BooleanNode(ifFalse));
	}
	
	public TernaryNode(final boolean condition, final int ifTrue, final int ifFalse) {
		this(new BooleanNode(condition), new IntegerNode(ifTrue), new IntegerNode(ifFalse));
	}

	@SuppressWarnings({
			"rawtypes", "unchecked"
	})
	public TernaryNode(final IASTNode<?> condition, final IASTNode<?> ifTrue, final IASTNode<?> ifFalse) {
		mCondition = condition;
		mExprTrue = ifTrue;
		mExprFalse = ifFalse;
		// mType = ifTrue.getType();
		addChild((ITreeNode) mCondition);
		addChild((ITreeNode) mExprTrue);
		addChild((ITreeNode) mExprFalse);
		
		if (checkTypes()) {
			setType();
		}
	}

	// FIXME fix this
	protected boolean checkTypes() {
		if (mCondition.getType().equals(BOOLEAN) && mExprTrue.getType().equals(mExprFalse.getType())) {
			return true;
		} else {
			throw new TypeException(mExprTrue.getType(), mExprFalse.getType());
		}
	}
	
	@Override
	public Object evaluate() throws TypeException {
		if (mExprTrue.getType().equals(mExprFalse.getType())) {
			final Object val = (Boolean) mCondition.evaluate() ? mExprTrue.evaluate() : mExprFalse.evaluate();
			setValue(val);
			return val;
		} else {
			throw new TypeException(mExprTrue.getType(), mExprFalse.getType());
		}
	}
	
	@Override
	public ICompiler generate(final ICompiler compiler) {
		getCondition().generate(compiler);

		compiler.putInstruction("push", 1);

		final String[] labels = generateLabels(2);

		compiler.putInstruction("beq", labels[0]);

		getExprFalse().generate(compiler);

		compiler.putInstruction("b", labels[1]);

		compiler.setNextPutListener(generateLabeller(labels[0]));

		getExprTrue().generate(compiler);

		compiler.setNextPutListener(generateLabeller(labels[1]));

		return compiler;
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ITernaryNode#getCondition()
	 */
	@Override
	public IASTNode<?> getCondition() {
		return mCondition;
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ITernaryNode#getExprFalse()
	 */
	@Override
	public IASTNode<?> getExprFalse() {
		return mExprFalse;
	}

	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ITernaryNode#getExprTrue()
	 */
	@Override
	public IASTNode<?> getExprTrue() {
		return mExprTrue;
	}

	@Override
	public IType getType() {
		return mType;
	}

	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ITernaryNode#setCondition(it.unibo.gciatto.sdt.adt.IASTNode)
	 */
	@Override
	public void setCondition(final IASTNode<Object> condition) {
		mCondition = condition;
		removeChild(0);
		insertChild(condition, 0);
		checkTypes();
	}

	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ITernaryNode#setExprFalse(it.unibo.gciatto.sdt.adt.IASTNode)
	 */
	@Override
	public void setExprFalse(final IASTNode<Object> exprFalse) {
		mExprFalse = exprFalse;
		removeChild(2);
		insertChild(exprFalse, 2);
		checkTypes();
	}

	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.ITernaryNode#setExprTrue(it.unibo.gciatto.sdt.adt.IASTNode)
	 */
	@Override
	public void setExprTrue(final IASTNode<Object> exprTrue) {
		mExprTrue = exprTrue;
		removeChild(1);
		insertChild(exprTrue, 1);
		checkTypes();
	}

	protected void setType() {
		mType = mExprTrue.getType();
	}
	
	@Override
	public String toString() {
		return String.format("ternary(%s,%s,%s):%s", mCondition, mExprTrue, mExprFalse, getType());
	}
	
}
