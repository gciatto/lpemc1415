package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.adt.IUnaryOperationNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.utils.tree.ITreeNode;
import it.unibo.gciatto.vm.ICompiler;

public abstract class UnaryOperationNode extends AbstractASTNode<Object> implements IUnaryOperationNode<Object> {
	
	public UnaryOperationNode(final boolean value) {
		this(new BooleanNode(value));
	}
	
	@SuppressWarnings({
			"unchecked", "rawtypes"
	})
	public UnaryOperationNode(final IASTNode<?> value) {
		super(value);
		if (checkType(value.getType())) {
			addChild((ITreeNode) value);
		}
		init(value);
	}
	
	public UnaryOperationNode(final int value) {
		this(new IntegerNode(value));
	}
	
	protected boolean checkType(final IType type1) throws TypeException {
		// if (type1.isSameTypeOf(Types.BOOLEAN)) {
		// return true;
		// }
		// throw new TypeException(Types.BOOLEAN, type1);
		return true;
	}
	
	@Override
	public abstract Object evaluate() throws TypeException;
	
	@Override
	public ICompiler generate(final ICompiler compiler) {
		getFirstValue().generate(compiler);

		return compiler;
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.IUnaryOperationNode#getFirstValue()
	 */
	@Override
	public IASTNode<?> getFirstValue() {
		return getChild(0);
	}
	
	protected abstract String getOpName();
	
	protected void init(final IASTNode<?> value1) {
		
	}
	
	@Override
	public String toString() {
		return String.format("%s(%s):%s", getOpName(), childrenToString(0), getType());
	}
}
