package it.unibo.gciatto.sdt.adt.impl;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.adt.IVariableNode;
import it.unibo.gciatto.sdt.scoping.ISymbolData;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.vm.ICompiler;

public class VarNode<T> extends DeclarationNode<T> implements IVariableNode<T> {
	public VarNode(final ISymbolData data, final IASTNode<T> expression) {
		super(data);
		if (expression != null) {
			addChild(expression);
		}
	}

	public VarNode(final String name, final IType type, final IASTNode<T> expression) {
		super(name, type);
		if (expression != null) {
			addChild(expression);
		}
	}
	
	@Override
	public ICompiler generate(final ICompiler compiler) {
		getExpression().generate(compiler);
		return compiler;
	}
	
	/*
	 * (non-Javadoc)
	 * @see it.unibo.gciatto.sdt.adt.impl.IVariableNode#getExpression()
	 */
	@Override
	public IASTNode<T> getExpression() {
		return countChildren() > 0 ? getChild(0) : null;
	}

	@Override
	public String toString() {
		return String.format("var(%s:%s = %s)", getName(), getType(), getExpression());
	}
	
}
