package it.unibo.gciatto.sdt.exception;

public class MultiplyDeclaredException extends RuntimeException {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 8850536103701321313L;

	private static final String MSG = "Symbol '%s' is already declared into an inner scope.";
	
	private final String mSymbol;
	
	public MultiplyDeclaredException(final String symbol) {
		super(String.format(MSG, symbol));
		mSymbol = symbol;
	}

	public MultiplyDeclaredException(final String symbol, final Throwable th) {
		super(String.format(MSG, symbol), th);
		mSymbol = symbol;
	}
}
