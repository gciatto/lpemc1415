package it.unibo.gciatto.sdt.exception;

public class NotDeclaredException extends RuntimeException {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 8850536103701321313L;

	private static final String MSG = "Symbol '%s' is not declared in this scope nor any inner one.";
	
	private final String mSymbol;
	
	public NotDeclaredException(final String symbol) {
		super(String.format(MSG, symbol));
		mSymbol = symbol;
	}

	public NotDeclaredException(final String symbol, final Throwable th) {
		super(String.format(MSG, symbol), th);
		mSymbol = symbol;
	}
}
