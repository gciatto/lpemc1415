package it.unibo.gciatto.sdt.exception;

import it.unibo.gciatto.sdt.typing.IType;

public class TypeException extends RuntimeException {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 8850536103701321313L;

	private static final String MSG = "TypeException: expected type '%s' while actual type is '%s'";
	
	private IType mExpected, mActual;

	public TypeException() {
		// TODO Auto-generated constructor stub
	}
	
	public TypeException(final IType expected) {
		super(String.format(MSG, expected, "different"));
		mExpected = expected;
		mActual = null;
	}

	public TypeException(final IType expected, final IType actual) {
		this(expected, actual, null);
	}

	public TypeException(final IType expected, final IType actual, final Throwable cause) {
		super(String.format(MSG, expected, actual), cause);
		mExpected = expected;
		mActual = actual;
	}

	@Override
	public String toString() {
		return String.format(MSG, mExpected, mActual);
	}
}
