package it.unibo.gciatto.sdt.parsing;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.utils.SizeUnit;
import it.unibo.gciatto.vm.ICompiler;
import it.unibo.gciatto.vm.IProgram;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.impl.Ram32Bit;
import it.unibo.gciatto.vm.stack.IStack;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;
import it.unibo.gciatto.vm.stack.impl.Stack32Bit;
import it.unibo.gciatto.vm.svm.SVMCompiler;
import it.unibo.gciatto.vm.svm.SVMCpu;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;

public class ExecuteFOOL {

	public static void main(final String[] args) throws Throwable {
		Logger.getGlobal().setLevel(Level.OFF);
		final String code = "./foolcode/quicksort.fool";
		final ANTLRStringStream stream = new ANTLRStringStream(PROVA); // new ANTLRFileStream(code);
		final FOOLLexer lexer = new FOOLLexer(stream);
		final CommonTokenStream token = new CommonTokenStream(lexer);
		final FOOLParser parser = new FOOLParser(token);
		final IASTNode<?> node = parser.prog();
		node.evaluate();
		System.out.println(node);
		final ICompiler compiler = new SVMCompiler();
		final IProgram prog = node.generate(compiler).compile();
		// System.out.println(prog);
		final IRandomAccessMemory mem = new Ram32Bit(1, SizeUnit.MiB);
		final IStack stack = new Stack32Bit(mem, IStack.Direction.POSITIVE, prog.getLength());
		final IStackBasedCpu cpu = new SVMCpu(mem, stack);
		prog.execute(cpu);
	}
	
	public static final String PROVA = "let  var x:int = 5+3; fun f:bool (n:int, m:int) let var x:bool = false;   in     x==(n==m);    in    print (    if f(7,8) then { false }     else { true } ) ;";

	public static final String PROVA1 = "print (if  77 == true then {3 + 2 + 1 + 1} else {70});";

	public static final String PROVA2 = "let fun stampa:int (x:int, y:int) let var fuffa:int = 2; in print((x + y) * fuffa); in stampa(666, 111);";

	public static final String PROVA3 = "let fun cube:int (n:int) let fun square:int (n:int) let in n*n;  in n*square(n); in print(cube(5));";

	public static final String PROVA4 = "let fun cube:int (n:int) let in n*n*n; fun f:int() cube(5); fun g:int(x:()->int) print(x()); in g(f);";
	
}