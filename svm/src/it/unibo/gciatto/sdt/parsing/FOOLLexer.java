// $ANTLR 3.4 /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g 2015-02-09 13:25:21

package it.unibo.gciatto.sdt.parsing;

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.DFA;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.Lexer;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;

@SuppressWarnings({
	"all", "warnings", "unchecked"
})
public class FOOLLexer extends Lexer {
	class DFA8 extends DFA {

		public DFA8(final BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			decisionNumber = 8;
			eot = DFA8_eot;
			eof = DFA8_eof;
			min = DFA8_min;
			max = DFA8_max;
			accept = DFA8_accept;
			special = DFA8_special;
			transition = DFA8_transition;
		}

		@Override
		public String getDescription() {
			return "1:1: Tokens : ( PLUS | MINUS | MULT | DIV | LPAR | RPAR | CLPAR | CRPAR | SEMIC | COLON | COMMA | DOT | XOR | OR | AND | NOT | GE | LE | GT | LW | EQ | NE | ASS | TRUE | FALSE | IF | THEN | ELSE | PRINT | LET | IN | VAR | FUN | CLASS | EXTENDS | NEW | NULL | INT | BOOL | ARROW | INTEGER | ID | WHITESP | COMMENT );";
		}
	}

	public static final int EOF = -1;
	public static final int AND = 4;
	public static final int ARROW = 5;
	public static final int ASS = 6;
	public static final int BOOL = 7;
	public static final int CLASS = 8;
	public static final int CLPAR = 9;
	public static final int COLON = 10;
	public static final int COMMA = 11;
	public static final int COMMENT = 12;
	public static final int CRPAR = 13;
	public static final int DIV = 14;
	public static final int DOT = 15;
	public static final int ELSE = 16;
	public static final int EQ = 17;
	public static final int EXTENDS = 18;
	public static final int FALSE = 19;
	public static final int FUN = 20;
	public static final int GE = 21;
	public static final int GT = 22;
	public static final int ID = 23;
	public static final int IF = 24;
	public static final int IN = 25;
	public static final int INT = 26;
	public static final int INTEGER = 27;
	public static final int LE = 28;
	public static final int LET = 29;
	public static final int LPAR = 30;
	public static final int LW = 31;
	public static final int MINUS = 32;
	public static final int MULT = 33;
	public static final int NE = 34;
	public static final int NEW = 35;
	public static final int NOT = 36;
	public static final int NULL = 37;
	public static final int OR = 38;
	public static final int PLUS = 39;
	public static final int PRINT = 40;
	public static final int RPAR = 41;
	public static final int SEMIC = 42;
	public static final int THEN = 43;
	public static final int TRUE = 44;
	public static final int VAR = 45;
	public static final int WHITESP = 46;

	public static final int XOR = 47;

	protected DFA8 dfa8 = new DFA8(this);
	static final String DFA8_eotS = "\2\uffff\1\44\1\uffff\1\46\10\uffff\2\41\1\uffff\1\41\1\uffff\1" + "\41\1\56\1\60\1\62\1\uffff\11\41\7\uffff\1\41\1\17\4\41\6\uffff" + "\4\41\1\111\1\113\7\41\1\123\1\21\1\124\1\125\4\41\1\132\1\uffff" + "\1\133\1\uffff\3\41\1\137\1\140\2\41\3\uffff\1\143\1\144\1\145\1" + "\41\2\uffff\1\147\2\41\2\uffff\1\41\1\153\3\uffff\1\154\1\uffff" + "\1\41\1\156\1\157\2\uffff\1\41\2\uffff\1\161\1\uffff";
	static final String DFA8_eofS = "\162\uffff";
	static final String DFA8_minS = "\1\11\1\uffff\1\76\1\uffff\1\52\10\uffff\1\157\1\162\1\uffff\1\156" + "\1\uffff\1\145\3\75\1\uffff\1\150\1\141\1\146\1\154\1\162\1\145" + "\1\141\1\154\1\157\7\uffff\1\162\1\60\1\144\1\164\1\167\1\154\6" + "\uffff\1\165\1\145\1\154\1\156\2\60\1\163\1\164\1\151\1\164\1\162" + "\1\141\1\157\4\60\1\154\1\145\1\156\1\163\1\60\1\uffff\1\60\1\uffff" + "\2\145\1\156\2\60\1\163\1\154\3\uffff\3\60\1\145\2\uffff\1\60\1" + "\156\1\164\2\uffff\1\163\1\60\3\uffff\1\60\1\uffff\1\144\2\60\2" + "\uffff\1\163\2\uffff\1\60\1\uffff";

	static final String DFA8_maxS = "\1\175\1\uffff\1\76\1\uffff\1\52\10\uffff\1\157\1\162\1\uffff\1" + "\156\1\uffff\1\165\3\75\1\uffff\1\162\1\165\1\156\1\170\1\162\1" + "\145\1\141\1\154\1\157\7\uffff\1\162\1\172\1\144\1\164\1\167\1\154" + "\6\uffff\1\165\1\145\1\154\1\156\2\172\1\163\1\164\1\151\1\164\1" + "\162\1\141\1\157\4\172\1\154\1\145\1\156\1\163\1\172\1\uffff\1\172" + "\1\uffff\2\145\1\156\2\172\1\163\1\154\3\uffff\3\172\1\145\2\uffff" + "\1\172\1\156\1\164\2\uffff\1\163\1\172\3\uffff\1\172\1\uffff\1\144" + "\2\172\2\uffff\1\163\2\uffff\1\172\1\uffff";

	static final String DFA8_acceptS = "\1\uffff\1\1\1\uffff\1\3\1\uffff\1\5\1\6\1\7\1\10\1\11\1\12\1\13" + "\1\14\2\uffff\1\16\1\uffff\1\17\4\uffff\1\26\11\uffff\1\51\1\52" + "\1\53\1\50\1\2\1\54\1\4\6\uffff\1\21\1\23\1\22\1\24\1\25\1\27\26" + "\uffff\1\32\1\uffff\1\37\7\uffff\1\15\1\20\1\44\4\uffff\1\41\1\46" + "\3\uffff\1\36\1\40\2\uffff\1\45\1\30\1\33\1\uffff\1\34\3\uffff\1" + "\47\1\31\1\uffff\1\35\1\42\1\uffff\1\43";

	static final String DFA8_specialS = "\162\uffff}>";

	static final String[] DFA8_transitionS = {
		"\2\42\2\uffff\1\42\22\uffff\1\42\1\26\4\uffff\1\21\1\uffff\1" + "\5\1\6\1\3\1\1\1\13\1\2\1\14\1\4\12\40\1\12\1\11\1\24\1\25\1" + "\23\2\uffff\32\41\6\uffff\1\20\1\37\1\36\1\41\1\32\1\30\2\41" + "\1\31\2\41\1\34\1\41\1\22\1\16\1\33\3\41\1\27\1\41\1\35\1\41" + "\1\15\2\41\1\7\1\17\1\10",
		"",
		"\1\43",
		"",
		"\1\45",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"\1\47",
		"\1\50",
		"",
		"\1\51",
		"",
		"\1\53\11\uffff\1\52\5\uffff\1\54",
		"\1\55",
		"\1\57",
		"\1\61",
		"",
		"\1\64\11\uffff\1\63",
		"\1\65\23\uffff\1\66",
		"\1\67\7\uffff\1\70",
		"\1\71\13\uffff\1\72",
		"\1\73",
		"\1\74",
		"\1\75",
		"\1\76",
		"\1\77",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"\1\100",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"\1\101",
		"\1\102",
		"\1\103",
		"\1\104",
		"",
		"",
		"",
		"",
		"",
		"",
		"\1\105",
		"\1\106",
		"\1\107",
		"\1\110",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"\12\41\7\uffff\32\41\6\uffff\23\41\1\112\6\41",
		"\1\114",
		"\1\115",
		"\1\116",
		"\1\117",
		"\1\120",
		"\1\121",
		"\1\122",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"\1\126",
		"\1\127",
		"\1\130",
		"\1\131",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"",
		"\1\134",
		"\1\135",
		"\1\136",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"\1\141",
		"\1\142",
		"",
		"",
		"",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"\1\146",
		"",
		"",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"\1\150",
		"\1\151",
		"",
		"",
		"\1\152",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"",
		"",
		"",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"",
		"\1\155",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		"",
		"",
		"\1\160",
		"",
		"",
		"\12\41\7\uffff\32\41\6\uffff\32\41",
		""
	};

	static final short[] DFA8_eot = DFA.unpackEncodedString(DFA8_eotS);

	static final short[] DFA8_eof = DFA.unpackEncodedString(DFA8_eofS);

	static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(DFA8_minS);

	static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(DFA8_maxS);

	static final short[] DFA8_accept = DFA.unpackEncodedString(DFA8_acceptS);

	static final short[] DFA8_special = DFA.unpackEncodedString(DFA8_specialS);

	static final short[][] DFA8_transition;

	static {
		final int numStates = DFA8_transitionS.length;
		DFA8_transition = new short[numStates][];
		for (int i = 0; i < numStates; i++) {
			DFA8_transition[i] = DFA.unpackEncodedString(DFA8_transitionS[i]);
		}
	}

	public FOOLLexer() {}

	public FOOLLexer(final CharStream input) {
		this(input, new RecognizerSharedState());
	}

	public FOOLLexer(final CharStream input, final RecognizerSharedState state) {
		super(input, state);
	}

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	@Override
	public String getGrammarFileName() {
		return "/home/gciatto/Scrivania/svm-git/svm/res/FOOL.g";
	}

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			final int _type = AND;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:413:5: ( 'and' | '&&' )
			int alt2 = 2;
			final int LA2_0 = input.LA(1);

			if (LA2_0 == 'a') {
				alt2 = 1;
			} else if (LA2_0 == '&') {
				alt2 = 2;
			} else {
				final NoViableAltException nvae = new NoViableAltException("", 2, 0, input);

				throw nvae;

			}
			switch (alt2) {
				case 1:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:413:7: 'and'
				{
					match("and");

				}
				break;
				case 2:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:413:15: '&&'
				{
					match("&&");

				}
				break;

			}
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "AND"

	// $ANTLR start "ARROW"
	public final void mARROW() throws RecognitionException {
		try {
			final int _type = ARROW;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:438:9: ( '->' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:438:11: '->'
			{
				match("->");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "ARROW"

	// $ANTLR start "ASS"
	public final void mASS() throws RecognitionException {
		try {
			final int _type = ASS;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:421:5: ( '=' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:421:7: '='
			{
				match('=');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "ASS"

	// $ANTLR start "BOOL"
	public final void mBOOL() throws RecognitionException {
		try {
			final int _type = BOOL;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:437:6: ( 'bool' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:437:8: 'bool'
			{
				match("bool");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "BOOL"

	// $ANTLR start "CLASS"
	public final void mCLASS() throws RecognitionException {
		try {
			final int _type = CLASS;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:432:7: ( 'class' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:432:9: 'class'
			{
				match("class");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "CLASS"

	// $ANTLR start "CLPAR"
	public final void mCLPAR() throws RecognitionException {
		try {
			final int _type = CLPAR;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:405:7: ( '{' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:405:9: '{'
			{
				match('{');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "CLPAR"

	// $ANTLR start "COLON"
	public final void mCOLON() throws RecognitionException {
		try {
			final int _type = COLON;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:408:9: ( ':' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:408:11: ':'
			{
				match(':');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "COLON"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			final int _type = COMMA;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:409:7: ( ',' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:409:9: ','
			{
				match(',');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "COMMA"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			final int _type = COMMENT;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:443:10: ( '/*' ( options {greedy=false; } : . )* '*/' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:443:12: '/*' ( options {greedy=false; } : . )* '*/'
			{
				match("/*");

				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:443:17: ( options {greedy=false; } : . )*
				loop7: do {
					int alt7 = 2;
					final int LA7_0 = input.LA(1);

					if (LA7_0 == '*') {
						final int LA7_1 = input.LA(2);

						if (LA7_1 == '/') {
							alt7 = 2;
						} else if (LA7_1 >= '\u0000' && LA7_1 <= '.' || LA7_1 >= '0' && LA7_1 <= '\uFFFF') {
							alt7 = 1;
						}

					} else if (LA7_0 >= '\u0000' && LA7_0 <= ')' || LA7_0 >= '+' && LA7_0 <= '\uFFFF') {
						alt7 = 1;
					}

					switch (alt7) {
						case 1:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:443:44: .
						{
							matchAny();

						}
						break;

						default:
							break loop7;
					}
				} while (true);

				match("*/");

				skip();

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "COMMENT"

	// $ANTLR start "CRPAR"
	public final void mCRPAR() throws RecognitionException {
		try {
			final int _type = CRPAR;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:406:7: ( '}' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:406:9: '}'
			{
				match('}');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "CRPAR"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			final int _type = DIV;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:402:6: ( '/' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:402:8: '/'
			{
				match('/');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "DIV"

	// $ANTLR start "DOT"
	public final void mDOT() throws RecognitionException {
		try {
			final int _type = DOT;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:410:5: ( '.' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:410:7: '.'
			{
				match('.');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "DOT"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			final int _type = ELSE;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:426:6: ( 'else' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:426:8: 'else'
			{
				match("else");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "ELSE"

	// $ANTLR start "EQ"
	public final void mEQ() throws RecognitionException {
		try {
			final int _type = EQ;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:419:4: ( '==' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:419:6: '=='
			{
				match("==");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "EQ"

	// $ANTLR start "EXTENDS"
	public final void mEXTENDS() throws RecognitionException {
		try {
			final int _type = EXTENDS;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:433:9: ( 'extends' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:433:11: 'extends'
			{
				match("extends");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "EXTENDS"

	// $ANTLR start "FALSE"
	public final void mFALSE() throws RecognitionException {
		try {
			final int _type = FALSE;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:423:7: ( 'false' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:423:9: 'false'
			{
				match("false");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "FALSE"

	// $ANTLR start "FUN"
	public final void mFUN() throws RecognitionException {
		try {
			final int _type = FUN;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:431:5: ( 'fun' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:431:7: 'fun'
			{
				match("fun");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "FUN"

	// $ANTLR start "GE"
	public final void mGE() throws RecognitionException {
		try {
			final int _type = GE;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:415:4: ( '>=' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:415:6: '>='
			{
				match(">=");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "GE"

	// $ANTLR start "GT"
	public final void mGT() throws RecognitionException {
		try {
			final int _type = GT;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:417:4: ( '>' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:417:6: '>'
			{
				match('>');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "GT"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			final int _type = ID;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:440:6: ( ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )* )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:440:8: ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			{
				if (input.LA(1) >= 'A' && input.LA(1) <= 'Z' || input.LA(1) >= 'a' && input.LA(1) <= 'z') {
					input.consume();
				} else {
					final MismatchedSetException mse = new MismatchedSetException(null, input);
					recover(mse);
					throw mse;
				}

				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:440:27: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
				loop5: do {
					int alt5 = 2;
					final int LA5_0 = input.LA(1);

					if (LA5_0 >= '0' && LA5_0 <= '9' || LA5_0 >= 'A' && LA5_0 <= 'Z' || LA5_0 >= 'a' && LA5_0 <= 'z') {
						alt5 = 1;
					}

					switch (alt5) {
						case 1:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:
						{
							if (input.LA(1) >= '0' && input.LA(1) <= '9' || input.LA(1) >= 'A' && input.LA(1) <= 'Z' || input.LA(1) >= 'a' && input.LA(1) <= 'z') {
								input.consume();
							} else {
								final MismatchedSetException mse = new MismatchedSetException(null, input);
								recover(mse);
								throw mse;
							}

						}
						break;

						default:
							break loop5;
					}
				} while (true);

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "ID"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			final int _type = IF;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:424:4: ( 'if' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:424:6: 'if'
			{
				match("if");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "IF"

	// $ANTLR start "IN"
	public final void mIN() throws RecognitionException {
		try {
			final int _type = IN;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:429:9: ( 'in' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:429:11: 'in'
			{
				match("in");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "IN"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			final int _type = INT;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:436:5: ( 'int' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:436:7: 'int'
			{
				match("int");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "INT"

	// $ANTLR start "INTEGER"
	public final void mINTEGER() throws RecognitionException {
		try {
			final int _type = INTEGER;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:439:9: ( ( ( '1' .. '9' ) ( '0' .. '9' )* ) | '0' )
			int alt4 = 2;
			final int LA4_0 = input.LA(1);

			if (LA4_0 >= '1' && LA4_0 <= '9') {
				alt4 = 1;
			} else if (LA4_0 == '0') {
				alt4 = 2;
			} else {
				final NoViableAltException nvae = new NoViableAltException("", 4, 0, input);

				throw nvae;

			}
			switch (alt4) {
				case 1:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:439:11: ( ( '1' .. '9' ) ( '0' .. '9' )* )
				{
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:439:11: ( ( '1' .. '9' ) ( '0' .. '9' )* )
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:439:12: ( '1' .. '9' ) ( '0' .. '9' )*
					{
						if (input.LA(1) >= '1' && input.LA(1) <= '9') {
							input.consume();
						} else {
							final MismatchedSetException mse = new MismatchedSetException(null, input);
							recover(mse);
							throw mse;
						}

						// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:439:22: ( '0' .. '9' )*
						loop3: do {
							int alt3 = 2;
							final int LA3_0 = input.LA(1);

							if (LA3_0 >= '0' && LA3_0 <= '9') {
								alt3 = 1;
							}

							switch (alt3) {
								case 1:
									// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:
								{
									if (input.LA(1) >= '0' && input.LA(1) <= '9') {
										input.consume();
									} else {
										final MismatchedSetException mse = new MismatchedSetException(null, input);
										recover(mse);
										throw mse;
									}

								}
								break;

								default:
									break loop3;
							}
						} while (true);

					}

				}
				break;
				case 2:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:439:37: '0'
				{
					match('0');

				}
				break;

			}
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "INTEGER"

	// $ANTLR start "LE"
	public final void mLE() throws RecognitionException {
		try {
			final int _type = LE;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:416:4: ( '<=' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:416:6: '<='
			{
				match("<=");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "LE"

	// $ANTLR start "LET"
	public final void mLET() throws RecognitionException {
		try {
			final int _type = LET;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:428:9: ( 'let' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:428:11: 'let'
			{
				match("let");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "LET"

	// $ANTLR start "LPAR"
	public final void mLPAR() throws RecognitionException {
		try {
			final int _type = LPAR;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:403:6: ( '(' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:403:8: '('
			{
				match('(');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "LPAR"

	// $ANTLR start "LW"
	public final void mLW() throws RecognitionException {
		try {
			final int _type = LW;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:418:4: ( '<' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:418:6: '<'
			{
				match('<');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "LW"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			final int _type = MINUS;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:400:9: ( '-' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:400:11: '-'
			{
				match('-');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "MINUS"
	// $ANTLR start "MULT"
	public final void mMULT() throws RecognitionException {
		try {
			final int _type = MULT;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:401:9: ( '*' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:401:11: '*'
			{
				match('*');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "MULT"
	// $ANTLR start "NE"
	public final void mNE() throws RecognitionException {
		try {
			final int _type = NE;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:420:4: ( '!=' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:420:6: '!='
			{
				match("!=");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "NE"
	// $ANTLR start "NEW"
	public final void mNEW() throws RecognitionException {
		try {
			final int _type = NEW;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:434:6: ( 'new' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:434:8: 'new'
			{
				match("new");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "NEW"
	// $ANTLR start "NOT"
	public final void mNOT() throws RecognitionException {
		try {
			final int _type = NOT;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:414:5: ( 'not' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:414:7: 'not'
			{
				match("not");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "NOT"
	// $ANTLR start "NULL"
	public final void mNULL() throws RecognitionException {
		try {
			final int _type = NULL;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:435:9: ( 'null' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:435:11: 'null'
			{
				match("null");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "NULL"
	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			final int _type = OR;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:412:4: ( 'or' | '||' )
			int alt1 = 2;
			final int LA1_0 = input.LA(1);

			if (LA1_0 == 'o') {
				alt1 = 1;
			} else if (LA1_0 == '|') {
				alt1 = 2;
			} else {
				final NoViableAltException nvae = new NoViableAltException("", 1, 0, input);

				throw nvae;

			}
			switch (alt1) {
				case 1:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:412:6: 'or'
				{
					match("or");

				}
				break;
				case 2:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:412:13: '||'
				{
					match("||");

				}
				break;

			}
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "OR"
	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			final int _type = PLUS;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:399:8: ( '+' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:399:10: '+'
			{
				match('+');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "PLUS"

	// $ANTLR start "PRINT"
	public final void mPRINT() throws RecognitionException {
		try {
			final int _type = PRINT;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:427:7: ( 'print' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:427:9: 'print'
			{
				match("print");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "PRINT"
	// $ANTLR start "RPAR"
	public final void mRPAR() throws RecognitionException {
		try {
			final int _type = RPAR;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:404:6: ( ')' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:404:8: ')'
			{
				match(')');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "RPAR"
	// $ANTLR start "SEMIC"
	public final void mSEMIC() throws RecognitionException {
		try {
			final int _type = SEMIC;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:407:8: ( ';' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:407:10: ';'
			{
				match(';');

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "SEMIC"
	// $ANTLR start "THEN"
	public final void mTHEN() throws RecognitionException {
		try {
			final int _type = THEN;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:425:6: ( 'then' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:425:8: 'then'
			{
				match("then");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "THEN"
	@Override
	public void mTokens() throws RecognitionException {
		// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:8: ( PLUS | MINUS | MULT | DIV | LPAR | RPAR | CLPAR | CRPAR | SEMIC | COLON | COMMA | DOT | XOR | OR | AND | NOT | GE | LE | GT | LW | EQ | NE | ASS | TRUE | FALSE | IF | THEN | ELSE | PRINT | LET | IN | VAR | FUN | CLASS | EXTENDS | NEW | NULL | INT | BOOL | ARROW | INTEGER | ID | WHITESP | COMMENT )
		int alt8 = 44;
		alt8 = dfa8.predict(input);
		switch (alt8) {
			case 1:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:10: PLUS
			{
				mPLUS();

			}
			break;
			case 2:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:15: MINUS
			{
				mMINUS();

			}
			break;
			case 3:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:21: MULT
			{
				mMULT();

			}
			break;
			case 4:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:26: DIV
			{
				mDIV();

			}
			break;
			case 5:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:30: LPAR
			{
				mLPAR();

			}
			break;
			case 6:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:35: RPAR
			{
				mRPAR();

			}
			break;
			case 7:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:40: CLPAR
			{
				mCLPAR();

			}
			break;
			case 8:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:46: CRPAR
			{
				mCRPAR();

			}
			break;
			case 9:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:52: SEMIC
			{
				mSEMIC();

			}
			break;
			case 10:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:58: COLON
			{
				mCOLON();

			}
			break;
			case 11:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:64: COMMA
			{
				mCOMMA();

			}
			break;
			case 12:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:70: DOT
			{
				mDOT();

			}
			break;
			case 13:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:74: XOR
			{
				mXOR();

			}
			break;
			case 14:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:78: OR
			{
				mOR();

			}
			break;
			case 15:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:81: AND
			{
				mAND();

			}
			break;
			case 16:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:85: NOT
			{
				mNOT();

			}
			break;
			case 17:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:89: GE
			{
				mGE();

			}
			break;
			case 18:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:92: LE
			{
				mLE();

			}
			break;
			case 19:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:95: GT
			{
				mGT();

			}
			break;
			case 20:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:98: LW
			{
				mLW();

			}
			break;
			case 21:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:101: EQ
			{
				mEQ();

			}
			break;
			case 22:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:104: NE
			{
				mNE();

			}
			break;
			case 23:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:107: ASS
			{
				mASS();

			}
			break;
			case 24:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:111: TRUE
			{
				mTRUE();

			}
			break;
			case 25:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:116: FALSE
			{
				mFALSE();

			}
			break;
			case 26:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:122: IF
			{
				mIF();

			}
			break;
			case 27:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:125: THEN
			{
				mTHEN();

			}
			break;
			case 28:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:130: ELSE
			{
				mELSE();

			}
			break;
			case 29:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:135: PRINT
			{
				mPRINT();

			}
			break;
			case 30:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:141: LET
			{
				mLET();

			}
			break;
			case 31:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:145: IN
			{
				mIN();

			}
			break;
			case 32:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:148: VAR
			{
				mVAR();

			}
			break;
			case 33:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:152: FUN
			{
				mFUN();

			}
			break;
			case 34:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:156: CLASS
			{
				mCLASS();

			}
			break;
			case 35:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:162: EXTENDS
			{
				mEXTENDS();

			}
			break;
			case 36:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:170: NEW
			{
				mNEW();

			}
			break;
			case 37:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:174: NULL
			{
				mNULL();

			}
			break;
			case 38:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:179: INT
			{
				mINT();

			}
			break;
			case 39:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:183: BOOL
			{
				mBOOL();

			}
			break;
			case 40:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:188: ARROW
			{
				mARROW();

			}
			break;
			case 41:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:194: INTEGER
			{
				mINTEGER();

			}
			break;
			case 42:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:202: ID
			{
				mID();

			}
			break;
			case 43:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:205: WHITESP
			{
				mWHITESP();

			}
			break;
			case 44:
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:1:213: COMMENT
			{
				mCOMMENT();

			}
			break;

		}

	}

	// $ANTLR start "TRUE"
	public final void mTRUE() throws RecognitionException {
		try {
			final int _type = TRUE;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:422:6: ( 'true' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:422:8: 'true'
			{
				match("true");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "TRUE"
	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			final int _type = VAR;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:430:9: ( 'var' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:430:11: 'var'
			{
				match("var");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "VAR"

	// $ANTLR start "WHITESP"
	public final void mWHITESP() throws RecognitionException {
		try {
			final int _type = WHITESP;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:441:9: ( ( '\\t' | ' ' | '\\r' | '\\n' )+ )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:441:11: ( '\\t' | ' ' | '\\r' | '\\n' )+
			{
				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:441:11: ( '\\t' | ' ' | '\\r' | '\\n' )+
				int cnt6 = 0;
				loop6: do {
					int alt6 = 2;
					final int LA6_0 = input.LA(1);

					if (LA6_0 >= '\t' && LA6_0 <= '\n' || LA6_0 == '\r' || LA6_0 == ' ') {
						alt6 = 1;
					}

					switch (alt6) {
						case 1:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:
						{
							if (input.LA(1) >= '\t' && input.LA(1) <= '\n' || input.LA(1) == '\r' || input.LA(1) == ' ') {
								input.consume();
							} else {
								final MismatchedSetException mse = new MismatchedSetException(null, input);
								recover(mse);
								throw mse;
							}

						}
						break;

						default:
							if (cnt6 >= 1) {
								break loop6;
							}
							final EarlyExitException eee = new EarlyExitException(6, input);
							throw eee;
					}
					cnt6++;
				} while (true);

				skip();

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}

	// $ANTLR end "WHITESP"

	// $ANTLR start "XOR"
	public final void mXOR() throws RecognitionException {
		try {
			final int _type = XOR;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:411:5: ( 'xor' )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:411:7: 'xor'
			{
				match("xor");

			}

			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "XOR"

}