// $ANTLR 3.4 /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g 2015-02-09 13:25:20

package it.unibo.gciatto.sdt.parsing;

import static it.unibo.gciatto.sdt.typing.Types.function;
import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.adt.IClassNode;
import it.unibo.gciatto.sdt.adt.IConstructorCallNode;
import it.unibo.gciatto.sdt.adt.IFieldNode;
import it.unibo.gciatto.sdt.adt.IMethodCallNode;
import it.unibo.gciatto.sdt.adt.IMethodNode;
import it.unibo.gciatto.sdt.adt.IVariableNode;
import it.unibo.gciatto.sdt.adt.impl.AndNode;
import it.unibo.gciatto.sdt.adt.impl.BooleanNode;
import it.unibo.gciatto.sdt.adt.impl.CallNode;
import it.unibo.gciatto.sdt.adt.impl.ClassNode;
import it.unibo.gciatto.sdt.adt.impl.DeclarationNode;
import it.unibo.gciatto.sdt.adt.impl.DivNode;
import it.unibo.gciatto.sdt.adt.impl.EqualsNode;
import it.unibo.gciatto.sdt.adt.impl.FieldNode;
import it.unibo.gciatto.sdt.adt.impl.FunNode;
import it.unibo.gciatto.sdt.adt.impl.GreaterEqualsNode;
import it.unibo.gciatto.sdt.adt.impl.GreaterNode;
import it.unibo.gciatto.sdt.adt.impl.IdNode;
import it.unibo.gciatto.sdt.adt.impl.IntegerNode;
import it.unibo.gciatto.sdt.adt.impl.LetInNode;
import it.unibo.gciatto.sdt.adt.impl.LowerEqualsNode;
import it.unibo.gciatto.sdt.adt.impl.LowerNode;
import it.unibo.gciatto.sdt.adt.impl.MethodNode;
import it.unibo.gciatto.sdt.adt.impl.MultNode;
import it.unibo.gciatto.sdt.adt.impl.NotEqualsNode;
import it.unibo.gciatto.sdt.adt.impl.NotNode;
import it.unibo.gciatto.sdt.adt.impl.NullNode;
import it.unibo.gciatto.sdt.adt.impl.OrNode;
import it.unibo.gciatto.sdt.adt.impl.PrintNode;
import it.unibo.gciatto.sdt.adt.impl.ProgramNode;
import it.unibo.gciatto.sdt.adt.impl.SubNode;
import it.unibo.gciatto.sdt.adt.impl.SumNode;
import it.unibo.gciatto.sdt.adt.impl.TernaryNode;
import it.unibo.gciatto.sdt.adt.impl.VarNode;
import it.unibo.gciatto.sdt.adt.impl.XorNode;
import it.unibo.gciatto.sdt.scoping.IScope;
import it.unibo.gciatto.sdt.scoping.ISymbolData;
import it.unibo.gciatto.sdt.scoping.SymbolType;
import it.unibo.gciatto.sdt.scoping.impl.Scope;
import it.unibo.gciatto.sdt.typing.IFunctionType;
import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.sdt.typing.Types;
import it.unibo.gciatto.sdt.typing.impl.FunctionType;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.BitSet;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.Parser;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenStream;

@SuppressWarnings({
	"all", "warnings", "unchecked"
})
public class FOOLParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "ARROW", "ASS", "BOOL", "CLASS", "CLPAR", "COLON", "COMMA", "COMMENT", "CRPAR", "DIV", "DOT", "ELSE", "EQ", "EXTENDS", "FALSE", "FUN", "GE", "GT", "ID", "IF", "IN", "INT", "INTEGER", "LE", "LET", "LPAR", "LW", "MINUS", "MULT", "NE", "NEW", "NOT", "NULL", "OR", "PLUS", "PRINT", "RPAR", "SEMIC", "THEN", "TRUE", "VAR", "WHITESP", "XOR"
	};

	public static final int EOF = -1;
	public static final int AND = 4;
	public static final int ARROW = 5;
	public static final int ASS = 6;
	public static final int BOOL = 7;
	public static final int CLASS = 8;
	public static final int CLPAR = 9;
	public static final int COLON = 10;
	public static final int COMMA = 11;
	public static final int COMMENT = 12;
	public static final int CRPAR = 13;
	public static final int DIV = 14;
	public static final int DOT = 15;
	public static final int ELSE = 16;
	public static final int EQ = 17;
	public static final int EXTENDS = 18;
	public static final int FALSE = 19;
	public static final int FUN = 20;
	public static final int GE = 21;
	public static final int GT = 22;
	public static final int ID = 23;
	public static final int IF = 24;
	public static final int IN = 25;
	public static final int INT = 26;
	public static final int INTEGER = 27;
	public static final int LE = 28;
	public static final int LET = 29;
	public static final int LPAR = 30;
	public static final int LW = 31;
	public static final int MINUS = 32;
	public static final int MULT = 33;
	public static final int NE = 34;
	public static final int NEW = 35;
	public static final int NOT = 36;
	public static final int NULL = 37;
	public static final int OR = 38;
	public static final int PLUS = 39;
	public static final int PRINT = 40;
	public static final int RPAR = 41;
	public static final int SEMIC = 42;
	public static final int THEN = 43;
	public static final int TRUE = 44;
	public static final int VAR = 45;
	public static final int WHITESP = 46;
	public static final int XOR = 47;

	private final IScope mScope = new Scope(0);

	// delegators

	public static final BitSet FOLLOW_VAR_in_dec46 = new BitSet(new long[] {
			0x0000000000800000L
	});
	public static final BitSet FOLLOW_ID_in_dec50 = new BitSet(new long[] {
			0x0000000000000400L
	});

	public static final BitSet FOLLOW_COLON_in_dec52 = new BitSet(new long[] {
			0x0000000044800080L
	});
	public static final BitSet FOLLOW_type_in_dec56 = new BitSet(new long[] {
			0x0000000000000040L
	});

	public static final BitSet FOLLOW_ASS_in_dec58 = new BitSet(new long[] {
			0x0000113849880000L
	});

	public static final BitSet FOLLOW_exp_in_dec62 = new BitSet(new long[] {
			0x0000040000000000L
	});

	public static final BitSet FOLLOW_SEMIC_in_dec64 = new BitSet(new long[] {
			0x0000200000100002L
	});

	public static final BitSet FOLLOW_FUN_in_dec75 = new BitSet(new long[] {
			0x0000000000800000L
	});

	public static final BitSet FOLLOW_ID_in_dec79 = new BitSet(new long[] {
			0x0000000000000400L
	});

	public static final BitSet FOLLOW_COLON_in_dec81 = new BitSet(new long[] {
			0x0000000044800080L
	});

	public static final BitSet FOLLOW_type_in_dec85 = new BitSet(new long[] {
			0x0000000040000000L
	});

	public static final BitSet FOLLOW_LPAR_in_dec105 = new BitSet(new long[] {
			0x0000020000800000L
	});

	public static final BitSet FOLLOW_ID_in_dec113 = new BitSet(new long[] {
			0x0000000000000400L
	});

	public static final BitSet FOLLOW_COLON_in_dec115 = new BitSet(new long[] {
			0x0000000044800080L
	});

	public static final BitSet FOLLOW_type_in_dec119 = new BitSet(new long[] {
			0x0000020000000800L
	});

	public static final BitSet FOLLOW_COMMA_in_dec129 = new BitSet(new long[] {
			0x0000000000800000L
	});

	// Delegated rules

	public static final BitSet FOLLOW_ID_in_dec133 = new BitSet(new long[] {
			0x0000000000000400L
	});
	public static final BitSet FOLLOW_COLON_in_dec135 = new BitSet(new long[] {
			0x0000000044800080L
	});
	public static final BitSet FOLLOW_type_in_dec139 = new BitSet(new long[] {
			0x0000020000000800L
	});
	public static final BitSet FOLLOW_RPAR_in_dec163 = new BitSet(new long[] {
			0x0000113869880000L
	});
	public static final BitSet FOLLOW_LET_in_dec169 = new BitSet(new long[] {
			0x0000200002100000L
	});
	public static final BitSet FOLLOW_dec_in_dec173 = new BitSet(new long[] {
			0x0000000002000000L
	});
	public static final BitSet FOLLOW_IN_in_dec175 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_exp_in_dec181 = new BitSet(new long[] {
			0x0000040000000000L
	});
	public static final BitSet FOLLOW_SEMIC_in_dec183 = new BitSet(new long[] {
			0x0000200000100002L
	});
	public static final BitSet FOLLOW_CLASS_in_cllist226 = new BitSet(new long[] {
			0x0000000000800000L
	});
	public static final BitSet FOLLOW_ID_in_cllist232 = new BitSet(new long[] {
			0x0000000040040000L
	});
	public static final BitSet FOLLOW_EXTENDS_in_cllist242 = new BitSet(new long[] {
			0x0000000000800000L
	});
	public static final BitSet FOLLOW_ID_in_cllist248 = new BitSet(new long[] {
			0x0000000040000000L
	});
	public static final BitSet FOLLOW_LPAR_in_cllist272 = new BitSet(new long[] {
			0x0000020000800000L
	});
	public static final BitSet FOLLOW_ID_in_cllist279 = new BitSet(new long[] {
			0x0000000000000400L
	});
	public static final BitSet FOLLOW_COLON_in_cllist281 = new BitSet(new long[] {
			0x0000000004800080L
	});
	public static final BitSet FOLLOW_basic_in_cllist287 = new BitSet(new long[] {
			0x0000020000000800L
	});
	public static final BitSet FOLLOW_COMMA_in_cllist300 = new BitSet(new long[] {
			0x0000000000800000L
	});
	public static final BitSet FOLLOW_ID_in_cllist306 = new BitSet(new long[] {
			0x0000000000000400L
	});
	public static final BitSet FOLLOW_COLON_in_cllist308 = new BitSet(new long[] {
			0x0000000004800080L
	});
	public static final BitSet FOLLOW_basic_in_cllist314 = new BitSet(new long[] {
			0x0000020000000800L
	});
	public static final BitSet FOLLOW_RPAR_in_cllist332 = new BitSet(new long[] {
			0x0000000000000200L
	});
	public static final BitSet FOLLOW_CLPAR_in_cllist355 = new BitSet(new long[] {
			0x0000000000102000L
	});
	public static final BitSet FOLLOW_FUN_in_cllist399 = new BitSet(new long[] {
			0x0000000000800000L
	});
	public static final BitSet FOLLOW_ID_in_cllist405 = new BitSet(new long[] {
			0x0000000000000400L
	});
	public static final BitSet FOLLOW_COLON_in_cllist407 = new BitSet(new long[] {
			0x0000000004800080L
	});
	public static final BitSet FOLLOW_basic_in_cllist413 = new BitSet(new long[] {
			0x0000000040000000L
	});
	public static final BitSet FOLLOW_LPAR_in_cllist415 = new BitSet(new long[] {
			0x0000020000800000L
	});
	public static final BitSet FOLLOW_ID_in_cllist466 = new BitSet(new long[] {
			0x0000000000000400L
	});
	public static final BitSet FOLLOW_COLON_in_cllist468 = new BitSet(new long[] {
			0x0000000044800080L
	});
	public static final BitSet FOLLOW_type_in_cllist474 = new BitSet(new long[] {
			0x0000020000000800L
	});
	public static final BitSet FOLLOW_COMMA_in_cllist523 = new BitSet(new long[] {
			0x0000000000800000L
	});
	public static final BitSet FOLLOW_ID_in_cllist529 = new BitSet(new long[] {
			0x0000000000000400L
	});
	public static final BitSet FOLLOW_COLON_in_cllist531 = new BitSet(new long[] {
			0x0000000044800080L
	});
	public static final BitSet FOLLOW_type_in_cllist537 = new BitSet(new long[] {
			0x0000020000000800L
	});
	public static final BitSet FOLLOW_RPAR_in_cllist634 = new BitSet(new long[] {
			0x0000113869880000L
	});
	public static final BitSet FOLLOW_LET_in_cllist653 = new BitSet(new long[] {
			0x0000200002000000L
	});
	public static final BitSet FOLLOW_VAR_in_cllist673 = new BitSet(new long[] {
			0x0000000000800000L
	});
	public static final BitSet FOLLOW_ID_in_cllist679 = new BitSet(new long[] {
			0x0000000000000400L
	});
	public static final BitSet FOLLOW_COLON_in_cllist681 = new BitSet(new long[] {
			0x0000000004800080L
	});
	public static final BitSet FOLLOW_basic_in_cllist687 = new BitSet(new long[] {
			0x0000000000000040L
	});
	public static final BitSet FOLLOW_ASS_in_cllist689 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_exp_in_cllist695 = new BitSet(new long[] {
			0x0000040000000000L
	});
	public static final BitSet FOLLOW_SEMIC_in_cllist697 = new BitSet(new long[] {
			0x0000200002000000L
	});
	public static final BitSet FOLLOW_IN_in_cllist738 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_exp_in_cllist747 = new BitSet(new long[] {
			0x0000040000000000L
	});
	public static final BitSet FOLLOW_SEMIC_in_cllist749 = new BitSet(new long[] {
			0x0000000000102000L
	});
	public static final BitSet FOLLOW_CRPAR_in_cllist820 = new BitSet(new long[] {
			0x0000000000000102L
	});
	public static final BitSet FOLLOW_basic_in_type881 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_arrow_in_type896 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_INT_in_basic925 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_BOOL_in_basic939 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_ID_in_basic962 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_LPAR_in_arrow1015 = new BitSet(new long[] {
			0x0000020044800080L
	});
	public static final BitSet FOLLOW_type_in_arrow1027 = new BitSet(new long[] {
			0x0000020000000800L
	});
	public static final BitSet FOLLOW_COMMA_in_arrow1038 = new BitSet(new long[] {
			0x0000000044800080L
	});
	public static final BitSet FOLLOW_type_in_arrow1044 = new BitSet(new long[] {
			0x0000020000000800L
	});
	public static final BitSet FOLLOW_RPAR_in_arrow1060 = new BitSet(new long[] {
			0x0000000000000020L
	});
	public static final BitSet FOLLOW_ARROW_in_arrow1062 = new BitSet(new long[] {
			0x0000000004800080L
	});
	public static final BitSet FOLLOW_basic_in_arrow1071 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_exp_in_prog1104 = new BitSet(new long[] {
			0x0000040000000000L
	});
	public static final BitSet FOLLOW_SEMIC_in_prog1106 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_LET_in_prog1116 = new BitSet(new long[] {
			0x0000200002100100L
	});
	public static final BitSet FOLLOW_cllist_in_prog1120 = new BitSet(new long[] {
			0x0000200002100000L
	});
	public static final BitSet FOLLOW_dec_in_prog1124 = new BitSet(new long[] {
			0x0000000002000000L
	});
	public static final BitSet FOLLOW_IN_in_prog1126 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_exp_in_prog1130 = new BitSet(new long[] {
			0x0000040000000000L
	});
	public static final BitSet FOLLOW_SEMIC_in_prog1132 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_term_in_exp1156 = new BitSet(new long[] {
			0x000080C100000002L
	});
	public static final BitSet FOLLOW_PLUS_in_exp1170 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_term_in_exp1176 = new BitSet(new long[] {
			0x000080C100000002L
	});
	public static final BitSet FOLLOW_MINUS_in_exp1189 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_term_in_exp1195 = new BitSet(new long[] {
			0x000080C100000002L
	});
	public static final BitSet FOLLOW_OR_in_exp1207 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_term_in_exp1213 = new BitSet(new long[] {
			0x000080C100000002L
	});
	public static final BitSet FOLLOW_XOR_in_exp1225 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_term_in_exp1231 = new BitSet(new long[] {
			0x000080C100000002L
	});
	public static final BitSet FOLLOW_fact_in_term1260 = new BitSet(new long[] {
			0x0000000200004012L
	});
	public static final BitSet FOLLOW_MULT_in_term1274 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_fact_in_term1280 = new BitSet(new long[] {
			0x0000000200004012L
	});
	public static final BitSet FOLLOW_DIV_in_term1293 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_fact_in_term1299 = new BitSet(new long[] {
			0x0000000200004012L
	});
	public static final BitSet FOLLOW_AND_in_term1312 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_fact_in_term1318 = new BitSet(new long[] {
			0x0000000200004012L
	});
	public static final BitSet FOLLOW_value_in_fact1349 = new BitSet(new long[] {
			0x0000000490620002L
	});
	public static final BitSet FOLLOW_EQ_in_fact1363 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_value_in_fact1369 = new BitSet(new long[] {
			0x0000000490620002L
	});
	public static final BitSet FOLLOW_GE_in_fact1382 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_value_in_fact1388 = new BitSet(new long[] {
			0x0000000490620002L
	});
	public static final BitSet FOLLOW_LE_in_fact1401 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_value_in_fact1407 = new BitSet(new long[] {
			0x0000000490620002L
	});
	public static final BitSet FOLLOW_GT_in_fact1420 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_value_in_fact1426 = new BitSet(new long[] {
			0x0000000490620002L
	});
	public static final BitSet FOLLOW_LW_in_fact1439 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_value_in_fact1445 = new BitSet(new long[] {
			0x0000000490620002L
	});
	public static final BitSet FOLLOW_NE_in_fact1458 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_value_in_fact1464 = new BitSet(new long[] {
			0x0000000490620002L
	});
	public static final BitSet FOLLOW_INTEGER_in_value1495 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_TRUE_in_value1509 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_FALSE_in_value1523 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_IF_in_value1534 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_exp_in_value1540 = new BitSet(new long[] {
			0x0000080000000000L
	});
	public static final BitSet FOLLOW_THEN_in_value1542 = new BitSet(new long[] {
			0x0000000000000200L
	});
	public static final BitSet FOLLOW_CLPAR_in_value1544 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_exp_in_value1550 = new BitSet(new long[] {
			0x0000000000002000L
	});
	public static final BitSet FOLLOW_CRPAR_in_value1552 = new BitSet(new long[] {
			0x0000000000010000L
	});
	public static final BitSet FOLLOW_ELSE_in_value1554 = new BitSet(new long[] {
			0x0000000000000200L
	});
	public static final BitSet FOLLOW_CLPAR_in_value1556 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_exp_in_value1562 = new BitSet(new long[] {
			0x0000000000002000L
	});
	public static final BitSet FOLLOW_CRPAR_in_value1564 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_LPAR_in_value1574 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_exp_in_value1580 = new BitSet(new long[] {
			0x0000020000000000L
	});
	public static final BitSet FOLLOW_RPAR_in_value1582 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_NOT_in_value1592 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_exp_in_value1598 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_PRINT_in_value1607 = new BitSet(new long[] {
			0x0000000040000000L
	});
	public static final BitSet FOLLOW_LPAR_in_value1609 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_exp_in_value1615 = new BitSet(new long[] {
			0x0000020000000000L
	});
	public static final BitSet FOLLOW_RPAR_in_value1617 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_ID_in_value1631 = new BitSet(new long[] {
			0x0000000040008002L
	});
	public static final BitSet FOLLOW_LPAR_in_value1642 = new BitSet(new long[] {
			0x0000133849880000L
	});
	public static final BitSet FOLLOW_exp_in_value1657 = new BitSet(new long[] {
			0x0000020000000800L
	});
	public static final BitSet FOLLOW_COMMA_in_value1672 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_exp_in_value1676 = new BitSet(new long[] {
			0x0000020000000800L
	});
	public static final BitSet FOLLOW_RPAR_in_value1704 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_DOT_in_value1717 = new BitSet(new long[] {
			0x0000000000800000L
	});
	public static final BitSet FOLLOW_ID_in_value1723 = new BitSet(new long[] {
			0x0000000040000000L
	});
	public static final BitSet FOLLOW_LPAR_in_value1725 = new BitSet(new long[] {
			0x0000133849880000L
	});
	public static final BitSet FOLLOW_exp_in_value1744 = new BitSet(new long[] {
			0x0000020000000800L
	});
	public static final BitSet FOLLOW_COMMA_in_value1768 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_exp_in_value1774 = new BitSet(new long[] {
			0x0000020000000800L
	});
	public static final BitSet FOLLOW_RPAR_in_value1809 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_NULL_in_value1821 = new BitSet(new long[] {
			0x0000000000000002L
	});
	public static final BitSet FOLLOW_NEW_in_value1835 = new BitSet(new long[] {
			0x0000000000800000L
	});
	public static final BitSet FOLLOW_ID_in_value1841 = new BitSet(new long[] {
			0x0000000040000000L
	});
	public static final BitSet FOLLOW_LPAR_in_value1843 = new BitSet(new long[] {
			0x0000133849880000L
	});
	public static final BitSet FOLLOW_exp_in_value1858 = new BitSet(new long[] {
			0x0000020000000800L
	});
	public static final BitSet FOLLOW_COMMA_in_value1871 = new BitSet(new long[] {
			0x0000113849880000L
	});
	public static final BitSet FOLLOW_exp_in_value1877 = new BitSet(new long[] {
			0x0000020000000800L
	});
	public static final BitSet FOLLOW_RPAR_in_value1895 = new BitSet(new long[] {
			0x0000000000000002L
	});

	public FOOLParser(final TokenStream input) {
		this(input, new RecognizerSharedState());
	}

	public FOOLParser(final TokenStream input, final RecognizerSharedState state) {
		super(input, state);
	}

	// $ANTLR start "arrow"
	// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:171:1: arrow returns [IFunctionType type] : LPAR (arg= type ( COMMA arg= type )* )? RPAR ARROW res= basic ;
	public final IFunctionType arrow() throws RecognitionException {
		IFunctionType type = null;

		IType arg = null;

		IType res = null;

		try {
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:172:2: ( LPAR (arg= type ( COMMA arg= type )* )? RPAR ARROW res= basic )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:172:4: LPAR (arg= type ( COMMA arg= type )* )? RPAR ARROW res= basic
			{

				final IFunctionType ft = new FunctionType();
				type = ft;

				match(input, LPAR, FOLLOW_LPAR_in_arrow1015);

				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:177:2: (arg= type ( COMMA arg= type )* )?
				int alt17 = 2;
				final int LA17_0 = input.LA(1);

				if (LA17_0 == BOOL || LA17_0 == ID || LA17_0 == INT || LA17_0 == LPAR) {
					alt17 = 1;
				}
				switch (alt17) {
					case 1:
						// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:178:3: arg= type ( COMMA arg= type )*
					{
						pushFollow(FOLLOW_type_in_arrow1027);
						arg = type();

						state._fsp--;

						ft.addParamType(arg);

						// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:179:3: ( COMMA arg= type )*
						loop16: do {
							int alt16 = 2;
							final int LA16_0 = input.LA(1);

							if (LA16_0 == COMMA) {
								alt16 = 1;
							}

							switch (alt16) {
								case 1:
									// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:180:4: COMMA arg= type
								{
									match(input, COMMA, FOLLOW_COMMA_in_arrow1038);

									pushFollow(FOLLOW_type_in_arrow1044);
									arg = type();

									state._fsp--;

									ft.addParamType(arg);

								}
								break;

								default:
									break loop16;
							}
						} while (true);

					}
					break;

				}

				match(input, RPAR, FOLLOW_RPAR_in_arrow1060);

				match(input, ARROW, FOLLOW_ARROW_in_arrow1062);

				pushFollow(FOLLOW_basic_in_arrow1071);
				res = basic();

				state._fsp--;

				ft.setReturnType(res);

			}

		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		}

		finally {
			// do for sure before leaving
		}
		return type;
	}

	// $ANTLR end "arrow"
	// $ANTLR start "basic"
	// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:165:1: basic returns [IType type] : ( INT | BOOL |id= ID );
	public final IType basic() throws RecognitionException {
		IType type = null;

		Token id = null;

		try {
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:166:2: ( INT | BOOL |id= ID )
			int alt15 = 3;
			switch (input.LA(1)) {
				case INT: {
					alt15 = 1;
				}
				break;
				case BOOL: {
					alt15 = 2;
				}
				break;
				case ID: {
					alt15 = 3;
				}
				break;
				default:
					final NoViableAltException nvae = new NoViableAltException("", 15, 0, input);

					throw nvae;

			}

			switch (alt15) {
				case 1:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:166:4: INT
				{
					match(input, INT, FOLLOW_INT_in_basic925);

					type = Types.INTEGER;

				}
				break;
				case 2:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:167:11: BOOL
				{
					match(input, BOOL, FOLLOW_BOOL_in_basic939);

					type = Types.BOOLEAN;

				}
				break;
				case 3:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:168:5: id= ID
				{
					id = (Token) match(input, ID, FOLLOW_ID_in_basic962);

					type = Types.findType(id != null ? id.getText() : null);

				}
				break;

			}
		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		}

		finally {
			// do for sure before leaving
		}
		return type;
	}

	// $ANTLR end "basic"
	// $ANTLR start "cllist"
	// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:81:1: cllist returns [List<IClassNode> classes] : ( CLASS id= ID ( EXTENDS superId= ID )? LPAR (i= ID COLON t= basic ( COMMA i= ID COLON t= basic )* )? RPAR CLPAR ( FUN i= ID COLON t= basic LPAR (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )? RPAR ( LET ( VAR vari= ID COLON vart= basic ASS varv= exp SEMIC )* IN )? e= exp SEMIC )* CRPAR )* ;
	public final List<IClassNode> cllist() throws RecognitionException {
		List<IClassNode> classes = null;

		Token id = null;
		Token superId = null;
		Token i = null;
		Token argi = null;
		Token vari = null;
		IType t = null;

		IType argt = null;

		IType vart = null;

		IASTNode<?> varv = null;

		IASTNode<?> e = null;

		try {
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:82:2: ( ( CLASS id= ID ( EXTENDS superId= ID )? LPAR (i= ID COLON t= basic ( COMMA i= ID COLON t= basic )* )? RPAR CLPAR ( FUN i= ID COLON t= basic LPAR (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )? RPAR ( LET ( VAR vari= ID COLON vart= basic ASS varv= exp SEMIC )* IN )? e= exp SEMIC )* CRPAR )* )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:82:5: ( CLASS id= ID ( EXTENDS superId= ID )? LPAR (i= ID COLON t= basic ( COMMA i= ID COLON t= basic )* )? RPAR CLPAR ( FUN i= ID COLON t= basic LPAR (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )? RPAR ( LET ( VAR vari= ID COLON vart= basic ASS varv= exp SEMIC )* IN )? e= exp SEMIC )* CRPAR )*
			{

				classes = new ArrayList<IClassNode>();

				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:85:3: ( CLASS id= ID ( EXTENDS superId= ID )? LPAR (i= ID COLON t= basic ( COMMA i= ID COLON t= basic )* )? RPAR CLPAR ( FUN i= ID COLON t= basic LPAR (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )? RPAR ( LET ( VAR vari= ID COLON vart= basic ASS varv= exp SEMIC )* IN )? e= exp SEMIC )* CRPAR )*
				loop13: do {
					int alt13 = 2;
					final int LA13_0 = input.LA(1);

					if (LA13_0 == CLASS) {
						alt13 = 1;
					}

					switch (alt13) {
						case 1:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:85:4: CLASS id= ID ( EXTENDS superId= ID )? LPAR (i= ID COLON t= basic ( COMMA i= ID COLON t= basic )* )? RPAR CLPAR ( FUN i= ID COLON t= basic LPAR (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )? RPAR ( LET ( VAR vari= ID COLON vart= basic ASS varv= exp SEMIC )* IN )? e= exp SEMIC )* CRPAR
						{
							match(input, CLASS, FOLLOW_CLASS_in_cllist226);

							id = (Token) match(input, ID, FOLLOW_ID_in_cllist232);

							IClassNode cnode = new ClassNode(id != null ? id.getText() : null);
							ISymbolData data = mScope.pushSymbol(id != null ? id.getText() : null, cnode.getType(), SymbolType.CLASS);
							mScope.incLevel();

							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:91:4: ( EXTENDS superId= ID )?
							int alt5 = 2;
							final int LA5_0 = input.LA(1);

							if (LA5_0 == EXTENDS) {
								alt5 = 1;
							}
							switch (alt5) {
								case 1:
									// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:91:5: EXTENDS superId= ID
								{
									match(input, EXTENDS, FOLLOW_EXTENDS_in_cllist242);

									superId = (Token) match(input, ID, FOLLOW_ID_in_cllist248);

									cnode = new ClassNode(data.getSymbol(), Types.findDefinition(superId != null ? superId.getText() : null));

								}
								break;

							}

							classes.add(cnode);

							match(input, LPAR, FOLLOW_LPAR_in_cllist272);

							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:97:10: (i= ID COLON t= basic ( COMMA i= ID COLON t= basic )* )?
							int alt7 = 2;
							final int LA7_0 = input.LA(1);

							if (LA7_0 == ID) {
								alt7 = 1;
							}
							switch (alt7) {
								case 1:
									// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:97:11: i= ID COLON t= basic ( COMMA i= ID COLON t= basic )*
								{
									i = (Token) match(input, ID, FOLLOW_ID_in_cllist279);

									match(input, COLON, FOLLOW_COLON_in_cllist281);

									pushFollow(FOLLOW_basic_in_cllist287);
									t = basic();

									state._fsp--;

									data = mScope.pushSymbol(i != null ? i.getText() : null, t);
									IFieldNode fnode = new FieldNode(data);
									cnode.addField(fnode);

									// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:103:5: ( COMMA i= ID COLON t= basic )*
									loop6: do {
										int alt6 = 2;
										final int LA6_0 = input.LA(1);

										if (LA6_0 == COMMA) {
											alt6 = 1;
										}

										switch (alt6) {
											case 1:
												// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:103:6: COMMA i= ID COLON t= basic
											{
												match(input, COMMA, FOLLOW_COMMA_in_cllist300);

												i = (Token) match(input, ID, FOLLOW_ID_in_cllist306);

												match(input, COLON, FOLLOW_COLON_in_cllist308);

												pushFollow(FOLLOW_basic_in_cllist314);
												t = basic();

												state._fsp--;

												data = mScope.pushSymbol(i != null ? i.getText() : null, t);
												fnode = new FieldNode(data);
												cnode.addField(fnode);

											}
											break;

											default:
												break loop6;
										}
									} while (true);

								}
								break;

							}

							match(input, RPAR, FOLLOW_RPAR_in_cllist332);

							match(input, CLPAR, FOLLOW_CLPAR_in_cllist355);

							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:111:21: ( FUN i= ID COLON t= basic LPAR (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )? RPAR ( LET ( VAR vari= ID COLON vart= basic ASS varv= exp SEMIC )* IN )? e= exp SEMIC )*
							loop12: do {
								int alt12 = 2;
								final int LA12_0 = input.LA(1);

								if (LA12_0 == FUN) {
									alt12 = 1;
								}

								switch (alt12) {
									case 1:
										// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:112:21: FUN i= ID COLON t= basic LPAR (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )? RPAR ( LET ( VAR vari= ID COLON vart= basic ASS varv= exp SEMIC )* IN )? e= exp SEMIC
									{
										match(input, FUN, FOLLOW_FUN_in_cllist399);

										i = (Token) match(input, ID, FOLLOW_ID_in_cllist405);

										match(input, COLON, FOLLOW_COLON_in_cllist407);

										pushFollow(FOLLOW_basic_in_cllist413);
										t = basic();

										state._fsp--;

										match(input, LPAR, FOLLOW_LPAR_in_cllist415);

										data = mScope.pushSymbol(i != null ? i.getText() : null, function(t), SymbolType.FUNC);
										final IMethodNode mnode = new MethodNode(data, cnode);
										cnode.addMethod(mnode);

										mScope.incLevel();

										// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:120:22: (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )?
										int alt9 = 2;
										final int LA9_0 = input.LA(1);

										if (LA9_0 == ID) {
											alt9 = 1;
										}
										switch (alt9) {
											case 1:
												// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:120:23: argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )*
											{
												argi = (Token) match(input, ID, FOLLOW_ID_in_cllist466);

												match(input, COLON, FOLLOW_COLON_in_cllist468);

												pushFollow(FOLLOW_type_in_cllist474);
												argt = type();

												state._fsp--;

												data = mScope.pushSymbol(argi != null ? argi.getText() : null, argt);
												DeclarationNode anode = new DeclarationNode(data);
												mnode.addParam(anode);

												// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:126:23: ( COMMA argi= ID COLON argt= type )*
												loop8: do {
													int alt8 = 2;
													final int LA8_0 = input.LA(1);

													if (LA8_0 == COMMA) {
														alt8 = 1;
													}

													switch (alt8) {
														case 1:
															// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:126:24: COMMA argi= ID COLON argt= type
														{
															match(input, COMMA, FOLLOW_COMMA_in_cllist523);

															argi = (Token) match(input, ID, FOLLOW_ID_in_cllist529);

															match(input, COLON, FOLLOW_COLON_in_cllist531);

															pushFollow(FOLLOW_type_in_cllist537);
															argt = type();

															state._fsp--;

															data = mScope.pushSymbol(argi != null ? argi.getText() : null, argt);
															anode = new DeclarationNode(data);
															mnode.addParam(anode);

														}
														break;

														default:
															break loop8;
													}
												} while (true);

											}
											break;

										}

										match(input, RPAR, FOLLOW_RPAR_in_cllist634);

										// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:135:17: ( LET ( VAR vari= ID COLON vart= basic ASS varv= exp SEMIC )* IN )?
										int alt11 = 2;
										final int LA11_0 = input.LA(1);

										if (LA11_0 == LET) {
											alt11 = 1;
										}
										switch (alt11) {
											case 1:
												// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:135:18: LET ( VAR vari= ID COLON vart= basic ASS varv= exp SEMIC )* IN
											{
												match(input, LET, FOLLOW_LET_in_cllist653);

												// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:136:18: ( VAR vari= ID COLON vart= basic ASS varv= exp SEMIC )*
												loop10: do {
													int alt10 = 2;
													final int LA10_0 = input.LA(1);

													if (LA10_0 == VAR) {
														alt10 = 1;
													}

													switch (alt10) {
														case 1:
															// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:136:19: VAR vari= ID COLON vart= basic ASS varv= exp SEMIC
														{
															match(input, VAR, FOLLOW_VAR_in_cllist673);

															vari = (Token) match(input, ID, FOLLOW_ID_in_cllist679);

															match(input, COLON, FOLLOW_COLON_in_cllist681);

															pushFollow(FOLLOW_basic_in_cllist687);
															vart = basic();

															state._fsp--;

															match(input, ASS, FOLLOW_ASS_in_cllist689);

															pushFollow(FOLLOW_exp_in_cllist695);
															varv = exp();

															state._fsp--;

															match(input, SEMIC, FOLLOW_SEMIC_in_cllist697);

															data = mScope.pushSymbol(vari != null ? vari.getText() : null, vart);
															final IVariableNode anode = new VarNode(data, varv);
															mnode.addParam(anode);

														}
														break;

														default:
															break loop10;
													}
												} while (true);

												match(input, IN, FOLLOW_IN_in_cllist738);

											}
											break;

										}

										pushFollow(FOLLOW_exp_in_cllist747);
										e = exp();

										state._fsp--;

										match(input, SEMIC, FOLLOW_SEMIC_in_cllist749);

										mnode.setExpression((IASTNode<Object>) e);
										mScope.decLevel();

									}
									break;

									default:
										break loop12;
								}
							} while (true);

							match(input, CRPAR, FOLLOW_CRPAR_in_cllist820);

							mScope.decLevel();

						}
						break;

						default:
							break loop13;
					}
				} while (true);

			}

		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		}

		finally {
			// do for sure before leaving
		}
		return classes;
	}

	// $ANTLR end "cllist"
	// $ANTLR start "dec"
	// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:33:1: dec returns [List<IASTNode<?>> nodes] : ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )* ;
	public final List<IASTNode<?>> dec() throws RecognitionException {
		List<IASTNode<?>> nodes = null;

		Token i = null;
		Token argi = null;
		IType t = null;

		IASTNode<?> e = null;

		IType argt = null;

		List<IASTNode<?>> d = null;

		try {
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:34:2: ( ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )* )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:34:4: ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )*
			{

				nodes = new ArrayList<IASTNode<?>>();

				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:37:3: ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )*
				loop4: do {
					int alt4 = 3;
					final int LA4_0 = input.LA(1);

					if (LA4_0 == VAR) {
						alt4 = 1;
					} else if (LA4_0 == FUN) {
						alt4 = 2;
					}

					switch (alt4) {
						case 1:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:37:4: VAR i= ID COLON t= type ASS e= exp SEMIC
						{
							match(input, VAR, FOLLOW_VAR_in_dec46);

							i = (Token) match(input, ID, FOLLOW_ID_in_dec50);

							match(input, COLON, FOLLOW_COLON_in_dec52);

							pushFollow(FOLLOW_type_in_dec56);
							t = type();

							state._fsp--;

							match(input, ASS, FOLLOW_ASS_in_dec58);

							pushFollow(FOLLOW_exp_in_dec62);
							e = exp();

							state._fsp--;

							match(input, SEMIC, FOLLOW_SEMIC_in_dec64);

							final ISymbolData data = mScope.pushSymbol(i != null ? i.getText() : null, t);
							final VarNode vnode = new VarNode(data, e);
							nodes.add(vnode);

						}
						break;
						case 2:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:43:5: FUN i= ID COLON t= type LPAR (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC
						{
							match(input, FUN, FOLLOW_FUN_in_dec75);

							i = (Token) match(input, ID, FOLLOW_ID_in_dec79);

							match(input, COLON, FOLLOW_COLON_in_dec81);

							pushFollow(FOLLOW_type_in_dec85);
							t = type();

							state._fsp--;

							ISymbolData data = mScope.pushSymbol(i != null ? i.getText() : null, function(t), SymbolType.FUNC);
							final FunNode fnode = new FunNode(data, null);
							nodes.add(fnode);

							mScope.incLevel();

							match(input, LPAR, FOLLOW_LPAR_in_dec105);

							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:52:3: (argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )* )?
							int alt2 = 2;
							final int LA2_0 = input.LA(1);

							if (LA2_0 == ID) {
								alt2 = 1;
							}
							switch (alt2) {
								case 1:
									// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:52:4: argi= ID COLON argt= type ( COMMA argi= ID COLON argt= type )*
								{
									argi = (Token) match(input, ID, FOLLOW_ID_in_dec113);

									match(input, COLON, FOLLOW_COLON_in_dec115);

									pushFollow(FOLLOW_type_in_dec119);
									argt = type();

									state._fsp--;

									data = mScope.pushSymbol(argi != null ? argi.getText() : null, argt);
									DeclarationNode decnode = new DeclarationNode(data);
									fnode.addParam(decnode);

									// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:58:4: ( COMMA argi= ID COLON argt= type )*
									loop1: do {
										int alt1 = 2;
										final int LA1_0 = input.LA(1);

										if (LA1_0 == COMMA) {
											alt1 = 1;
										}

										switch (alt1) {
											case 1:
												// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:58:5: COMMA argi= ID COLON argt= type
											{
												match(input, COMMA, FOLLOW_COMMA_in_dec129);

												argi = (Token) match(input, ID, FOLLOW_ID_in_dec133);

												match(input, COLON, FOLLOW_COLON_in_dec135);

												pushFollow(FOLLOW_type_in_dec139);
												argt = type();

												state._fsp--;

												data = mScope.pushSymbol(argi != null ? argi.getText() : null, argt);
												decnode = new DeclarationNode(data);
												fnode.addParam(decnode);

											}
											break;

											default:
												break loop1;
										}
									} while (true);

								}
								break;

							}

							match(input, RPAR, FOLLOW_RPAR_in_dec163);

							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:67:4: ( LET d= dec IN )?
							int alt3 = 2;
							final int LA3_0 = input.LA(1);

							if (LA3_0 == LET) {
								alt3 = 1;
							}
							switch (alt3) {
								case 1:
									// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:67:5: LET d= dec IN
								{
									match(input, LET, FOLLOW_LET_in_dec169);

									pushFollow(FOLLOW_dec_in_dec173);
									d = dec();

									state._fsp--;

									match(input, IN, FOLLOW_IN_in_dec175);

								}
								break;

							}

							pushFollow(FOLLOW_exp_in_dec181);
							e = exp();

							state._fsp--;

							match(input, SEMIC, FOLLOW_SEMIC_in_dec183);

							if (d != null) {
								for (final IASTNode<?> dec : d) {
									fnode.addDeclaration(dec);
								}
							}
							fnode.setExpression(e);
							mScope.decLevel();
							d = null;

						}
						break;

						default:
							break loop4;
					}
				} while (true);

			}

		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		}

		finally {
			// do for sure before leaving
		}
		return nodes;
	}

	// $ANTLR end "dec"
	// $ANTLR start "exp"
	// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:208:1: exp returns [IASTNode<?> node] : t1= term ( PLUS t2= term | MINUS t3= term | OR t4= term | XOR t5= term )* ;
	public final IASTNode<?> exp() throws RecognitionException {
		IASTNode<?> node = null;

		IASTNode<?> t1 = null;

		IASTNode<?> t2 = null;

		IASTNode<?> t3 = null;

		IASTNode<?> t4 = null;

		IASTNode<?> t5 = null;

		try {
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:209:2: (t1= term ( PLUS t2= term | MINUS t3= term | OR t4= term | XOR t5= term )* )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:209:4: t1= term ( PLUS t2= term | MINUS t3= term | OR t4= term | XOR t5= term )*
			{
				pushFollow(FOLLOW_term_in_exp1156);
				t1 = term();

				state._fsp--;

				node = t1;

				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:213:3: ( PLUS t2= term | MINUS t3= term | OR t4= term | XOR t5= term )*
				loop19: do {
					int alt19 = 5;
					switch (input.LA(1)) {
						case PLUS: {
							alt19 = 1;
						}
						break;
						case MINUS: {
							alt19 = 2;
						}
						break;
						case OR: {
							alt19 = 3;
						}
						break;
						case XOR: {
							alt19 = 4;
						}
						break;

					}

					switch (alt19) {
						case 1:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:214:4: PLUS t2= term
						{
							match(input, PLUS, FOLLOW_PLUS_in_exp1170);

							pushFollow(FOLLOW_term_in_exp1176);
							t2 = term();

							state._fsp--;

							node = new SumNode(node, t2);

						}
						break;
						case 2:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:218:6: MINUS t3= term
						{
							match(input, MINUS, FOLLOW_MINUS_in_exp1189);

							pushFollow(FOLLOW_term_in_exp1195);
							t3 = term();

							state._fsp--;

							node = new SubNode(node, t3);

						}
						break;
						case 3:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:222:6: OR t4= term
						{
							match(input, OR, FOLLOW_OR_in_exp1207);

							pushFollow(FOLLOW_term_in_exp1213);
							t4 = term();

							state._fsp--;

							node = new OrNode(node, t4);

						}
						break;
						case 4:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:226:6: XOR t5= term
						{
							match(input, XOR, FOLLOW_XOR_in_exp1225);

							pushFollow(FOLLOW_term_in_exp1231);
							t5 = term();

							state._fsp--;

							node = new XorNode(node, t5);

						}
						break;

						default:
							break loop19;
					}
				} while (true);

			}

		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		}

		finally {
			// do for sure before leaving
		}
		return node;
	}

	// $ANTLR end "exp"
	// $ANTLR start "fact"
	// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:255:1: fact returns [IASTNode<?> node] : v1= value ( EQ v2= value | GE v3= value | LE v4= value | GT v5= value | LW v6= value | NE v7= value )* ;
	public final IASTNode<?> fact() throws RecognitionException {
		IASTNode<?> node = null;

		IASTNode<?> v1 = null;

		IASTNode<?> v2 = null;

		IASTNode<?> v3 = null;

		IASTNode<?> v4 = null;

		IASTNode<?> v5 = null;

		IASTNode<?> v6 = null;

		IASTNode<?> v7 = null;

		try {
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:256:2: (v1= value ( EQ v2= value | GE v3= value | LE v4= value | GT v5= value | LW v6= value | NE v7= value )* )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:256:4: v1= value ( EQ v2= value | GE v3= value | LE v4= value | GT v5= value | LW v6= value | NE v7= value )*
			{
				pushFollow(FOLLOW_value_in_fact1349);
				v1 = value();

				state._fsp--;

				node = v1;

				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:260:3: ( EQ v2= value | GE v3= value | LE v4= value | GT v5= value | LW v6= value | NE v7= value )*
				loop21: do {
					int alt21 = 7;
					switch (input.LA(1)) {
						case EQ: {
							alt21 = 1;
						}
						break;
						case GE: {
							alt21 = 2;
						}
						break;
						case LE: {
							alt21 = 3;
						}
						break;
						case GT: {
							alt21 = 4;
						}
						break;
						case LW: {
							alt21 = 5;
						}
						break;
						case NE: {
							alt21 = 6;
						}
						break;

					}

					switch (alt21) {
						case 1:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:261:4: EQ v2= value
						{
							match(input, EQ, FOLLOW_EQ_in_fact1363);

							pushFollow(FOLLOW_value_in_fact1369);
							v2 = value();

							state._fsp--;

							node = new EqualsNode(node, v2);

						}
						break;
						case 2:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:265:6: GE v3= value
						{
							match(input, GE, FOLLOW_GE_in_fact1382);

							pushFollow(FOLLOW_value_in_fact1388);
							v3 = value();

							state._fsp--;

							node = new GreaterEqualsNode(node, v3);

						}
						break;
						case 3:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:269:6: LE v4= value
						{
							match(input, LE, FOLLOW_LE_in_fact1401);

							pushFollow(FOLLOW_value_in_fact1407);
							v4 = value();

							state._fsp--;

							node = new LowerEqualsNode(node, v4);

						}
						break;
						case 4:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:273:6: GT v5= value
						{
							match(input, GT, FOLLOW_GT_in_fact1420);

							pushFollow(FOLLOW_value_in_fact1426);
							v5 = value();

							state._fsp--;

							node = new GreaterNode(node, v5);

						}
						break;
						case 5:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:277:6: LW v6= value
						{
							match(input, LW, FOLLOW_LW_in_fact1439);

							pushFollow(FOLLOW_value_in_fact1445);
							v6 = value();

							state._fsp--;

							node = new LowerNode(node, v6);

						}
						break;
						case 6:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:281:6: NE v7= value
						{
							match(input, NE, FOLLOW_NE_in_fact1458);

							pushFollow(FOLLOW_value_in_fact1464);
							v7 = value();

							state._fsp--;

							node = new NotEqualsNode(node, v7);

						}
						break;

						default:
							break loop21;
					}
				} while (true);

			}

		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		}

		finally {
			// do for sure before leaving
		}
		return node;
	}

	// $ANTLR end "fact"
	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	@Override
	public String getGrammarFileName() {
		return "/home/gciatto/Scrivania/svm-git/svm/res/FOOL.g";
	}

	@Override
	public String[] getTokenNames() {
		return FOOLParser.tokenNames;
	}

	private boolean isMethodInvocation(final ISymbolData symbol) {
		return symbol != null && symbol.getSymbolType() == SymbolType.FUNC && symbol.getDefinitionNode() instanceof IMethodNode;
	}

	// $ANTLR start "prog"
	// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:190:1: prog returns [IASTNode<?> node] : (e= exp SEMIC | LET c= cllist d= dec IN e= exp SEMIC ) ;
	public final IASTNode<?> prog() throws RecognitionException {
		IASTNode<?> node = null;

		IASTNode<?> e = null;

		List<IClassNode> c = null;

		List<IASTNode<?>> d = null;

		try {
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:191:2: ( (e= exp SEMIC | LET c= cllist d= dec IN e= exp SEMIC ) )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:192:3: (e= exp SEMIC | LET c= cllist d= dec IN e= exp SEMIC )
			{

				final LetInNode linode = new LetInNode(mScope, null, null, null);
				node = new ProgramNode(linode);

				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:196:3: (e= exp SEMIC | LET c= cllist d= dec IN e= exp SEMIC )
				int alt18 = 2;
				final int LA18_0 = input.LA(1);

				if (LA18_0 == FALSE || LA18_0 >= ID && LA18_0 <= IF || LA18_0 == INTEGER || LA18_0 == LPAR || LA18_0 >= NEW && LA18_0 <= NULL || LA18_0 == PRINT || LA18_0 == TRUE) {
					alt18 = 1;
				} else if (LA18_0 == LET) {
					alt18 = 2;
				} else {
					final NoViableAltException nvae = new NoViableAltException("", 18, 0, input);

					throw nvae;

				}
				switch (alt18) {
					case 1:
						// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:196:4: e= exp SEMIC
					{
						pushFollow(FOLLOW_exp_in_prog1104);
						e = exp();

						state._fsp--;

						match(input, SEMIC, FOLLOW_SEMIC_in_prog1106);

						linode.setExpression(e);

					}
					break;
					case 2:
						// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:200:5: LET c= cllist d= dec IN e= exp SEMIC
					{
						match(input, LET, FOLLOW_LET_in_prog1116);

						pushFollow(FOLLOW_cllist_in_prog1120);
						c = cllist();

						state._fsp--;

						pushFollow(FOLLOW_dec_in_prog1124);
						d = dec();

						state._fsp--;

						match(input, IN, FOLLOW_IN_in_prog1126);

						pushFollow(FOLLOW_exp_in_prog1130);
						e = exp();

						state._fsp--;

						match(input, SEMIC, FOLLOW_SEMIC_in_prog1132);

						linode.addClasses(c);
						linode.addDeclarations(d);
						linode.setExpression(e);

					}
					break;

				}

			}

		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		}

		finally {
			// do for sure before leaving
		}
		return node;
	}

	// $ANTLR end "prog"
	// $ANTLR start "term"
	// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:233:1: term returns [IASTNode<?> node] : f1= fact ( MULT f2= fact | DIV f3= fact | AND f4= fact )* ;
	public final IASTNode<?> term() throws RecognitionException {
		IASTNode<?> node = null;

		IASTNode<?> f1 = null;

		IASTNode<?> f2 = null;

		IASTNode<?> f3 = null;

		IASTNode<?> f4 = null;

		try {
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:234:2: (f1= fact ( MULT f2= fact | DIV f3= fact | AND f4= fact )* )
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:234:4: f1= fact ( MULT f2= fact | DIV f3= fact | AND f4= fact )*
			{
				pushFollow(FOLLOW_fact_in_term1260);
				f1 = fact();

				state._fsp--;

				node = f1;

				// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:238:3: ( MULT f2= fact | DIV f3= fact | AND f4= fact )*
				loop20: do {
					int alt20 = 4;
					switch (input.LA(1)) {
						case MULT: {
							alt20 = 1;
						}
						break;
						case DIV: {
							alt20 = 2;
						}
						break;
						case AND: {
							alt20 = 3;
						}
						break;

					}

					switch (alt20) {
						case 1:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:239:4: MULT f2= fact
						{
							match(input, MULT, FOLLOW_MULT_in_term1274);

							pushFollow(FOLLOW_fact_in_term1280);
							f2 = fact();

							state._fsp--;

							node = new MultNode(node, f2);

						}
						break;
						case 2:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:243:6: DIV f3= fact
						{
							match(input, DIV, FOLLOW_DIV_in_term1293);

							pushFollow(FOLLOW_fact_in_term1299);
							f3 = fact();

							state._fsp--;

							node = new DivNode(node, f3);

						}
						break;
						case 3:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:247:6: AND f4= fact
						{
							match(input, AND, FOLLOW_AND_in_term1312);

							pushFollow(FOLLOW_fact_in_term1318);
							f4 = fact();

							state._fsp--;

							node = new AndNode(node, f4);

						}
						break;

						default:
							break loop20;
					}
				} while (true);

			}

		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		}

		finally {
			// do for sure before leaving
		}
		return node;
	}

	// $ANTLR end "term"
	// $ANTLR start "type"
	// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:154:1: type returns [IType type] : (b= basic |a= arrow );
	public final IType type() throws RecognitionException {
		IType type = null;

		IType b = null;

		IFunctionType a = null;

		try {
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:155:2: (b= basic |a= arrow )
			int alt14 = 2;
			final int LA14_0 = input.LA(1);

			if (LA14_0 == BOOL || LA14_0 == ID || LA14_0 == INT) {
				alt14 = 1;
			} else if (LA14_0 == LPAR) {
				alt14 = 2;
			} else {
				final NoViableAltException nvae = new NoViableAltException("", 14, 0, input);

				throw nvae;

			}
			switch (alt14) {
				case 1:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:155:6: b= basic
				{
					pushFollow(FOLLOW_basic_in_type881);
					b = basic();

					state._fsp--;

					type = b;

				}
				break;
				case 2:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:159:5: a= arrow
				{
					pushFollow(FOLLOW_arrow_in_type896);
					a = arrow();

					state._fsp--;

					type = a;

				}
				break;

			}
		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		}

		finally {
			// do for sure before leaving
		}
		return type;
	}

	// $ANTLR end "type"
	// $ANTLR start "value"
	// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:288:1: value returns [IASTNode<?> node] : (n= INTEGER |b= TRUE |b= FALSE | IF c= exp THEN CLPAR et= exp CRPAR ELSE CLPAR ef= exp CRPAR | LPAR e= exp RPAR | NOT e= exp | PRINT LPAR e= exp RPAR |i= ID ( LPAR (arg= exp ( COMMA arg= exp )* )? RPAR | DOT i1= ID LPAR (arg= exp ( COMMA arg= exp )* )? RPAR )? | NULL | NEW id= ID LPAR (arg= exp ( COMMA arg= exp )* )? RPAR );
	public final IASTNode<?> value() throws RecognitionException {
		IASTNode<?> node = null;

		Token n = null;
		Token b = null;
		Token i = null;
		Token i1 = null;
		Token id = null;
		IASTNode<?> c = null;

		IASTNode<?> et = null;

		IASTNode<?> ef = null;

		IASTNode<?> e = null;

		IASTNode<?> arg = null;

		try {
			// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:289:2: (n= INTEGER |b= TRUE |b= FALSE | IF c= exp THEN CLPAR et= exp CRPAR ELSE CLPAR ef= exp CRPAR | LPAR e= exp RPAR | NOT e= exp | PRINT LPAR e= exp RPAR |i= ID ( LPAR (arg= exp ( COMMA arg= exp )* )? RPAR | DOT i1= ID LPAR (arg= exp ( COMMA arg= exp )* )? RPAR )? | NULL | NEW id= ID LPAR (arg= exp ( COMMA arg= exp )* )? RPAR )
			int alt29 = 10;
			switch (input.LA(1)) {
				case INTEGER: {
					alt29 = 1;
				}
				break;
				case TRUE: {
					alt29 = 2;
				}
				break;
				case FALSE: {
					alt29 = 3;
				}
				break;
				case IF: {
					alt29 = 4;
				}
				break;
				case LPAR: {
					alt29 = 5;
				}
				break;
				case NOT: {
					alt29 = 6;
				}
				break;
				case PRINT: {
					alt29 = 7;
				}
				break;
				case ID: {
					alt29 = 8;
				}
				break;
				case NULL: {
					alt29 = 9;
				}
				break;
				case NEW: {
					alt29 = 10;
				}
				break;
				default:
					final NoViableAltException nvae = new NoViableAltException("", 29, 0, input);

					throw nvae;

			}

			switch (alt29) {
				case 1:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:289:5: n= INTEGER
				{
					n = (Token) match(input, INTEGER, FOLLOW_INTEGER_in_value1495);

					node = new IntegerNode(Integer.parseInt(n != null ? n.getText() : null));

				}
				break;
				case 2:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:293:4: b= TRUE
				{
					b = (Token) match(input, TRUE, FOLLOW_TRUE_in_value1509);

					node = new BooleanNode(true);

				}
				break;
				case 3:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:297:4: b= FALSE
				{
					b = (Token) match(input, FALSE, FOLLOW_FALSE_in_value1523);

					node = new BooleanNode(false);

				}
				break;
				case 4:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:301:5: IF c= exp THEN CLPAR et= exp CRPAR ELSE CLPAR ef= exp CRPAR
				{
					match(input, IF, FOLLOW_IF_in_value1534);

					pushFollow(FOLLOW_exp_in_value1540);
					c = exp();

					state._fsp--;

					match(input, THEN, FOLLOW_THEN_in_value1542);

					match(input, CLPAR, FOLLOW_CLPAR_in_value1544);

					pushFollow(FOLLOW_exp_in_value1550);
					et = exp();

					state._fsp--;

					match(input, CRPAR, FOLLOW_CRPAR_in_value1552);

					match(input, ELSE, FOLLOW_ELSE_in_value1554);

					match(input, CLPAR, FOLLOW_CLPAR_in_value1556);

					pushFollow(FOLLOW_exp_in_value1562);
					ef = exp();

					state._fsp--;

					match(input, CRPAR, FOLLOW_CRPAR_in_value1564);

					node = new TernaryNode(c, et, ef);

				}
				break;
				case 5:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:305:4: LPAR e= exp RPAR
				{
					match(input, LPAR, FOLLOW_LPAR_in_value1574);

					pushFollow(FOLLOW_exp_in_value1580);
					e = exp();

					state._fsp--;

					match(input, RPAR, FOLLOW_RPAR_in_value1582);

					node = e;

				}
				break;
				case 6:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:309:4: NOT e= exp
				{
					match(input, NOT, FOLLOW_NOT_in_value1592);

					pushFollow(FOLLOW_exp_in_value1598);
					e = exp();

					state._fsp--;

					node = new NotNode(e);

				}
				break;
				case 7:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:313:4: PRINT LPAR e= exp RPAR
				{
					match(input, PRINT, FOLLOW_PRINT_in_value1607);

					match(input, LPAR, FOLLOW_LPAR_in_value1609);

					pushFollow(FOLLOW_exp_in_value1615);
					e = exp();

					state._fsp--;

					match(input, RPAR, FOLLOW_RPAR_in_value1617);

					node = new PrintNode(e);

				}
				break;
				case 8:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:319:4: i= ID ( LPAR (arg= exp ( COMMA arg= exp )* )? RPAR | DOT i1= ID LPAR (arg= exp ( COMMA arg= exp )* )? RPAR )?
				{
					i = (Token) match(input, ID, FOLLOW_ID_in_value1631);

					final IdNode inode = new IdNode(mScope.peekSymbol(i != null ? i.getText() : null), mScope.getLevel());
					node = inode;

					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:324:4: ( LPAR (arg= exp ( COMMA arg= exp )* )? RPAR | DOT i1= ID LPAR (arg= exp ( COMMA arg= exp )* )? RPAR )?
					int alt26 = 3;
					final int LA26_0 = input.LA(1);

					if (LA26_0 == LPAR) {
						alt26 = 1;
					} else if (LA26_0 == DOT) {
						alt26 = 2;
					}
					switch (alt26) {
						case 1:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:324:5: LPAR (arg= exp ( COMMA arg= exp )* )? RPAR
						{
							match(input, LPAR, FOLLOW_LPAR_in_value1642);

							final ISymbolData data = mScope.peekSymbol(i != null ? i.getText() : null);
							CallNode cnode;
							if (isMethodInvocation(data)) {
								final IMethodNode def = (IMethodNode) data.getDefinitionNode();
								cnode = (CallNode) def.getCallNode(null, mScope.getLevel());
							} else {
								cnode = new CallNode(data, mScope.getLevel());
							}
							node = cnode;

							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:336:5: (arg= exp ( COMMA arg= exp )* )?
							int alt23 = 2;
							final int LA23_0 = input.LA(1);

							if (LA23_0 == FALSE || LA23_0 >= ID && LA23_0 <= IF || LA23_0 == INTEGER || LA23_0 == LPAR || LA23_0 >= NEW && LA23_0 <= NULL || LA23_0 == PRINT || LA23_0 == TRUE) {
								alt23 = 1;
							}
							switch (alt23) {
								case 1:
									// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:336:6: arg= exp ( COMMA arg= exp )*
								{
									pushFollow(FOLLOW_exp_in_value1657);
									arg = exp();

									state._fsp--;

									cnode.addParam(arg);

									// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:340:6: ( COMMA arg= exp )*
									loop22: do {
										int alt22 = 2;
										final int LA22_0 = input.LA(1);

										if (LA22_0 == COMMA) {
											alt22 = 1;
										}

										switch (alt22) {
											case 1:
												// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:340:7: COMMA arg= exp
											{
												match(input, COMMA, FOLLOW_COMMA_in_value1672);

												pushFollow(FOLLOW_exp_in_value1676);
												arg = exp();

												state._fsp--;

												cnode.addParam(arg);

											}
											break;

											default:
												break loop22;
										}
									} while (true);

								}
								break;

							}

							match(input, RPAR, FOLLOW_RPAR_in_value1704);

							cnode.validate();

						}
						break;
						case 2:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:350:6: DOT i1= ID LPAR (arg= exp ( COMMA arg= exp )* )? RPAR
						{
							match(input, DOT, FOLLOW_DOT_in_value1717);

							i1 = (Token) match(input, ID, FOLLOW_ID_in_value1723);

							match(input, LPAR, FOLLOW_LPAR_in_value1725);

							// Dot
							final IClassNode def = Types.getDefinition(inode.getType());
							final IMethodNode met = def.getMethodByName(i1 != null ? i1.getText() : null);
							final IMethodCallNode mcnode = met.getCallNode(inode, mScope.getLevel());
							node = mcnode;

							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:358:6: (arg= exp ( COMMA arg= exp )* )?
							int alt25 = 2;
							final int LA25_0 = input.LA(1);

							if (LA25_0 == FALSE || LA25_0 >= ID && LA25_0 <= IF || LA25_0 == INTEGER || LA25_0 == LPAR || LA25_0 >= NEW && LA25_0 <= NULL || LA25_0 == PRINT || LA25_0 == TRUE) {
								alt25 = 1;
							}
							switch (alt25) {
								case 1:
									// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:358:7: arg= exp ( COMMA arg= exp )*
								{
									pushFollow(FOLLOW_exp_in_value1744);
									arg = exp();

									state._fsp--;

									mcnode.addParam((IASTNode<Object>) arg);

									// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:362:7: ( COMMA arg= exp )*
									loop24: do {
										int alt24 = 2;
										final int LA24_0 = input.LA(1);

										if (LA24_0 == COMMA) {
											alt24 = 1;
										}

										switch (alt24) {
											case 1:
												// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:363:8: COMMA arg= exp
											{
												match(input, COMMA, FOLLOW_COMMA_in_value1768);

												pushFollow(FOLLOW_exp_in_value1774);
												arg = exp();

												state._fsp--;

												mcnode.addParam((IASTNode<Object>) arg);

											}
											break;

											default:
												break loop24;
										}
									} while (true);

									mcnode.validate();

								}
								break;

							}

							match(input, RPAR, FOLLOW_RPAR_in_value1809);

						}
						break;

					}

				}
				break;
				case 9:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:373:5: NULL
				{
					match(input, NULL, FOLLOW_NULL_in_value1821);

					node = new NullNode(mScope);

				}
				break;
				case 10:
					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:377:5: NEW id= ID LPAR (arg= exp ( COMMA arg= exp )* )? RPAR
				{
					match(input, NEW, FOLLOW_NEW_in_value1835);

					id = (Token) match(input, ID, FOLLOW_ID_in_value1841);

					match(input, LPAR, FOLLOW_LPAR_in_value1843);

					final IConstructorCallNode cnode = Types.findDefinition(id != null ? id.getText() : null).getConstructor().getCallNode(mScope.getLevel());
					node = cnode;

					// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:382:4: (arg= exp ( COMMA arg= exp )* )?
					int alt28 = 2;
					final int LA28_0 = input.LA(1);

					if (LA28_0 == FALSE || LA28_0 >= ID && LA28_0 <= IF || LA28_0 == INTEGER || LA28_0 == LPAR || LA28_0 >= NEW && LA28_0 <= NULL || LA28_0 == PRINT || LA28_0 == TRUE) {
						alt28 = 1;
					}
					switch (alt28) {
						case 1:
							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:382:5: arg= exp ( COMMA arg= exp )*
						{
							pushFollow(FOLLOW_exp_in_value1858);
							arg = exp();

							state._fsp--;

							cnode.addParam((IASTNode<Object>) arg);

							// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:386:5: ( COMMA arg= exp )*
							loop27: do {
								int alt27 = 2;
								final int LA27_0 = input.LA(1);

								if (LA27_0 == COMMA) {
									alt27 = 1;
								}

								switch (alt27) {
									case 1:
										// /home/gciatto/Scrivania/svm-git/svm/res/FOOL.g:386:6: COMMA arg= exp
									{
										match(input, COMMA, FOLLOW_COMMA_in_value1871);

										pushFollow(FOLLOW_exp_in_value1877);
										arg = exp();

										state._fsp--;

										cnode.addParam((IASTNode<Object>) arg);

									}
									break;

									default:
										break loop27;
								}
							} while (true);

						}
						break;

					}

					match(input, RPAR, FOLLOW_RPAR_in_value1895);

					cnode.validate();

				}
				break;

			}
		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		}

		finally {
			// do for sure before leaving
		}
		return node;
	}
	// $ANTLR end "value"

}