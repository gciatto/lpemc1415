package it.unibo.gciatto.sdt.scoping;

import it.unibo.gciatto.sdt.exception.MultiplyDeclaredException;
import it.unibo.gciatto.sdt.typing.IType;

import java.util.List;
import java.util.Set;

public interface IScope {
	void decLevel();

	int getLevel();

	Set<String> getScopeSymbols();

	List<ISymbolData> getSymbolDatas(String symbol);

	void incLevel();

	int peekLevel(String symbol);
	
	ISymbolData peekSymbol(String symbol);

	SymbolType peekSymbolType(String symbol);

	IType peekType(String symbol);

	ISymbolData popSymbol(String symbol);
	
	ISymbolData pushSymbol(String symbol, IType type) throws MultiplyDeclaredException;

	ISymbolData pushSymbol(String symbol, IType type, SymbolType stype) throws MultiplyDeclaredException;
}
