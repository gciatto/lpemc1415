package it.unibo.gciatto.sdt.scoping;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.typing.IType;

public interface ISymbolData extends Comparable<ISymbolData> {
	IASTNode<?> getDefinitionNode();

	int getLevel();

	String getSymbol();

	SymbolType getSymbolType();

	IType getType();

	void setDefinitionNode(IASTNode<?> node);

	void setLevel(int level);

	void setSymbolType(SymbolType type);

	void setType(IType type);
}