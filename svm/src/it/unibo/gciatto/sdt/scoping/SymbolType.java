package it.unibo.gciatto.sdt.scoping;

public enum SymbolType {
	VAR,
	FUNC,
	CLASS;
}
