package it.unibo.gciatto.sdt.scoping.impl;

import static it.unibo.gciatto.sdt.typing.Types.BOOLEAN;
import static it.unibo.gciatto.sdt.typing.Types.INTEGER;
import static it.unibo.gciatto.sdt.typing.Types.VOID;
import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.exception.MultiplyDeclaredException;
import it.unibo.gciatto.sdt.exception.NotDeclaredException;
import it.unibo.gciatto.sdt.scoping.IScope;
import it.unibo.gciatto.sdt.scoping.ISymbolData;
import it.unibo.gciatto.sdt.scoping.SymbolType;
import it.unibo.gciatto.sdt.typing.IFunctionType;
import it.unibo.gciatto.sdt.typing.IType;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Scope implements IScope {
	private static interface ISymbolDataDequeConsumer<T> {
		T consume(Deque<ISymbolData> deque, ISymbolData peek);
	}
	
	private static class SymbolData implements ISymbolData {
		private int mLevel = 0;
		private IType mType = VOID;
		private SymbolType mSType = SymbolType.VAR;
		private final String mSymbol;
		private IASTNode<?> mDefinition;
		
		public SymbolData(final String symbol, final int level, final IType type) {
			mLevel = level;
			mType = type;
			mSymbol = symbol;
		}

		public SymbolData(final String symbol, final int level, final IType type, final SymbolType stype) {
			mLevel = level;
			mType = type;
			mSType = stype;
			mSymbol = symbol;
		}
		
		@Override
		public int compareTo(final ISymbolData o) {
			return o.getLevel() - getLevel();
		}
		
		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof SymbolData)) {
				return false;
			}
			final SymbolData other = (SymbolData) obj;
			if (mLevel != other.mLevel) {
				return false;
			}
			if (mType == null) {
				if (other.mType != null) {
					return false;
				}
			} else if (!mType.getName().equals(other.mType.getName())) {
				return false;
			}
			return true;
		}
		
		@Override
		public IASTNode<?> getDefinitionNode() {
			return mDefinition;
		}
		
		@Override
		public int getLevel() {
			return mLevel;
		}
		
		@Override
		public String getSymbol() {
			return mSymbol;
		}
		
		@Override
		public SymbolType getSymbolType() {
			return mSType;
		}
		
		@Override
		public IType getType() {
			return mType;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + mLevel;
			result = prime * result + (mType == null ? 0 : mType.hashCode());
			return result;
		}
		
		@Override
		public void setDefinitionNode(final IASTNode<?> node) {
			mDefinition = node;
		}
		
		@Override
		public void setLevel(final int level) {
			mLevel = level;
		}
		
		@Override
		public void setSymbolType(final SymbolType type) {
			mSType = type;
		}
		
		@Override
		public void setType(final IType type) {
			mType = type;
		}
		
		@Override
		public String toString() {
			return String.format("{%s, %s, %s}", mLevel, mType, mSType);
		}
		
	}

	public static void main(final String[] args) {
		final IScope scope = new Scope();
		scope.pushSymbol("a", BOOLEAN);
		scope.pushSymbol("b", INTEGER);
		System.out.printf("%s\n\n", scope);
		
		scope.incLevel();
		scope.pushSymbol("a", INTEGER);
		scope.pushSymbol("b", BOOLEAN);
		System.out.printf("%s\n\n", scope);
		
		scope.incLevel();
		scope.pushSymbol("a", VOID);
		scope.pushSymbol("b", VOID);
		scope.pushSymbol("c", VOID);
		System.out.printf("%s\n\n", scope);
		
		scope.decLevel();
		System.out.printf("%s\n\n", scope);
		
		// scope.pushSymbol("a", Object.class);
	}
	
	private int mLevel = 0;
	
	private final Map<String, Deque<ISymbolData>> mScope = new HashMap<String, Deque<ISymbolData>>();
	
	public Scope() {
		this(0);
	}

	public Scope(final int initialLevel) {
		mLevel = initialLevel;
	}

	@Override
	public void decLevel() {
		final LinkedList<String> keysToRemove = new LinkedList<String>();
		mScope.forEach((s, d) -> {
			final ISymbolData peek = d.peek();
			if (peek != null && peek.getLevel() >= getLevel()) {
				d.pop();
				if (d.isEmpty()) {
					keysToRemove.add(s);
				}
			}
		});
		keysToRemove.forEach(s -> mScope.remove(s));
		mLevel--;
	}
	
	@Override
	public int getLevel() {
		return mLevel;
	}
	
	@Override
	public Set<String> getScopeSymbols() {
		return new HashSet<String>(mScope.keySet());
	}
	
	@Override
	public List<ISymbolData> getSymbolDatas(final String symbol) {
		return getSymbolDeque(symbol, (deque, peek) -> new ArrayList<ISymbolData>(deque));
	}
	
	protected <T> T getSymbolDeque(final String symbol, final ISymbolDataDequeConsumer<T> consumer) {
		final Deque<ISymbolData> deque = mScope.get(symbol);
		return consumer.consume(deque, deque != null ? deque.peek() : null);
	}
	
	@Override
	public void incLevel() {
		mLevel++;
	}
	
	@Override
	public int peekLevel(final String symbol) {
		final int level = getSymbolDeque(symbol, (deque, peek) -> peek == null ? -1 : peek.getLevel());
		if (level == -1) {
			throw new NotDeclaredException(symbol);
		}
		return level;
	}
	
	@Override
	public ISymbolData peekSymbol(final String symbol) {
		final ISymbolData data = getSymbolDeque(symbol, (deque, peek) -> peek);
		if (data == null) {
			throw new NotDeclaredException(symbol);
		}
		return data;
	}
	
	@Override
	public SymbolType peekSymbolType(final String symbol) {
		final SymbolType stype = getSymbolDeque(symbol, (deque, peek) -> peek == null ? null : peek.getSymbolType());
		if (stype == null) {
			throw new NotDeclaredException(symbol);
		}
		return stype;
	}

	@Override
	public IType peekType(final String symbol) {
		final IType type = getSymbolDeque(symbol, (deque, peek) -> peek == null ? null : peek.getType());
		if (type == null) {
			throw new NotDeclaredException(symbol);
		}
		return type;
	}
	
	@Override
	public ISymbolData popSymbol(final String symbol) {
		return getSymbolDeque(symbol, (deque, peek) -> deque.pop());
	}
	
	@Override
	public ISymbolData pushSymbol(final String symbol, final IType type) throws MultiplyDeclaredException {
		final SymbolType symbolType = type instanceof IFunctionType ? SymbolType.FUNC : SymbolType.VAR;
		return pushSymbol(symbol, type, symbolType);
	}
	
	@Override
	public ISymbolData pushSymbol(final String symbol, final IType type, final SymbolType stype) throws MultiplyDeclaredException {
		final ISymbolData data = getSymbolDeque(symbol, (deque, peek) -> {
			if (deque == null) {
				deque = new ArrayDeque<ISymbolData>();
				mScope.put(symbol, deque);
			} else {
				if (peek != null && peek.getLevel() >= getLevel()) {
					throw new MultiplyDeclaredException(symbol);
				}
			}
			final ISymbolData data1 = new SymbolData(symbol, mLevel, type, stype);
			deque.push(data1);
			return data1;
		});
		return data;
	}
	
	protected String symbolDatasToString(final String symbol) {
		final StringBuilder sb = new StringBuilder(symbol);
		sb.append(" : ");
		sb.append(getSymbolDeque(symbol, (deque, peek) -> {
			final StringBuilder sb1 = new StringBuilder();
			if (deque == null || deque.isEmpty()) {
				sb1.append("{}");
			} else {
				sb1.append(deque.getFirst());
				deque.forEach(d -> {
					if (d.equals(deque.getFirst())) {
						return;
					} else {
						sb1.append(String.format(" >> %s", d));
					}
				});
			}
			return sb1.toString();
		}));
		return sb.toString();
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		if (mScope.isEmpty()) {
			sb.append("empty");
		} else {
			mScope.keySet().forEach(s -> {
				sb.append(symbolDatasToString(s));
				sb.append("\n");
			});
		}
		return sb.toString();
	}
}
