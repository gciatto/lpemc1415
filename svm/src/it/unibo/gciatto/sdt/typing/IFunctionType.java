package it.unibo.gciatto.sdt.typing;

import java.util.List;

public interface IFunctionType extends IType {
	void addParamType(IType type);
	
	int countParams();
	
	List<IType> getParamsTypes();
	
	IType getParamType(int index);
	
	IType getReturnType();
	
	void insertParamType(int index, IType type);
	
	void setReturnType(IType type);
}
