package it.unibo.gciatto.sdt.typing;

import java.util.Set;

public interface IType extends Iterable<IType> {
	void addSubtype(IType... types);
	
	String getName();
	
	int getSize();
	
	Set<IType> getSubtypes();
	
	IType getSupertype();
	
	boolean isHierarchyRoot();
	
	boolean isSameTypeOf(IType other);
	
	boolean isSubtypeOf(IType other);
	
	boolean isSupertypeOf(IType other);
	
	void setSize(int size);
}
