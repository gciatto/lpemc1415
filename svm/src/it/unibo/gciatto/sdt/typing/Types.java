package it.unibo.gciatto.sdt.typing;

import it.unibo.gciatto.sdt.adt.IClassNode;
import it.unibo.gciatto.sdt.exception.TypeException;
import it.unibo.gciatto.sdt.typing.impl.FunctionType;
import it.unibo.gciatto.sdt.typing.impl.Type;

import java.util.HashMap;
import java.util.Map;

public class Types {
	public static IClassNode findDefinition(final String name) {
		final IType type = new Type(name);
		if (mTypesDefinitions.containsKey(type)) {
			return mTypesDefinitions.get(type);
		} else {
			throw new TypeException();
		}
	}
	
	public static IType findType(final String name) {
		for (final IType t : mTypesDefinitions.keySet()) {
			if (t.getName().equals(name)) {
				return t;
			}
		}
		throw new TypeException();
	}
	
	public static IFunctionType function(final IType returnType, final IType... params) {
		return new FunctionType(returnType, params);
	}
	
	public static IClassNode getDefinition(final IType type) {
		return mTypesDefinitions.get(type);
	}
	
	public static boolean hasDefinition(final IType type) {
		return mTypesDefinitions.containsKey(type);
	}
	
	public static void main(final String[] args) {
		final IType foo = new Type("foo", BOOLEAN);
		
		final IFunctionType f = function(INTEGER, INTEGER, INTEGER, INTEGER);
		
		System.out.println(f);
		
		final IFunctionType f1 = function(foo, foo, BOOLEAN, BOOLEAN);
		
		System.out.println(f1);
		
		System.out.println(f.isSupertypeOf(f1));
		
		System.out.println(f1.isSubtypeOf(f));
	}

	// public static IType OBJECT = new Type("Object", 2);
	
	public static IType putDefition(final IClassNode definition, final IType type) {
		mTypesDefinitions.put(type, definition);
		return type;
	}
	
	// static {
	// //TODO put object definition
	// }
	
	public static IType VOID = new Type("void");
	
	public static IType NULL_TYPE = new Type("$null_type$") {

		@Override
		public boolean isSameTypeOf(final IType other) {
			return true;
		}

		@Override
		public boolean isSubtypeOf(final IType other) {
			return true;
		}

		@Override
		public boolean isSupertypeOf(final IType other) {
			return true;
		}
		
	};
	
	public static IType INTEGER = new Type("int");
	
	public static IType BOOLEAN = new Type("bool", INTEGER);
	
	private static final Map<IType, IClassNode> mTypesDefinitions = new HashMap<IType, IClassNode>();
	
}
