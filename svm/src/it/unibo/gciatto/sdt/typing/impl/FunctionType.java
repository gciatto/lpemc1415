package it.unibo.gciatto.sdt.typing.impl;

import it.unibo.gciatto.sdt.typing.IFunctionType;
import it.unibo.gciatto.sdt.typing.IType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class FunctionType implements IFunctionType {
	private final List<IType> mParamsTypes;
	private IType mReturnType;
	
	public FunctionType() {
		mReturnType = null;
		mParamsTypes = new LinkedList<IType>();
	}

	public FunctionType(final IType returnType, final IType... params) {
		if (Objects.isNull(returnType)) {
			throw new IllegalArgumentException("A function must have a return type");
		}
		mReturnType = returnType;
		if (params.length == 0) {
			mParamsTypes = new ArrayList<IType>();
		} else {
			mParamsTypes = new ArrayList<IType>(Arrays.asList(params));
		}
	}
	
	@Override
	public void addParamType(final IType type) {
		mParamsTypes.add(type);
	}

	@Override
	public void addSubtype(final IType... types) {
		/*
		 * empty
		 */
	}
	
	@Override
	public int countParams() {
		return mParamsTypes.size();
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IFunctionType)) {
			return false;
		}
		return isSameTypeOf((IFunctionType) obj);
	}
	
	@Override
	public String getName() {
		return String.format("(%s) -> %s", paramsToString(), mReturnType.getName());
	}

	@Override
	public List<IType> getParamsTypes() {
		return mParamsTypes;
	}
	
	@Override
	public IType getParamType(final int index) {
		return mParamsTypes.get(index);
	}
	
	@Override
	public IType getReturnType() {
		return mReturnType;
	}

	@Override
	public int getSize() {
		return 2;
	}
	
	@Override
	public Set<IType> getSubtypes() {
		return Collections.emptySet();
	}
	
	@Override
	public IType getSupertype() {
		return null;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (mParamsTypes == null ? 0 : mParamsTypes.hashCode());
		result = prime * result + (mReturnType == null ? 0 : mReturnType.hashCode());
		return result;
	}

	@Override
	public void insertParamType(final int index, final IType type) {
		mParamsTypes.add(index, type);
	}

	@Override
	public boolean isHierarchyRoot() {
		return true;
	}

	@Override
	public boolean isSameTypeOf(final IType other) {
		if (other instanceof FunctionType) {
			final IFunctionType f = (IFunctionType) other;
			if (f.getReturnType().isSameTypeOf(mReturnType) && f.countParams() == countParams()) {
				for (int i = 0; i < f.countParams(); i++) {
					if (!f.getParamType(i).isSameTypeOf(getParamType(i))) {
						return false;
					}
				}
				return true;
			}
			return false;
		} else {
			return false;
		}
	}

	@Override
	public boolean isSubtypeOf(final IType other) {
		if (other instanceof IFunctionType) {
			final IFunctionType otherFun = (IFunctionType) other;
			if (!getReturnType().isSubtypeOf(otherFun.getReturnType())) {
				return false;
			}
			final int count = countParams();
			if (count != otherFun.countParams()) {
				return false;
			}
			for (int i = 0; i < count; i++) {
				if (!getParamType(i).isSubtypeOf(otherFun.getParamType(i))) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isSupertypeOf(final IType other) {
		if (other instanceof IFunctionType) {
			final IFunctionType otherFun = (IFunctionType) other;
			if (!mReturnType.isSupertypeOf(otherFun.getReturnType())) {
				return false;
			}
			final int count = countParams();
			if (count != otherFun.countParams()) {
				return false;
			}
			for (int i = 0; i < count; i++) {
				if (!getParamType(i).isSupertypeOf(otherFun.getParamType(i))) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Iterator<IType> iterator() {
		return new TypeIterator(this);
	}

	protected String paramsToString() {
		if (mParamsTypes.size() == 0) {
			return "";
		} else {
			final StringBuilder sb = new StringBuilder(mParamsTypes.get(0).getName());
			for (int i = 1; i < mParamsTypes.size(); i++) {
				sb.append(", ");
				sb.append(mParamsTypes.get(i));
			}
			return sb.toString();
		}
	}

	@Override
	public void setReturnType(final IType type) {
		mReturnType = type;
	}

	@Override
	public void setSize(final int size) {
		/*
		 * does nothing
		 */
	}

	@Override
	public String toString() {
		return getName();
	}
	
}
