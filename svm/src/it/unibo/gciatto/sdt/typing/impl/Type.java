package it.unibo.gciatto.sdt.typing.impl;

import it.unibo.gciatto.sdt.typing.IType;
import it.unibo.gciatto.sdt.typing.Types;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

public class Type implements IType {
	private final String mName;
	private final IType mSuperType;
	private final HashSet<IType> mSubTypes = new HashSet<IType>();
	private int mSize;
	
	public Type(final String name) {
		this(name, null);
	}
	
	public Type(final String name, final int size) {
		this(name, null, size);
	}
	
	public Type(final String name, final IType superType) {
		this(name, superType, new IType[0]);
	}
	
	public Type(final String name, final IType superType, final int size) {
		this(name, superType, size, new IType[0]);
	}
	
	public Type(final String name, final IType superType, final int size, final IType... subTypes) {
		mName = name;
		mSuperType = superType;
		mSize = size;
		if (Objects.nonNull(superType)) {
			superType.addSubtype(this);
		}
		if (subTypes.length > 0) {
			mSubTypes.addAll(Arrays.asList(subTypes));
		}
	}
	
	public Type(final String name, final IType superType, final IType... subTypes) {
		this(name, superType, 1, subTypes);
	}
	
	@Override
	public void addSubtype(final IType... types) {
		mSubTypes.addAll(Arrays.asList(types));
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IType)) {
			return false;
		}
		return isSameTypeOf((IType) obj);
	}
	
	@Override
	public String getName() {
		return mName;
	}
	
	@Override
	public int getSize() {
		return mSize;
	}
	
	@Override
	public Set<IType> getSubtypes() {
		return mSubTypes;
	}

	@Override
	public IType getSupertype() {
		return mSuperType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (mName == null ? 0 : mName.hashCode());
		return result;
	}

	@Override
	public boolean isHierarchyRoot() {
		return Objects.isNull(mSuperType);
	}

	@Override
	public boolean isSameTypeOf(final IType other) {
		if (other != null && Types.NULL_TYPE.getName().equals(other.getName())) {
			return true;
		}
		if (other instanceof Type) {
			return other.getName().equals(getName());
		} else {
			return false;
		}
	}

	@Override
	public boolean isSubtypeOf(final IType other) {
		if (other != null && Types.NULL_TYPE.getName().equals(other.getName())) {
			return true;
		}
		if (isSameTypeOf(other)) {
			return false;
		}
		for (final IType superType : this) {
			if (superType.isSameTypeOf(other)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isSupertypeOf(final IType other) {
		if (other != null && Types.NULL_TYPE.getName().equals(other.getName())) {
			return true;
		}
		for (final IType superType : other) {
			if (superType.isSameTypeOf(this)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Iterator<IType> iterator() {
		return new TypeIterator(this);
	}

	@Override
	public void setSize(final int size) {
		mSize = size;
	}

	@Override
	public String toString() {
		return getName();
	}
	
}
