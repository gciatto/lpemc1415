package it.unibo.gciatto.sdt.typing.impl;

import it.unibo.gciatto.sdt.typing.IType;

import java.util.Iterator;
import java.util.Objects;

class TypeIterator implements Iterator<IType> {
	private IType mType;
	
	public TypeIterator(final IType start) {
		mType = start;
	}

	@Override
	public boolean hasNext() {
		return Objects.nonNull(mType);
	}

	@Override
	public IType next() {
		final IType output = mType;
		mType = mType.isHierarchyRoot() ? null : mType.getSupertype();
		return output;
	}
	
}