package it.unibo.gciatto.utils;

import java.nio.ByteBuffer;

public class ByteUtils {
	public static long bytesToDoubleWord(final byte... value) {
		if (value.length >= 4) {
			// long res = value[0];
			// for (int i = 1; i < 8; i++) {
			// res |= value[i] << (i * 8);
			// }
			// return res;
			mBuffer.rewind();
			mBuffer.put(value);
			return mBuffer.getLong(0);
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	public static short bytesToHalfWord(final byte... value) {
		if (value.length >= 2) {
			// short res = value[0];
			// res |= value[1] << 8;
			// return res;
			mBuffer.rewind();
			mBuffer.put(value);
			return mBuffer.getShort(0);
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	public static int bytesToWord(final byte... value) {
		if (value.length >= 4) {
			// int res = value[0];
			// res |= value[1] << 8;
			// res |= value[2] << 16;
			// res |= value[3] << 24;
			// return res;
			mBuffer.rewind();
			mBuffer.put(value);
			return mBuffer.getInt(0);
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	public static byte[] doubleWordToBytes(final long value) {
		mBuffer.putLong(0, value);
		return getBytes(Long.BYTES);
		// final byte[] res = new byte[] {
		// (byte) (value & 0x00000000000000ffl),
		// (byte) ((value & 0x000000000000ff00l) >> 8),
		// (byte) ((value & 0x0000000000ff0000l) >> 16),
		// (byte) ((value & 0x00000000ff000000l) >> 24),
		// (byte) ((value & 0x000000ff00000000l) >> 32),
		// (byte) ((value & 0x0000ff0000000000l) >> 40),
		// (byte) ((value & 0x00ff000000000000l) >> 48),
		// (byte) ((value & 0xff00000000000000l) >> 56)
		// };
		// return res;
	}
	
	private static byte[] getBytes(final int size) {
		final byte[] blk = new byte[size];
		// for (int i = 0; i < size; i++) {
		// blk[i] = mBuffer.get(i);
		// }
		mBuffer.rewind();
		mBuffer.get(blk, 0, size);
		return blk;
	}
	
	public static byte[] halfWordToBytes(final short value) {
		mBuffer.putShort(0, value);
		return getBytes(Short.BYTES);
	}
	
	public static byte[] wordToBytes(final int value) {
		mBuffer.putInt(0, value);
		return getBytes(Integer.BYTES);
		// final byte[] res = new byte[] {
		// (byte) (value & 0x000000ff),
		// (byte) ((value & 0x0000ff00) >> 8),
		// (byte) ((value & 0x00ff0000) >> 16),
		// (byte) ((value & 0xff000000) >> 24)
		// };
		// return res;
	}
	
	private static final ByteBuffer mBuffer = ByteBuffer.allocate(16);
}
