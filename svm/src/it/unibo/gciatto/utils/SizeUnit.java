package it.unibo.gciatto.utils;

public enum SizeUnit {
	B(
			1),
	BITS_16(
			2),
	BITS_32(
			4),
	BITS_64(
			8),
	KiB(
			1024),
	KB(
			1000),
	MiB(
			1024 * 1024),
	MB(
			1000 * 1000),
	GiB(
			1024 * 1024 * 1024),
	GB(
			1000 * 1000 * 1000);
	
	private final long mFactor;
	
	private SizeUnit(final long factor) {
		mFactor = factor;
	}
	
	public long getBytes(final int num) {
		return num * mFactor;
	}
	
	public long getFactor() {
		return mFactor;
	}
	
}
