package it.unibo.gciatto.utils.tree;

import java.util.List;

public interface ITreeNode<T> extends Iterable<ITreeNode<T>> {
	
	public abstract void addChild(ITreeNode<T> child);
	
	public abstract int countChildren();
	
	public abstract ITreeNode<T> getChild(int index);
	
	public abstract List<? extends ITreeNode<T>> getChildren();
	
	public abstract ITreeNode<T> getParent();
	
	public abstract T getValue();
	
	public abstract void insertChild(ITreeNode<T> child, int index);
	
	public abstract boolean isChild(ITreeNode<T> child);
	
	public abstract boolean isLeaf();
	
	public abstract boolean isRoot();
	
	public abstract void removeChild(int index);
	
	public abstract void removeChild(ITreeNode<T> child);
	
	public abstract void setParent(ITreeNode<T> parent);
	
	public abstract void setValue(T value);
	
}