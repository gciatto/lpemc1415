package it.unibo.gciatto.utils.tree.impl;

import it.unibo.gciatto.utils.tree.ITreeNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TreeNode<T> implements ITreeNode<T> {
	
	public class ChildToRootIterator implements Iterator<ITreeNode<T>> {
		
		private ITreeNode<T> mNode = TreeNode.this;

		@Override
		public boolean hasNext() {
			return mNode != null;
		}

		@Override
		public ITreeNode<T> next() {
			final ITreeNode<T> temp = mNode;
			
			mNode = mNode.isRoot() ? null : mNode.getParent();
			
			return temp;
		}
		
	}

	public static void main(final String[] args) {
		final ITreeNode<String> i = node("i");
		final ITreeNode<String> t = node("a", node("b", node("e"), node("f")), node("c"), node("d", node("g", node("h", i))));
		System.out.println(t);
		for (final ITreeNode<String> n : i) {
			System.out.println(n);
		}
	}
	
	@SafeVarargs
	public static <E> ITreeNode<E> node(final E value, final ITreeNode<E>... children) {
		return node(null, value, children);
	}
	
	@SafeVarargs
	public static <E> ITreeNode<E> node(final ITreeNode<E> parent, final E value, final ITreeNode<E>... children) {
		final ITreeNode<E> root = new TreeNode<E>(value, children.length, parent);
		for (final ITreeNode<E> n : children) {
			root.addChild(n);
		}
		return root;
	}
	
	private T mValue = null;
	
	private ITreeNode<T> mParent = null;
	
	private ArrayList<ITreeNode<T>> mChildren;
	
	public TreeNode() {
		this(null, null);
	}
	
	public TreeNode(final int childrenCount) {
		this(childrenCount, null);
	}
	
	public TreeNode(final int childrenCount, final ITreeNode<T> parent) {
		this(null, childrenCount, parent);
	}
	
	public TreeNode(final T value) {
		this(value, null);
	}
	
	public TreeNode(final T value, final int childrenCount) {
		this(value, childrenCount, null);
	}
	
	public TreeNode(final T value, final int childrenCount, final ITreeNode<T> parent) {
		mChildren = new ArrayList<ITreeNode<T>>(childrenCount);
		mParent = parent;
		mValue = value;
	}

	public TreeNode(final T value, final ITreeNode<T> parent) {
		this(value, 1, parent);
	}

	@Override
	public void addChild(final ITreeNode<T> child) {
		mChildren.add(child);
		child.setParent(this);
	}

	@Override
	public int countChildren() {
		return getChildren().size();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof TreeNode)) {
			return false;
		}
		final TreeNode other = (TreeNode) obj;
		if (mChildren == null) {
			if (other.mChildren != null) {
				return false;
			}
		} else if (!mChildren.equals(other.mChildren)) {
			return false;
		}
		if (mValue == null) {
			if (other.mValue != null) {
				return false;
			}
		} else if (!mValue.equals(other.mValue)) {
			return false;
		}
		return true;
	}

	@Override
	public ITreeNode<T> getChild(final int index) {
		return getChildren().get(index);
	}

	@Override
	public List<ITreeNode<T>> getChildren() {
		return mChildren;
	}

	@Override
	public ITreeNode<T> getParent() {
		return mParent;
	}

	@Override
	public T getValue() {
		return mValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (mChildren == null ? 0 : mChildren.hashCode());
		result = prime * result + (mValue == null ? 0 : mValue.hashCode());
		return result;
	}

	@Override
	public void insertChild(final ITreeNode<T> child, final int index) {
		mChildren.add(index, child);
		child.setParent(this);
	}

	@Override
	public boolean isChild(final ITreeNode<T> child) {
		return mChildren.contains(child);
	}

	@Override
	public boolean isLeaf() {
		return mChildren.isEmpty();
	}

	@Override
	public boolean isRoot() {
		return mParent == null;
	}

	@Override
	public ChildToRootIterator iterator() {
		return new ChildToRootIterator();
	}

	@Override
	public void removeChild(final int index) {
		mChildren.get(index).setParent(null);
		mChildren.remove(index);
	}

	@Override
	public void removeChild(final ITreeNode<T> child) {
		mChildren.remove(child);
		child.setParent(null);
	}

	@Override
	public void setParent(final ITreeNode<T> parent) {
		mParent = parent;
	}

	@Override
	public void setValue(final T value) {
		mValue = value;
	}

	@Override
	public String toString() {
		if (mChildren.isEmpty()) {
			return String.format("%s", mValue);
		} else {
			return String.format("%s: %s", mValue, mChildren);
		}
	}
}
