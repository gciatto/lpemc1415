package it.unibo.gciatto.vm;

import java.io.File;
import java.io.IOException;

import org.antlr.runtime.RecognitionException;

public interface ICompiler {
	void assignLabel(String label, IInstruction assignTo);
	
	IProgram compile();
	
	IProgram compile(File file) throws IOException, RecognitionException;
	
	IProgram compile(String string) throws IOException, RecognitionException;
	
	IInstruction getLast();
	
	boolean isNextPutListenerSet();
	
	// IInstruction putInstruction(String instruction, String label, Object... params);
	
	// IInstruction putInstruction(IInstruction instruction, Object... params);
	
	IInstruction putInstruction(String instruction, Object... params);
	
	void putLabel(String label, int index);
	
	void setNextPutListener(IInstructionPutListener listener);
}
