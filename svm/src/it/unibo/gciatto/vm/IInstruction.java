package it.unibo.gciatto.vm;

import java.io.Serializable;

public interface IInstruction extends Serializable {
	int getCode();
	
	int getIndex();
	
	String getLabel();
	
	String getLabelParam();
	
	String getName();
	
	byte[] getParams();
	
	int getSize();
	
	boolean hasLabelParam();
	
	boolean isLabelSet();
	
	void onExecute(IVonNeumannCpu cpu) throws Exception;
	
	void onFetch(IVonNeumannCpu cpu) throws Exception;
	
	void setIndex(int index);
	
	void setLabel(String label);
	
	void setLabelParam(String label);

	void setParams(byte[] data);
	
	byte[] toByteArray();
	
	int[] toWordsArray();
}
