package it.unibo.gciatto.vm;

public interface IInstructionPutListener {
	void onNextPut(ICompiler compiler, int actualAddress, IInstruction instruction, Object... args);
	
	void onNextPutting(ICompiler compiler, int futureAddress, IInstruction instruction, Object... args);
}
