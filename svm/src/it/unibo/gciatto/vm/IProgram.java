package it.unibo.gciatto.vm;

import java.io.Serializable;
import java.util.List;

public interface IProgram extends List<IInstruction>, Serializable {
	void execute(IVonNeumannCpu cpu);
	
	int getLength();
	
	void loadOn(IRandomAccessMemory memory, int index);
}
