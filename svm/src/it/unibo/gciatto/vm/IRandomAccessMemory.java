package it.unibo.gciatto.vm;

import java.io.Serializable;

public interface IRandomAccessMemory extends Iterable<Byte>, Serializable {
	int getGranularity();
	
	int getSize();
	
	byte loadByte(int index);
	
	byte[] loadData(int index, int size);
	
	long loadDoubleWord(int index);
	
	short loadHalfWord(int index);
	
	int loadWord(int index);
	
	void storeByte(int index, byte value);
	
	void storeData(int index, byte... bytes);
	
	void storeDoubleWord(int index, long value);
	
	void storeHalfWord(int index, short value);
	
	void storeWord(int index, int value);
}
