package it.unibo.gciatto.vm;

import java.io.Serializable;

public interface IRegister extends Serializable {
	int dec();
	
	int dec(int value);
	
	String getName();
	
	int getValue();
	
	int inc();
	
	int inc(int value);
	
	void setValue(int value);
}
