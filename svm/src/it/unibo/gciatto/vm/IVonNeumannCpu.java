package it.unibo.gciatto.vm;

import java.io.PrintStream;
import java.io.Serializable;

public interface IVonNeumannCpu extends Serializable {
	public static interface IVonNeumannListener {
		boolean isInstructionInteresting(IInstruction instruction);
		
		void onExecuted(IInstruction executed, IVonNeumannCpu cpu, int ip);
		
		void onFetched(IInstruction fetched, IVonNeumannCpu cpu, int ip);
	}
	
	void execute();
	
	void fetch();
	
	IInstruction getCurrentInstruction();
	
	IRegister getInstructionPointer();
	
	IRandomAccessMemory getPrimaryMemory();
	
	PrintStream getPrintStream();
	
	void halt();
	
	void setListener(IVonNeumannListener listener);
	
	void start();
}
