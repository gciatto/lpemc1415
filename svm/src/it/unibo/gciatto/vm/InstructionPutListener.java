package it.unibo.gciatto.vm;

public class InstructionPutListener implements IInstructionPutListener {

	@Override
	public void onNextPut(final ICompiler compiler, final int actualAddress, final IInstruction instruction, final Object... args) {}

	@Override
	public void onNextPutting(final ICompiler compiler, final int futureAddress, final IInstruction instruction, final Object... args) {}
	
}
