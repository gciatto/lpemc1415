package it.unibo.gciatto.vm.impl;

import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public abstract class AbstractInstruction implements IInstruction {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -36114517847480578L;
	
	private int mSize = 1;
	
	private int mIndex = -1;
	
	private String mLabel;
	
	public AbstractInstruction(final byte... data) {
		if (data.length < 1 || data[0] != getCode()) {
			throw new IllegalArgumentException();
		}
	}
	
	public AbstractInstruction(final int code, final byte... data) {
		if (code != getCode()) {
			throw new IllegalArgumentException();
		}
	}
	
	public AbstractInstruction(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		final int code = codeMemory.loadWord(istructionPointer.getValue());
		if (code != getCode()) {
			throw new IllegalArgumentException();
		}
	}
	
	public AbstractInstruction(final IStackBasedCpu cpu) {
		this(cpu.getPrimaryMemory(), cpu.getInstructionPointer());
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AbstractInstruction)) {
			return false;
		}
		final AbstractInstruction other = (AbstractInstruction) obj;
		if (getCode() != other.getCode()) {
			return false;
		}
		return true;
	}
	
	@Override
	public int getIndex() {
		return mIndex;
	}
	
	@Override
	public String getLabel() {
		return mLabel;
	}
	
	@Override
	public byte[] getParams() {
		return null;
	}
	
	@Override
	public int getSize() {
		return mSize;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getCode();
		return result;
	}
	
	@Override
	public boolean isLabelSet() {
		return mLabel != null && !mLabel.isEmpty();
	}
	
	@Override
	public void setIndex(final int index) {
		mIndex = index;
	}
	
	@Override
	public void setLabel(final String label) {
		mLabel = label;
	}
	
	@Override
	public void setParams(final byte[] data) {
		
	}
	
	protected void setSize(final int size) {
		mSize = size;
	}
	
	@Override
	public String toString() {
		if (isLabelSet()) {
			return String.format("%s:\n\t[%s]%s", getLabel(), getIndex(), getName());
		} else {
			return String.format("\t[%s]%s", getIndex(), getName());
		}
	}
	
}
