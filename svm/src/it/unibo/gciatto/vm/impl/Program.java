package it.unibo.gciatto.vm.impl;

import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IProgram;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IVonNeumannCpu;

import java.util.ArrayList;

public class Program extends ArrayList<IInstruction> implements IProgram {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -5228298401244022336L;
	
	public Program(final IInstruction... instructions) {
		super(20);
		for (final IInstruction i : instructions) {
			add(i);
		}
	}
	
	@Override
	public void execute(final IVonNeumannCpu cpu) {
		loadOn(cpu.getPrimaryMemory(), 0);
		cpu.start();
	}
	
	@Override
	public int getLength() {
		int length = 0;
		for (final IInstruction i : this) {
			length += i.getSize();
		}
		return length;
	}
	
	@Override
	public void loadOn(final IRandomAccessMemory memory, final int index) {
		int i = index;
		for (final IInstruction t : this) {
			final byte[] bytes = t.toByteArray();
			memory.storeData(i, bytes);
			i += bytes.length / memory.getGranularity();
		}
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(10 * size());
		
		sb.append("$" + getClass().getSimpleName() + "$");
		sb.append(":\n\n");
		for (final IInstruction i : this) {
			sb.append(i.toString());
			sb.append("\n");
		}
		return sb.toString();
	}
	
}
