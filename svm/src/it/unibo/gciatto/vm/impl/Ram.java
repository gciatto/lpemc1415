package it.unibo.gciatto.vm.impl;

import it.unibo.gciatto.utils.SizeUnit;
import it.unibo.gciatto.vm.IRandomAccessMemory;

import java.nio.ByteBuffer;
import java.util.Iterator;

public class Ram implements IRandomAccessMemory {
	public class MemoryIterator implements Iterator<Byte> {
		private int mIndex = 0;
		
		public MemoryIterator() {
			// TODO Auto-generated constructor stub
		}
		
		public MemoryIterator(final int start) {
			mIndex = start;
		}
		
		@Override
		public boolean hasNext() {
			return mIndex < getSize();
		}
		
		@Override
		public Byte next() {
			return loadByte(mIndex++);
		}
		
	}
	
	/**
	 *
	 */
	private static final long serialVersionUID = -4414010740409187259L;
	
	public static final int GRANULARITY = 1;
	
	private final ByteBuffer mBuffer;
	
	public Ram(final int size) {
		this(size, SizeUnit.B);
	}
	
	public Ram(final int size, final SizeUnit unit) {
		mBuffer = ByteBuffer.allocate((int) unit.getBytes(size));
	}
	
	@Override
	public int getGranularity() {
		return GRANULARITY;
	}
	
	@Override
	public int getSize() {
		return mBuffer.capacity();
	}
	
	@Override
	public MemoryIterator iterator() {
		return new MemoryIterator();
	}
	
	@Override
	public byte loadByte(final int index) {
		return mBuffer.get(index);
	}
	
	@Override
	public byte[] loadData(final int index, final int size) {
		mBuffer.position(index);
		final byte[] data = new byte[size];
		mBuffer.get(data, 0, size);
		return data;
	}
	
	@Override
	public long loadDoubleWord(final int index) {
		return mBuffer.getLong(index);
	}
	
	@Override
	public short loadHalfWord(final int index) {
		return mBuffer.getShort(index);
	}
	
	@Override
	public int loadWord(final int index) {
		return mBuffer.getInt(index);
	}
	
	@Override
	public void storeByte(final int index, final byte value) {
		mBuffer.put(index, value);
	}
	
	@Override
	public void storeData(final int index, final byte... bytes) {
		mBuffer.position(index);
		mBuffer.put(bytes);
	}
	
	@Override
	public void storeDoubleWord(final int index, final long value) {
		mBuffer.putLong(index, value);
	}
	
	@Override
	public void storeHalfWord(final int index, final short value) {
		mBuffer.putShort(index, value);
	}
	
	@Override
	public void storeWord(final int index, final int value) {
		mBuffer.putInt(index, value);
	}
	
	@Override
	public String toString() {
		final int MAX = 10;
		final StringBuilder sb = new StringBuilder("Ram [");
		int i;
		for (i = 0; i < Math.min(MAX, mBuffer.capacity() - 1); i++) {
			sb.append(Integer.toHexString(Byte.toUnsignedInt(mBuffer.get(i))));
			if ((i + 1) % Integer.BYTES == 0) {
				sb.append("| ");
			} else {
				sb.append(", ");
			}
		}
		sb.append(Integer.toHexString(Byte.toUnsignedInt(mBuffer.get(i++))));
		if (i == mBuffer.capacity()) {
			sb.append("]");
		} else {
			sb.append(", ... ]");
		}
		return sb.toString();
	}
	
}
