package it.unibo.gciatto.vm.impl;

import it.unibo.gciatto.utils.ByteUtils;
import it.unibo.gciatto.utils.SizeUnit;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.svm.isa.ISA;

import java.util.Iterator;

public class Ram32Bit implements IRandomAccessMemory {
	public static final int GRANULARITY = 4;
	
	private static final long serialVersionUID = 8951081755574173836L;
	private final int[] mMem;
	
	public Ram32Bit(final int size) {
		this(size, SizeUnit.BITS_32);
	}
	
	public Ram32Bit(final int size, final SizeUnit unit) {
		if (unit == SizeUnit.B && size % SizeUnit.BITS_32.getFactor() != 0) {
			throw new IllegalArgumentException();
		}
		mMem = new int[(int) (unit.getBytes(size) / SizeUnit.BITS_32.getFactor())];
	}
	
	@Override
	public int getGranularity() {
		return GRANULARITY;
	}
	
	@Override
	public int getSize() {
		return mMem.length;
	}
	
	@Override
	public Iterator<Byte> iterator() {
		return null;
	}
	
	public int load(final int index) {
		return mMem[index];
	}
	
	@Override
	public byte loadByte(final int index) {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public byte[] loadData(final int index, final int size) {
		final int[] words = loadWords(index, size);
		final byte[] bytes = new byte[size * ISA.WORD_SIZE];
		for (int i = 0; i < words.length; i++) {
			final byte[] word = ByteUtils.wordToBytes(words[i]);
			for (int j = 0; j < word.length; j++) {
				bytes[4 * i + j] = word[j];
			}
		}
		return bytes;
	}
	
	@Override
	public long loadDoubleWord(final int index) {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public short loadHalfWord(final int index) {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public int loadWord(final int index) {
		return load(index);
	}
	
	public int[] loadWords(final int index, final int size) {
		final int[] data = new int[size];
		int i;
		for (i = 0; i < size; i++) {
			data[i] = load(index + i);
		}
		return data;
	}
	
	public void store(final int index, final int value) {
		mMem[index] = value;
	}
	
	@Override
	public void storeByte(final int index, final byte value) {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public void storeData(final int index, final byte... bytes) {
		if (bytes.length % ISA.WORD_SIZE != 0) {
			throw new IllegalArgumentException();
		}
		for (int i = 0; i < bytes.length; i += ISA.WORD_SIZE) {
			store(index + i / ISA.WORD_SIZE, ByteUtils.bytesToWord(new byte[] {
					bytes[i], bytes[i + 1], bytes[i + 2], bytes[i + 3]
			}));
		}
	}
	
	public void storeData(final int index, final int... data) {
		int i;
		for (i = 0; i < data.length; i++) {
			mMem[i + index] = data[i];
		}
	}
	
	@Override
	public void storeDoubleWord(final int index, final long value) {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public void storeHalfWord(final int index, final short value) {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public void storeWord(final int index, final int value) {
		store(index, value);
	}
	
	@Override
	public String toString() {
		final int MAX = 1024;
		final StringBuilder sb = new StringBuilder("Ram [");
		int i;
		for (i = 0; i < Math.min(MAX, getSize() - 1); i++) {
			sb.append("0x");
			sb.append(Integer.toHexString(load(i)));
			sb.append(", ");
		}
		sb.append("0x");
		sb.append(Integer.toHexString(load(i)));
		if (i == getSize() - 1) {
			sb.append("]");
		} else {
			sb.append(", ... ]");
		}
		return sb.toString();
	}
	
}
