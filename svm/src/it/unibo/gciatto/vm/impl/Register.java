package it.unibo.gciatto.vm.impl;

import it.unibo.gciatto.vm.IRegister;

public class Register implements IRegister {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -6826805199288494468L;
	private int mValue;
	private final String mName;
	
	public Register() {
		this("Register", 0);
	}
	
	public Register(final String name) {
		this(name, 0);
	}
	
	public Register(final String name, final int value) {
		mValue = value;
		mName = name;
	}
	
	@Override
	public int dec() {
		return dec(1);
	}
	
	@Override
	public int dec(final int value) {
		mValue -= value;
		return mValue;
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Register)) {
			return false;
		}
		final Register other = (Register) obj;
		if (mName == null) {
			if (other.mName != null) {
				return false;
			}
		} else if (!mName.equals(other.mName)) {
			return false;
		}
		if (mValue != other.mValue) {
			return false;
		}
		return true;
	}
	
	@Override
	public String getName() {
		return mName;
	}
	
	@Override
	public int getValue() {
		return mValue;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (mName == null ? 0 : mName.hashCode());
		result = prime * result + mValue;
		return result;
	}
	
	@Override
	public int inc() {
		return inc(1);
	}
	
	@Override
	public int inc(final int value) {
		mValue += value;
		return mValue;
	}
	
	@Override
	public void setValue(final int value) {
		mValue = value;
	}
	
	@Override
	public String toString() {
		return String.format("%s = %s", mName, mValue);
	}
	
}
