package it.unibo.gciatto.vm.stack;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;

import java.io.Serializable;

public interface IStack extends Serializable {
	static enum Direction {
		POSITIVE,
		NEGATIVE;
	}
	
	Direction getDirection();
	
	IRandomAccessMemory getMemory();
	
	IRegister getStackBase();
	
	int getStackBaseValue();
	
	IRegister getStackPointer();
	
	int getStackPointerValue();
	
	boolean isEmpty();
	
	byte peekByte() throws IndexOutOfBoundsException;
	
	long peekDoubleWord() throws IndexOutOfBoundsException;
	
	short peekHalfWord() throws IndexOutOfBoundsException;
	
	int peekWord() throws IndexOutOfBoundsException;
	
	byte popByte() throws IndexOutOfBoundsException;
	
	long popDoubleWord() throws IndexOutOfBoundsException;
	
	short popHalfWord() throws IndexOutOfBoundsException;
	
	int popWord() throws IndexOutOfBoundsException;
	
	void push(byte value);
	
	void push(int value);
	
	void push(long value);
	
	void push(short value);
	
}
