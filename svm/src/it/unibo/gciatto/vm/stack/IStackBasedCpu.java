package it.unibo.gciatto.vm.stack;

import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.IVonNeumannCpu;

import java.io.Serializable;

public interface IStackBasedCpu extends Serializable, IVonNeumannCpu {
	public static interface IStackBasedCpuListener extends IVonNeumannListener {
		
		void onExecuted(IInstruction executed, IStackBasedCpu cpu, int ip);
		
		void onFetched(IInstruction fetched, IStackBasedCpu cpu, int ip);
	}
	
	IRegister getFramePointer();
	
	IRegister getHeapPointer();
	
	IRegister getReturnAddress();
	
	IRegister getReturnValue();
	
	IStack getStack();
	
	IRegister getStackBase();
	
	IRegister getStackPointer();
	
	void setListener(IStackBasedCpuListener listener);
}
