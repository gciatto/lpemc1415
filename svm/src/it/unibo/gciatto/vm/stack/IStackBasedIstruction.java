package it.unibo.gciatto.vm.stack;

import it.unibo.gciatto.vm.IInstruction;

import java.io.Serializable;

public interface IStackBasedIstruction extends Serializable, IInstruction {
	void onExecute(IStackBasedCpu cpu) throws Exception;
	
	void onFetch(IStackBasedCpu cpu) throws Exception;
}
