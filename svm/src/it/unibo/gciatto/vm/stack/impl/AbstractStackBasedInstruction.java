package it.unibo.gciatto.vm.stack.impl;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.IVonNeumannCpu;
import it.unibo.gciatto.vm.impl.AbstractInstruction;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;
import it.unibo.gciatto.vm.stack.IStackBasedIstruction;

public abstract class AbstractStackBasedInstruction extends AbstractInstruction implements IStackBasedIstruction {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 3232193906897057633L;
	
	public AbstractStackBasedInstruction(final byte... data) {
		super(data);
	}
	
	public AbstractStackBasedInstruction(final int code, final byte... data) {
		super(code, data);
	}
	
	public AbstractStackBasedInstruction(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
	}
	
	public AbstractStackBasedInstruction(final IStackBasedCpu cpu) {
		super(cpu);
	}
	
	@Override
	public void onExecute(final IVonNeumannCpu cpu) throws Exception {
		if (cpu instanceof IStackBasedCpu) {
			onExecute((IStackBasedCpu) cpu);
		}
	}
	
	@Override
	public void onFetch(final IVonNeumannCpu cpu) throws Exception {
		if (cpu instanceof IStackBasedCpu) {
			onFetch((IStackBasedCpu) cpu);
		}
	}
	
}
