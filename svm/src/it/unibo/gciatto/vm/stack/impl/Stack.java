package it.unibo.gciatto.vm.stack.impl;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.impl.Register;
import it.unibo.gciatto.vm.stack.IStack;

public class Stack implements IStack {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 8706965421000393742L;
	private final Direction mDirection;
	private final IRegister mSB;
	private final IRegister mSP;
	private final IRandomAccessMemory mMemory;
	private final java.util.Stack<Number> mInternal = new java.util.Stack<Number>();
	
	public Stack(final IRandomAccessMemory memory) {
		this(memory, Direction.NEGATIVE);
	}
	
	public Stack(final IRandomAccessMemory memory, final Direction direction) {
		this(memory, direction, direction == Direction.NEGATIVE ? memory.getSize() - 1 : 0);
	}
	
	public Stack(final IRandomAccessMemory memory, final Direction direction, final int baseAddr) {
		this(memory, direction, new Register("StackBase", baseAddr), new Register("StackPointer", baseAddr));
	}
	
	public Stack(final IRandomAccessMemory memory, final Direction direction, final IRegister sb, final IRegister sp) {
		mDirection = direction;
		mSB = sb;
		mSP = sp;
		mMemory = memory;
	}
	
	protected void decSP(final int size) {
		if (getDirection() == Direction.POSITIVE) {
			getStackPointer().dec(size);
		} else {
			getStackPointer().inc(size);
		}
	}
	
	@Override
	public Direction getDirection() {
		return mDirection;
	}
	
	@Override
	public IRandomAccessMemory getMemory() {
		return mMemory;
	}
	
	@Override
	public IRegister getStackBase() {
		return mSB;
	}
	
	@Override
	public int getStackBaseValue() {
		return mSB.getValue();
	}
	
	@Override
	public IRegister getStackPointer() {
		return mSP;
	}
	
	@Override
	public int getStackPointerValue() {
		return mSP.getValue();
	}
	
	protected void incSP(final int size) {
		if (getDirection() == Direction.POSITIVE) {
			getStackPointer().inc(size);
		} else {
			getStackPointer().dec(size);
		}
	}
	
	@Override
	public boolean isEmpty() {
		return getStackBaseValue() == getStackPointerValue();
	}
	
	protected int nextSP(final int size) {
		if (getDirection() == Direction.POSITIVE) {
			return getStackPointerValue() + size;
		} else {
			return getStackPointerValue() - size;
		}
	}
	
	@Override
	public byte peekByte() throws IndexOutOfBoundsException {
		final byte value = popByte();
		push(value);
		return value;
	}
	
	@Override
	public long peekDoubleWord() throws IndexOutOfBoundsException {
		final long value = popDoubleWord();
		push(value);
		return value;
	}
	
	@Override
	public short peekHalfWord() throws IndexOutOfBoundsException {
		final short value = popHalfWord();
		push(value);
		return value;
	}
	
	@Override
	public int peekWord() throws IndexOutOfBoundsException {
		final int value = popWord();
		push(value);
		return value;
	}
	
	@Override
	public byte popByte() throws IndexOutOfBoundsException {
		final byte value;
		if (getDirection() == Direction.POSITIVE) {
			decSP(Byte.BYTES);
			value = getMemory().loadByte(getStackPointerValue());
		} else {
			value = getMemory().loadByte(getStackPointerValue() + 1);
			decSP(Byte.BYTES);
		}
		popInternal();
		return value;
	}
	
	@Override
	public long popDoubleWord() throws IndexOutOfBoundsException {
		final long value;
		if (getDirection() == Direction.POSITIVE) {
			decSP(Long.BYTES);
			value = getMemory().loadDoubleWord(getStackPointerValue());
		} else {
			value = getMemory().loadDoubleWord(getStackPointerValue() + 1);
			decSP(Long.BYTES);
		}
		popInternal();
		return value;
	}
	
	@Override
	public short popHalfWord() throws IndexOutOfBoundsException {
		final short value;
		if (getDirection() == Direction.POSITIVE) {
			decSP(Short.BYTES);
			value = getMemory().loadHalfWord(getStackPointerValue());
		} else {
			value = getMemory().loadHalfWord(getStackPointerValue() + 1);
			decSP(Short.BYTES);
		}
		popInternal();
		return value;
	}
	
	private Number popInternal() {
		return mInternal.pop();
	}
	
	@Override
	public int popWord() throws IndexOutOfBoundsException {
		// decSP(Integer.BYTES);
		// return getMemory().loadWord(getStackPointerValue());
		final int value;
		if (getDirection() == Direction.POSITIVE) {
			decSP(Integer.BYTES);
			value = getMemory().loadWord(getStackPointerValue());
		} else {
			value = getMemory().loadWord(getStackPointerValue() + 1);
			decSP(Integer.BYTES);
		}
		popInternal();
		return value;
	}
	
	protected int prevSP(final int size) {
		if (getDirection() == Direction.POSITIVE) {
			return getStackPointerValue() - size;
		} else {
			return getStackPointerValue() + size;
		}
	}
	
	@Override
	public void push(final byte value) {
		if (getDirection() == Direction.POSITIVE) {
			getMemory().storeByte(getStackPointerValue(), value);
			incSP(Byte.BYTES);
		} else {
			incSP(Byte.BYTES);
			getMemory().storeByte(getStackPointerValue() + 1, value);
		}
		pushInternal(value);
	}
	
	@Override
	public void push(final int value) {
		if (getDirection() == Direction.POSITIVE) {
			getMemory().storeWord(getStackPointerValue(), value);
			incSP(Integer.BYTES);
		} else {
			incSP(Integer.BYTES);
			getMemory().storeWord(getStackPointerValue() + 1, value);
		}
		pushInternal(value);
	}
	
	@Override
	public void push(final long value) {
		if (getDirection() == Direction.POSITIVE) {
			getMemory().storeDoubleWord(getStackPointerValue(), value);
			incSP(Long.BYTES);
		} else {
			incSP(Long.BYTES);
			getMemory().storeDoubleWord(getStackPointerValue() + 1, value);
		}
		pushInternal(value);
	}
	
	@Override
	public void push(final short value) {
		if (getDirection() == Direction.POSITIVE) {
			getMemory().storeHalfWord(getStackPointerValue(), value);
			incSP(Short.BYTES);
		} else {
			incSP(Short.BYTES);
			getMemory().storeHalfWord(getStackPointerValue() + 1, value);
		}
		pushInternal(value);
	}
	
	private void pushInternal(final Number value) {
		mInternal.push(value);
	}
	
	@Override
	public String toString() {
		return String.format("Stack {%s, %s, Memory %s}", mSB, mSP, mInternal);
	}
	
}
