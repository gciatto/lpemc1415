package it.unibo.gciatto.vm.stack.impl;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.impl.Register;
import it.unibo.gciatto.vm.stack.IStack;

public class Stack32Bit implements IStack {
	public static interface IHighlighter {
		String stackElementToString(Number value, int stackIndex, int memIndex);
	}
	
	/**
	 *
	 */
	private static final long serialVersionUID = 8706965421000393742L;
	private final Direction mDirection;
	private final IRegister mSB;
	private final IRegister mSP;
	private final IRandomAccessMemory mMemory;
	private final java.util.Stack<Number> mInternal = new java.util.Stack<Number>();
	private IHighlighter mHighlighter = null;
	
	public Stack32Bit(final IRandomAccessMemory memory) {
		this(memory, Direction.NEGATIVE);
	}
	
	public Stack32Bit(final IRandomAccessMemory memory, final Direction direction) {
		this(memory, direction, direction == Direction.NEGATIVE ? memory.getSize() : -1);
	}
	
	public Stack32Bit(final IRandomAccessMemory memory, final Direction direction, final int baseAddr) {
		this(memory, direction, new Register("StackBase", baseAddr), new Register("StackPointer", baseAddr));
	}

	public Stack32Bit(final IRandomAccessMemory memory, final Direction direction, final IRegister sb, final IRegister sp) {
		mDirection = direction;
		mSB = sb;
		mSP = sp;
		mMemory = memory;
	}

	protected void decSP() {
		if (getDirection() == Direction.POSITIVE) {
			getStackPointer().dec();
		} else {
			getStackPointer().inc();
		}
	}
	
	@Override
	public Direction getDirection() {
		return mDirection;
	}
	
	public IHighlighter getHighlighter() {
		return mHighlighter;
	}
	
	@Override
	public IRandomAccessMemory getMemory() {
		return mMemory;
	}
	
	@Override
	public IRegister getStackBase() {
		return mSB;
	}
	
	@Override
	public int getStackBaseValue() {
		return mSB.getValue();
	}
	
	@Override
	public IRegister getStackPointer() {
		return mSP;
	}
	
	@Override
	public int getStackPointerValue() {
		return mSP.getValue();
	}
	
	protected void incSP() {
		if (getDirection() == Direction.POSITIVE) {
			getStackPointer().inc();
		} else {
			getStackPointer().dec();
		}
	}
	
	@Override
	public boolean isEmpty() {
		return getStackBaseValue() == getStackPointerValue();
	}
	
	public int peek() {
		return mInternal.peek().intValue();
	}
	
	@Override
	public byte peekByte() throws IndexOutOfBoundsException {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public long peekDoubleWord() throws IndexOutOfBoundsException {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public short peekHalfWord() throws IndexOutOfBoundsException {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public int peekWord() throws IndexOutOfBoundsException {
		return peek();
	}
	
	public int pop() {
		final int value;
		value = getMemory().loadWord(getStackPointerValue());
		decSP();
		popInternal();
		return value;
	}
	
	@Override
	public byte popByte() throws IndexOutOfBoundsException {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public long popDoubleWord() throws IndexOutOfBoundsException {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public short popHalfWord() throws IndexOutOfBoundsException {
		throw new RuntimeException("not implemented");
	}
	
	private Number popInternal() {
		return mInternal.pop();
	}
	
	@Override
	public int popWord() throws IndexOutOfBoundsException {
		return pop();
	}
	
	@Override
	public void push(final byte value) {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public void push(final int value) {
		incSP();
		getMemory().storeWord(getStackPointerValue(), value);
		pushInternal(value);
	}
	
	@Override
	public void push(final long value) {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public void push(final short value) {
		throw new RuntimeException("not implemented");
	}
	
	private void pushInternal(final Number value) {
		mInternal.push(value);
	}
	
	public void setHighlighter(final IHighlighter highlighter) {
		mHighlighter = highlighter;
	}
	
	protected String stackElementToString(final Number value, final int stackIndex, final int memIndex) {
		if (mHighlighter == null) {
			return value.toString();
		} else {
			return mHighlighter.stackElementToString(value, stackIndex, memIndex);
		}
	}
	
	protected String stackToString() {
		final StringBuilder sb = new StringBuilder("[");
		final int size = mInternal.size();
		for (int i = 0; i < size; i++) {
			final int j = mSB.getValue() + (1 + i) * (mDirection == Direction.POSITIVE ? 1 : -1);
			sb.append(stackElementToString(mInternal.get(i), i, j));
			if (i < size - 1) {
				sb.append(", ");
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
	@Override
	public String toString() {
		return String.format("Stack {%s, %s, Memory %s}", mSB, mSP, stackToString());
	}
	
}
