package it.unibo.gciatto.vm.svm;

import it.unibo.gciatto.utils.SizeUnit;
import it.unibo.gciatto.vm.ICompiler;
import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IProgram;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.InstructionPutListener;
import it.unibo.gciatto.vm.impl.Ram32Bit;
import it.unibo.gciatto.vm.stack.IStack;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;
import it.unibo.gciatto.vm.stack.IStackBasedCpu.IStackBasedCpuListener;
import it.unibo.gciatto.vm.stack.impl.Stack32Bit;
import it.unibo.gciatto.vm.svm.SVMCpu.CpuListener;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.antlr.runtime.RecognitionException;

public class ExecuteVM {
	
	private static IProgram getFuffaExample() {
		final ICompiler compiler = new SVMCompiler();
		compiler.putInstruction("push", 2);
		compiler.putInstruction("push", 1);
		compiler.putInstruction("b", "code");
		compiler.putInstruction("halt");
		compiler.putInstruction("halt");
		compiler.putInstruction("halt");
		compiler.putInstruction("halt");
		compiler.setNextPutListener(new InstructionPutListener() {

			@Override
			public void onNextPutting(final ICompiler compiler, final int futureAddress, final IInstruction instruction, final Object... args) {
				instruction.setLabel("code");
			}
			
		});
		compiler.putInstruction("print");
		compiler.putInstruction("pop");
		compiler.putInstruction("print");
		compiler.putInstruction("halt");
		return compiler.compile();
	}

	public static void main(final String[] args) throws IOException, RecognitionException {
		Logger.getGlobal().setLevel(Level.OFF);
		if (args.length == 0) {
			System.out.println(mSyntaxInfo);
		} else {
			final boolean verbose;
			final File file;
			if (args[0].equalsIgnoreCase("-v")) {
				verbose = args[0].equalsIgnoreCase("-v");
				if (args.length == 1) {
					System.out.println(mSyntaxInfo);
					return;
				}
				file = new File(args[1]);
			} else {
				verbose = false;
				file = new File(args[0]);
			}
			if (file.exists()) {
				final ICompiler compiler = new SVMCompiler();
				// final IProgram prog = compiler.compile(file);
				final IProgram prog = getFuffaExample();
				final IRandomAccessMemory mem = new Ram32Bit(mMemKibs, SizeUnit.KiB);
				final IStack stack = new Stack32Bit(mem);
				final IStackBasedCpu cpu = new SVMCpu(mem, stack);
				if (verbose) {
					cpu.setListener(mVerboseListener);
				}
				prog.execute(cpu);
			} else {
				System.err.println("No such file.");
			}
		}
	}

	private static final IStackBasedCpuListener mVerboseListener = new CpuListener() {
		int step = 0;
		
		@Override
		public void onExecuted(final IInstruction executed, final IStackBasedCpu cpu, final int ip) {
			System.out.printf("step %s:\tistr=%s ip=%s sp=%s peek=%s hp=%s fp=%s ra=%s rv=%s\n", step++, executed.getName(), cpu.getInstructionPointer().getValue(), cpu.getStackPointer().getValue(), cpu.getStack().isEmpty() ? "*empty*" : cpu.getStack().peekWord(), cpu.getHeapPointer().getValue(), cpu.getFramePointer().getValue(), cpu.getReturnAddress().getValue(), cpu.getReturnValue().getValue());
		}
		
	};

	private static final String mSyntaxInfo = "Syntax: <program_name> [-v] <file_to_compile>\n\t-v\tVerbose mode: shows registers and memory status step by step.";

	private static final int mMemKibs = 10;
}
