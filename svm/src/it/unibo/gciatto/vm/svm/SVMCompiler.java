package it.unibo.gciatto.vm.svm;

import it.unibo.gciatto.utils.ByteUtils;
import it.unibo.gciatto.vm.ICompiler;
import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IInstructionPutListener;
import it.unibo.gciatto.vm.IProgram;
import it.unibo.gciatto.vm.impl.Program;
import it.unibo.gciatto.vm.svm.isa.ISA;
import it.unibo.gciatto.vm.svm.parsing.SVMLexer;
import it.unibo.gciatto.vm.svm.parsing.SVMParser;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

public class SVMCompiler implements ICompiler {
	
	private static class LabelReminder {
		private int mIndex;
		private final List<IInstruction> mRemindTo = new LinkedList<IInstruction>();
		private boolean mSet = false;
		private final String mLabel;
		
		public LabelReminder(final String label) {
			mLabel = label;
		}
		
		public int getIndex() {
			return mIndex;
		}
		
		public String getLabel() {
			return mLabel;
		}
		
		public void remindMe(final IInstruction instruction) {
			if (mSet) {
				instruction.setParams(ByteUtils.wordToBytes(getIndex()));
			}
			mRemindTo.add(instruction);
		}
		
		private void remindToAll() {
			for (final IInstruction i : mRemindTo) {
				i.setParams(ByteUtils.wordToBytes(getIndex()));
			}
		}
		
		public void setIndex(final int index) {
			mIndex = index;
			mSet = true;
			remindToAll();
		}
		
	}
	
	private final Map<String, LabelReminder> mLabels = new HashMap<String, SVMCompiler.LabelReminder>();
	private final IProgram mProgram = new Program();
	private int mSize = 0;
	private IInstructionPutListener mNextPutListener = null;
	
	private IInstruction mLast;
	
	@Override
	public void assignLabel(final String label, final IInstruction assignTo) {
		putLabel(label, assignTo.getIndex());
		assignTo.setLabel(label);
	}
	
	@Override
	public IProgram compile() {
		return mProgram;
	}
	
	protected IProgram compile(final ANTLRStringStream stream) throws IOException, RecognitionException {
		final SVMLexer lexer = new SVMLexer(stream);
		final CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		final SVMParser parser = new SVMParser(tokenStream);
		parser.setCompiler(this);
		parser.assembly();
		return compile();
	}
	
	@Override
	public IProgram compile(final File file) throws IOException, RecognitionException {
		return compile(new ANTLRFileStream(file.getAbsolutePath()));
	}
	
	@Override
	public IProgram compile(final String string) throws IOException, RecognitionException {
		return compile(new ANTLRStringStream(string));
	}
	
	@Override
	public IInstruction getLast() {
		return mLast;
	}
	
	@Override
	public boolean isNextPutListenerSet() {
		return mNextPutListener != null;
	}
	
	private IInstruction putInstruction(final IInstruction instr, final Object... params) {
		if (isNextPutListenerSet()) {
			mNextPutListener.onNextPutting(this, mSize, instr, params);
		}
		mProgram.add(instr);
		instr.setIndex(mSize);
		mSize += instr.getSize();
		if (params.length >= 1) {
			if (params[0] instanceof Integer) {
				final Integer num = (Integer) params[0];
				instr.setParams(ByteUtils.wordToBytes(num.intValue()));
			} else if (params[0] instanceof String) {
				final String label = params[0].toString();
				instr.setLabelParam(label);
				final LabelReminder reminder;
				if (!mLabels.containsKey(label)) {
					reminder = new LabelReminder(label);
					mLabels.put(label, reminder);
				} else {
					reminder = mLabels.get(label);
				}
				reminder.remindMe(instr);
			}
		}
		mLast = instr;
		if (instr.isLabelSet()) {
			assignLabel(instr.getLabel(), instr);
		}
		if (isNextPutListenerSet()) {
			mNextPutListener.onNextPut(this, mSize, instr, params);
			setNextPutListener(null);
		}
		return instr;
	}

	@Override
	public IInstruction putInstruction(final String instruction, final Object... params) {
		final IInstruction instr = ISA.instance(instruction);
		return putInstruction(instr, params);
	}

	@Override
	public void putLabel(final String label, final int index) {
		final LabelReminder reminder;
		if (!mLabels.containsKey(label)) {
			reminder = new LabelReminder(label);
			mLabels.put(label, reminder);
		} else {
			reminder = mLabels.get(label);
		}
		reminder.setIndex(index);
	}

	@Override
	public void setNextPutListener(final IInstructionPutListener listener) {
		mNextPutListener = listener;
	}

	// @Override
	// public IInstruction putInstruction(String instruction, String label, Object... params) {
	// final IInstruction instr = ISA.instance(instruction);
	// instr.setLabel(label);
	// return putInstruction(instr, params);
	// }
}
