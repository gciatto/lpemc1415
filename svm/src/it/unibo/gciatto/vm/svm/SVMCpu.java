package it.unibo.gciatto.vm.svm;

import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.IVonNeumannCpu;
import it.unibo.gciatto.vm.impl.Register;
import it.unibo.gciatto.vm.stack.IStack;
import it.unibo.gciatto.vm.stack.IStack.Direction;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;
import it.unibo.gciatto.vm.stack.impl.Stack32Bit;
import it.unibo.gciatto.vm.stack.impl.Stack32Bit.IHighlighter;
import it.unibo.gciatto.vm.svm.isa.ISA;

import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SVMCpu implements IStackBasedCpu {
	
	public static class CpuListener implements IStackBasedCpuListener {
		
		@Override
		public boolean isInstructionInteresting(final IInstruction instruction) {
			return true;
		}
		
		@Override
		public void onExecuted(final IInstruction executed, final IStackBasedCpu cpu, final int ip) {
			
		}
		
		@Override
		public void onExecuted(final IInstruction executed, final IVonNeumannCpu cpu, final int ip) {
			if (cpu instanceof IStackBasedCpu) {
				onExecuted(executed, (IStackBasedCpu) cpu, ip);
			}
		}
		
		@Override
		public void onFetched(final IInstruction fetched, final IStackBasedCpu cpu, final int ip) {
			
		}
		
		@Override
		public void onFetched(final IInstruction fetched, final IVonNeumannCpu cpu, final int ip) {
			onExecuted(fetched, (IStackBasedCpu) cpu, ip);
		}
		
	}
	
	/**
	 *
	 */
	private static final long serialVersionUID = 6846032806602433474L;
	
	private static final Logger L = Logger.getGlobal();
	
	private final IRandomAccessMemory mMemory;
	private final IStack mStack;
	private final IRegister mIP, mHP, mFP, mRA, mRV;
	private IInstruction mCurrentInstruction;
	private boolean mRunning = false;
	private long mStep;
	private IVonNeumannListener mListener = null;
	private final PrintStream mPrintStream;
	
	public SVMCpu(final IRandomAccessMemory memory, final IStack stack) {
		this(memory, stack, System.out);
	}
	
	public SVMCpu(final IRandomAccessMemory memory, final IStack stack, final PrintStream printStream) {
		mMemory = memory;
		mStack = stack;
		mIP = new Register("Instruction Pointer");
		mHP = new Register("Heap Pointer");
		mFP = new Register("Frame Pointer");
		mRA = new Register("Return Address");
		mRV = new Register("Return Value");
		
		mPrintStream = printStream;

		if (stack instanceof Stack32Bit) {
			((Stack32Bit) stack).setHighlighter(new IHighlighter() {
				
				@Override
				public String stackElementToString(final Number value, final int stackIndex, final int memIndex) {
					if (memIndex == getFramePointer().getValue()) {
						return String.format("| FP -> *%s* |", value);
					} else {
						if (stackIndex % 10 == 0) {
							return String.format("%s = %s", memIndex, value);
						} else {
							return String.format("%s", value);
						}
					}
				}
			});
		}
		
		cleanUp();
	}

	protected void check() {
		if (getStack().getDirection() == Direction.POSITIVE) {
			if (getStackPointer().getValue() < getStackBase().getValue()) {
				throw new IllegalStateException("stack smash");
			}
			if (getStackPointer().getValue() > getHeapPointer().getValue()) {
				throw new IllegalStateException("stack overflow");
			}
		}
		if (getStack().getDirection() == Direction.NEGATIVE) {
			if (getStackPointer().getValue() > getStackBase().getValue()) {
				throw new IllegalStateException("stack smash");
			}
			if (getStackPointer().getValue() < getHeapPointer().getValue()) {
				throw new IllegalStateException("stack overflow");
			}
		}
	}
	
	protected void cleanUp() {
		getInstructionPointer().setValue(0);
		getStackPointer().setValue(getStack().getStackBaseValue());
		getFramePointer().setValue(getStackBase().getValue());
		getHeapPointer().setValue(getStack().getDirection() == Direction.POSITIVE ? getPrimaryMemory().getSize() : 0);
		mRunning = true;
		mStep = 0;
	}
	
	@Override
	public void execute() {
		try {
			getCurrentInstruction().onExecute(this);
			check();
			notifyExecuted();
		} catch (final Exception ex) {
			ex.printStackTrace();
			L.log(Level.WARNING, ex.getMessage(), ex);
			halt();
		}
	}
	
	@Override
	public void fetch() {
		setCurrentInstruction(ISA.decode(getPrimaryMemory(), getInstructionPointer()));
		try {
			getCurrentInstruction().onFetch(this);
			notifyFetched();
		} catch (final Exception ex) {
			ex.printStackTrace();
			halt();
		} finally {
			getInstructionPointer().inc(getCurrentInstruction().getSize());
		}
	}
	
	@Override
	public IInstruction getCurrentInstruction() {
		return mCurrentInstruction;
	}
	
	@Override
	public IRegister getFramePointer() {
		return mFP;
	}
	
	@Override
	public IRegister getHeapPointer() {
		return mHP;
	}
	
	@Override
	public IRegister getInstructionPointer() {
		return mIP;
	}
	
	@Override
	public IRandomAccessMemory getPrimaryMemory() {
		return mMemory;
	}
	
	@Override
	public PrintStream getPrintStream() {
		return mPrintStream;
	}
	
	@Override
	public IRegister getReturnAddress() {
		return mRA;
	}
	
	@Override
	public IRegister getReturnValue() {
		return mRV;
	}
	
	@Override
	public IStack getStack() {
		return mStack;
	}
	
	@Override
	public IRegister getStackBase() {
		return mStack.getStackBase();
	}
	
	@Override
	public IRegister getStackPointer() {
		return mStack.getStackPointer();
	}
	
	@Override
	public void halt() {
		mRunning = false;
	}
	
	protected String heapToString() {
		final StringBuilder sb = new StringBuilder("[");
		int i = mHP.getValue();
		if (i == getPrimaryMemory().getSize()) {
			sb.append("]");
			return sb.toString();
		}
		sb.append(getPrimaryMemory().loadWord(i));
		for (i++; i < getPrimaryMemory().getSize(); i++) {
			sb.append(", ");
			sb.append(getPrimaryMemory().loadWord(i));
		}
		sb.append("]");
		return sb.toString();
	}
	
	protected void notifyExecuted() {
		if (mListener != null && mListener.isInstructionInteresting(getCurrentInstruction())) {
			mListener.onExecuted(getCurrentInstruction(), this, mIP.getValue());
		}
	}
	
	protected void notifyFetched() {
		if (mListener != null && mListener.isInstructionInteresting(getCurrentInstruction())) {
			mListener.onFetched(getCurrentInstruction(), this, mIP.getValue());
		}
	}
	
	protected void setCurrentInstruction(final IInstruction current) {
		mCurrentInstruction = current;
	}
	
	@Override
	public void setListener(final IStackBasedCpuListener listener) {
		mListener = listener;
	}
	
	@Override
	@Deprecated
	public void setListener(final IVonNeumannListener listener) {
		if (listener instanceof IStackBasedCpuListener) {
			setListener((IStackBasedCpuListener) listener);
		}
	}
	
	@Override
	public void start() {
		int addr;
		
		cleanUp();
		
		L.info(String.format("***STARTING***\n%s", this));
		
		while (mRunning) {
			addr = getInstructionPointer().getValue();
			
			fetch();
			
			execute();
			
			mStep++;
			
			L.info(String.format("***STEP %s***\n%s", mStep, this));
		}
		
		L.info(String.format("***HALTED***\n%s", this));
		
	}

	@Override
	public String toString() {
		return String.format("%s [\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s\n\tHeap: %s\n\tCurrentInstruction=%s\n]", super.toString(), mStack, mIP, mHP, mFP, mRA, mRV, heapToString(), mCurrentInstruction);
		// return String.format("%s [\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s\n\tCurrentInstruction=%s\n]", super.toString(), mStack, mIP, mHP, mFP, mRA, mRV, mCurrentInstruction);
	}
}
