package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

abstract class BinaryWordOperator extends SVMInstruction implements IInstruction {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -5119545865165539544L;
	
	public BinaryWordOperator(final byte... data) {
		super(data);
	}
	
	public BinaryWordOperator(final int code, final byte... data) {
		super(code, data);
	}
	
	public BinaryWordOperator(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
	}
	
	public BinaryWordOperator(final IStackBasedCpu cpu) {
		super(cpu);
	}
	
	protected abstract int calculate(int first, int second);
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		final int fstWord, sndWord, result;
		sndWord = cpu.getStack().popWord();
		exLog("Popped %s", sndWord);
		fstWord = cpu.getStack().popWord();
		exLog("Popped %s", fstWord);
		result = calculate(fstWord, sndWord);
		cpu.getStack().push(result);
		exLog("Pushed %s", result);
	}
	
}
