package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.utils.ByteUtils;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class Branch extends Command {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -8056765439173719569L;
	private int mAddress;
	
	private String mLabelParam;
	
	public Branch(final byte... data) {
		super(data);
		setSize(2);
		if (data.length >= 5) {
			mAddress = ByteUtils.bytesToWord(data[1], data[2], data[3], data[4]);
		}
	}
	
	public Branch(final int code, final byte... data) {
		super(code, data);
		setSize(2);
	}
	
	public Branch(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		setSize(2);
	}
	
	public Branch(final IStackBasedCpu cpu) {
		super(cpu);
		setSize(2);
	}
	
	protected boolean checkCondition(final IStackBasedCpu cpu) {
		return true;
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.BRANCH.getCode();
	}
	
	@Override
	public String getLabelParam() {
		return mLabelParam;
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.BRANCH.getName();
	}
	
	@Override
	public byte[] getParams() {
		return ByteUtils.wordToBytes(mAddress);
	}
	
	@Override
	public boolean hasLabelParam() {
		return mLabelParam != null;
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		mAddress = getParamWord(cpu);
		if (checkCondition(cpu)) {
			cpu.getInstructionPointer().setValue(mAddress);
			exLog("Jump to %s", mAddress);
		}
	}
	
	@Override
	public void setLabelParam(final String label) {
		mLabelParam = label;
	}
	
	@Override
	public void setParams(final byte[] data) {
		mAddress = ByteUtils.bytesToWord(data);
	}

	@Override
	public byte[] toByteArray() {
		final byte[] address = ByteUtils.wordToBytes(mAddress);
		return new byte[] {
				(byte) 0, (byte) 0, (byte) 0, getCodeAsByte(), address[0], address[1], address[2], address[3],
		};
	}
	
	@Override
	public String toString() {
		return String.format("%s %s", super.toString(), hasLabelParam() ? mLabelParam : mAddress);
	}
	
	@Override
	public int[] toWordsArray() {
		return new int[] {
				getCode(), mAddress
		};
	}
	
}
