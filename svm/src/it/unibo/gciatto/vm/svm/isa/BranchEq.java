package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class BranchEq extends Branch {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -3424366131039290529L;
	private int mValue1, mValue2;
	
	public BranchEq(final byte... data) {
		super(data);
	}
	
	public BranchEq(final int code, final byte... data) {
		super(code, data);
	}
	
	public BranchEq(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
	}
	
	public BranchEq(final IStackBasedCpu cpu) {
		super(cpu);
	}
	
	@Override
	protected boolean checkCondition(final IStackBasedCpu cpu) {
		mValue2 = cpu.getStack().popWord();
		exLog("Popped %s", mValue2);
		mValue1 = cpu.getStack().popWord();
		exLog("Popped %s", mValue1);
		return mValue1 == mValue2;
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.BRANCHEQ.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.BRANCHEQ.getName();
	}
	
	protected int getValue1() {
		return mValue1;
	}
	
	protected int getValue2() {
		return mValue2;
	}
	
}
