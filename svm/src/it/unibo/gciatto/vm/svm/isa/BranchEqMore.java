package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class BranchEqMore extends BranchEq {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -8230514725079241935L;
	
	public BranchEqMore(final byte... data) {
		super(data);
	}
	
	public BranchEqMore(final int code, final byte... data) {
		super(code, data);
	}
	
	public BranchEqMore(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
	}
	
	public BranchEqMore(final IStackBasedCpu cpu) {
		super(cpu);
	}
	
	@Override
	protected boolean checkCondition(final IStackBasedCpu cpu) {
		super.checkCondition(cpu);
		return getValue1() >= getValue2();
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.BRANCHEQMORE.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.BRANCHEQMORE.getName();
	}
}
