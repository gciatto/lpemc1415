package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class BranchNotEq extends BranchEq {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -8230514725079241935L;
	
	public BranchNotEq(final byte... data) {
		super(data);
	}
	
	public BranchNotEq(final int code, final byte... data) {
		super(code, data);
	}
	
	public BranchNotEq(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
	}
	
	public BranchNotEq(final IStackBasedCpu cpu) {
		super(cpu);
	}
	
	@Override
	protected boolean checkCondition(final IStackBasedCpu cpu) {
		super.checkCondition(cpu);
		return getValue1() != getValue2();
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.BRANCHNOTEQ.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.BRANCHNOTEQ.getName();
	}
}
