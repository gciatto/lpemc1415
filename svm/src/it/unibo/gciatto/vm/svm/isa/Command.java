package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

abstract class Command extends SVMInstruction implements IInstruction {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -1780872777205848303L;
	
	public Command(final byte... data) {
		super(data);
	}
	
	public Command(final int code, final byte... data) {
		super(code, data);
	}
	
	public Command(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
	}
	
	public Command(final IStackBasedCpu cpu) {
		super(cpu);
	}
	
}
