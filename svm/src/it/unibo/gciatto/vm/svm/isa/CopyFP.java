package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class CopyFP extends Command {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -8113654954807188960L;
	
	public CopyFP(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public CopyFP(final int code, final byte... data) {
		super(code, data);
	}
	
	public CopyFP(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public CopyFP(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.COPYFP.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.COPYFP.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		final IRegister fp, sp;
		fp = cpu.getFramePointer();
		sp = cpu.getStackPointer();
		fp.setValue(sp.getValue());
		exLog("SP copied into FP: %s", sp);
	}
	
}
