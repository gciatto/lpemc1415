package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

class Div extends BinaryWordOperator implements IInstruction {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 8476575700522716897L;
	
	public Div(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public Div(final int code, final byte... data) {
		super(code, data);
	}
	
	public Div(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public Div(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected int calculate(final int first, final int second) {
		return first / second;
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.DIV.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.DIV.getName();
	}
	
}
