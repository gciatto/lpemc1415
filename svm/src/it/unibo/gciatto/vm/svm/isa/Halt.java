package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class Halt extends Command {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -9103694854036846235L;
	
	public Halt(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public Halt(final int code, final byte... data) {
		super(code, data);
	}
	
	public Halt(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public Halt(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.HALT.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.HALT.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		cpu.halt();
		exLog("Halted");
	}
	
}
