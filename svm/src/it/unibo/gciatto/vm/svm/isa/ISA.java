package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.utils.ByteUtils;
import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.svm.parsing.SVMParser;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public final class ISA {

	public static enum Instructions {
		AND(
				"and",
				SVMParser.AND),
				OR(
						"or",
						SVMParser.OR),
						XOR(
								"xor",
								SVMParser.XOR),
								ADD(
										"add",
										SVMParser.ADD),
										SUB(
												"sub",
												SVMParser.SUB),
												HALT(
														"halt",
														SVMParser.HALT),
														MULT(
																"mult",
																SVMParser.MULT),
																DIV(
																		"div",
																		SVMParser.DIV),
																		PUSH(
																				"push",
																				SVMParser.PUSH),
																				POP(
																						"pop",
																						SVMParser.POP),
																						STOREW(
																								"sw",
																								SVMParser.STOREW),
																								LOADW(
																										"lw",
																										SVMParser.LOADW),
																										BRANCH(
																												"b",
																												SVMParser.BRANCH),
																												BRANCHEQ(
																														"beq",
																														SVMParser.BRANCHEQ),
																														BRANCHNOTEQ(
																																"bneq",
																																SVMParser.BRANCHNOTEQ),
																																BRANCHLESS(
																																		"bless",
																																		SVMParser.BRANCHLESS),
																																		BRANCHEQLESS(
																																				"beless",
																																				SVMParser.BRANCHEQLESS),
																																				BRANCHMORE(
																																						"bmore",
																																						SVMParser.BRANCHMORE),
																																						BRANCHEQMORE(
																																								"bemore",
																																								SVMParser.BRANCHEQMORE),
																																								JS(
																																										"js",
																																										SVMParser.JS),
																																										PRINT(
																																												"print",
																																												SVMParser.PRINT),
																																												STOREFP(
																																														"sfp",
																																														SVMParser.STOREFP),
																																														STOREHP(
																																																"shp",
																																																SVMParser.STOREHP),
																																																STORERA(
																																																		"sra",
																																																		SVMParser.STORERA),
																																																		STORERV(
																																																				"srv",
																																																				SVMParser.STORERV),
																																																				LOADFP(
																																																						"lfp",
																																																						SVMParser.LOADFP),
																																																						COPYFP(
																																																								"cfp",
																																																								SVMParser.COPYFP),
																																																								LOADHP(
																																																										"lhp",
																																																										SVMParser.LOADHP),
																																																										LOADRA(
																																																												"lra",
																																																												SVMParser.LOADRA),
																																																												LOADRV(
																																																														"lrv",
																																																														SVMParser.LOADRV);

		public static Instructions fromCode(final int code) {
			return VALUES_BY_CODES.get(code);
		}

		public static Instructions fromName(final String name) {
			return VALUES_BY_NAMES.get(name);
		}

		public static final Map<Integer, Instructions> VALUES_BY_CODES = new HashMap<Integer, ISA.Instructions>();

		public static final Map<String, Instructions> VALUES_BY_NAMES = new HashMap<String, ISA.Instructions>();
		static {
			for (final Instructions i : Instructions.values()) {
				VALUES_BY_CODES.put(i.getCode(), i);
				VALUES_BY_NAMES.put(i.getName(), i);
			}
		}

		private final String mName;

		private final int mCode;

		private Instructions(final int code) {
			mName = name();
			mCode = code;
		}

		private Instructions(final String name, final int code) {
			mName = name;
			mCode = code;
		}

		public int getCode() {
			return mCode;
		}

		public String getName() {
			return mName;
		}
	}

	private static IInstruction _instance(final String name, final int code, final byte... data) {
		final Class<?> clazz = getIstructionClass(name);
		if (clazz == null) {
			return null;
		}
		try {
			final Constructor<?> constructor = clazz.getConstructor(int.class, byte[].class);
			final Object inst = constructor.newInstance(code, data);
			return (IInstruction) inst;
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
			return null;
		}
	}

	public static IInstruction decode(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		final int code = codeMemory.loadWord(istructionPointer.getValue());
		final IInstruction i = INSTANCES.get(Instructions.fromCode(code));
		return i;
	}

	private static Class<?> getIstructionClass(final String name) {
		final IInstruction instance = INSTANCES.get(Instructions.fromName(name));
		if (instance != null) {
			return instance.getClass();
		}
		return null;
	}

	public static IInstruction instance(final String name, final int... parameter) {
		final IInstruction result;
		final Instructions instruction = Instructions.fromName(name);
		if (instruction == null) {
			result = null;
		} else {
			if (parameter.length == 0) {
				result = _instance(name, instruction.getCode());
			} else {
				final byte[] params = ByteUtils.wordToBytes(parameter[0]);
				final byte[] data = new byte[] {
						(byte) instruction.getCode(), params[0], params[1], params[2], params[3]
				};
				result = _instance(name, instruction.getCode(), data);
			}
		}
		if (result == null) {
			L.warning("Null instruction: " + name);
		}
		return result;
	}

	private static final Logger L = Logger.getLogger(ISA.class.getSimpleName());

	public static final int WORD_SIZE = Integer.BYTES;

	public static final int ISTRUCTION_SIZE = WORD_SIZE;

	private static final Map<Instructions, IInstruction> INSTANCES = new HashMap<ISA.Instructions, IInstruction>();

	static {
		INSTANCES.put(Instructions.AND, new And((byte) Instructions.AND.getCode()));
		INSTANCES.put(Instructions.OR, new Or((byte) Instructions.OR.getCode()));
		INSTANCES.put(Instructions.XOR, new Xor((byte) Instructions.XOR.getCode()));
		INSTANCES.put(Instructions.ADD, new Add((byte) Instructions.ADD.getCode()));
		INSTANCES.put(Instructions.DIV, new Div((byte) Instructions.DIV.getCode()));
		INSTANCES.put(Instructions.HALT, new Halt((byte) Instructions.HALT.getCode()));
		INSTANCES.put(Instructions.MULT, new Mult((byte) Instructions.MULT.getCode()));
		INSTANCES.put(Instructions.SUB, new Sub((byte) Instructions.SUB.getCode()));
		INSTANCES.put(Instructions.PUSH, new Push((byte) Instructions.PUSH.getCode()));
		INSTANCES.put(Instructions.POP, new Pop((byte) Instructions.POP.getCode()));
		INSTANCES.put(Instructions.STOREW, new StoreW((byte) Instructions.STOREW.getCode()));
		INSTANCES.put(Instructions.LOADW, new LoadW((byte) Instructions.LOADW.getCode()));
		INSTANCES.put(Instructions.BRANCH, new Branch((byte) Instructions.BRANCH.getCode()));
		INSTANCES.put(Instructions.BRANCHEQ, new BranchEq((byte) Instructions.BRANCHEQ.getCode()));
		INSTANCES.put(Instructions.BRANCHNOTEQ, new BranchNotEq((byte) Instructions.BRANCHNOTEQ.getCode()));
		INSTANCES.put(Instructions.BRANCHLESS, new BranchLess((byte) Instructions.BRANCHLESS.getCode()));
		INSTANCES.put(Instructions.BRANCHMORE, new BranchMore((byte) Instructions.BRANCHMORE.getCode()));
		INSTANCES.put(Instructions.BRANCHEQLESS, new BranchEqLess((byte) Instructions.BRANCHEQLESS.getCode()));
		INSTANCES.put(Instructions.BRANCHEQMORE, new BranchEqMore((byte) Instructions.BRANCHEQMORE.getCode()));
		INSTANCES.put(Instructions.JS, new Js((byte) Instructions.JS.getCode()));
		INSTANCES.put(Instructions.PRINT, new Print((byte) Instructions.PRINT.getCode()));
		INSTANCES.put(Instructions.STOREFP, new StoreFP((byte) Instructions.STOREFP.getCode()));
		INSTANCES.put(Instructions.STOREHP, new StoreHP((byte) Instructions.STOREHP.getCode()));
		INSTANCES.put(Instructions.STORERA, new StoreRA((byte) Instructions.STORERA.getCode()));
		INSTANCES.put(Instructions.STORERV, new StoreRV((byte) Instructions.STORERV.getCode()));
		INSTANCES.put(Instructions.LOADFP, new LoadFP((byte) Instructions.LOADFP.getCode()));
		INSTANCES.put(Instructions.LOADHP, new LoadHP((byte) Instructions.LOADHP.getCode()));
		INSTANCES.put(Instructions.LOADRA, new LoadRA((byte) Instructions.LOADRA.getCode()));
		INSTANCES.put(Instructions.LOADRV, new LoadRV((byte) Instructions.LOADRV.getCode()));
		INSTANCES.put(Instructions.COPYFP, new CopyFP((byte) Instructions.COPYFP.getCode()));
	}
}
