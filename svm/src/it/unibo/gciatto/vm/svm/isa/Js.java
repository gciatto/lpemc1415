package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class Js extends Command {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -5090514624426046306L;
	private int mAddress, mIP;
	
	public Js(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public Js(final int code, final byte... data) {
		super(code, data);
	}
	
	public Js(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public Js(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.JS.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.JS.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		mAddress = cpu.getStack().popWord();
		exLog("Popped %s", mAddress);
		mIP = cpu.getInstructionPointer().getValue();
		cpu.getReturnAddress().setValue(mIP);
		cpu.getInstructionPointer().setValue(mAddress);
		exLog("RA set to %s", mIP);
		exLog("Jump to %s", mAddress);
	}
	
}
