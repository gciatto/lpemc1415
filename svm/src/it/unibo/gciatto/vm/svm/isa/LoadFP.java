package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class LoadFP extends LoadR {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -6418544202630956414L;
	
	public LoadFP(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public LoadFP(final int code, final byte... data) {
		super(code, data);
	}
	
	public LoadFP(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public LoadFP(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.LOADFP.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.LOADFP.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		loadR(cpu.getStack(), cpu.getFramePointer());
	}
	
}
