package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class LoadHP extends LoadR {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -1350534919283334868L;
	
	public LoadHP(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public LoadHP(final int code, final byte... data) {
		super(code, data);
	}
	
	public LoadHP(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public LoadHP(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.LOADHP.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.LOADHP.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		loadR(cpu.getStack(), cpu.getHeapPointer());
	}
	
}
