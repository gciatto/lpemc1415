package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStack;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

abstract class LoadR extends Command {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -324401992703287722L;
	private int mValue;
	
	public LoadR(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public LoadR(final int code, final byte... data) {
		super(code, data);
	}
	
	public LoadR(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public LoadR(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	protected void loadR(final IStack stack, final IRegister register) {
		mValue = register.getValue();
		stack.push(mValue);
		exLog("Pushed %s", mValue);
	}
	
}
