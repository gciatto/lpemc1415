package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class LoadRA extends LoadR {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -3953734520107178952L;
	
	public LoadRA(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public LoadRA(final int code, final byte... data) {
		super(code, data);
	}
	
	public LoadRA(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public LoadRA(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.LOADRA.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.LOADRA.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		loadR(cpu.getStack(), cpu.getReturnAddress());
	}
	
}
