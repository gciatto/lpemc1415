package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class LoadRV extends LoadR {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -4070501582260649563L;
	
	public LoadRV(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public LoadRV(final int code, final byte... data) {
		super(code, data);
	}
	
	public LoadRV(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public LoadRV(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.LOADRV.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.LOADRV.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		loadR(cpu.getStack(), cpu.getReturnValue());
	}
	
}
