package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

class LoadW extends Command {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -2721590293560611151L;
	private int mValue, mAddress;
	
	public LoadW(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public LoadW(final int code, final byte... data) {
		super(code, data);
	}
	
	public LoadW(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public LoadW(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.LOADW.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.LOADW.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		mAddress = cpu.getStack().popWord();
		exLog("Popped %s", mAddress);
		mValue = cpu.getPrimaryMemory().loadWord(mAddress);
		cpu.getStack().push(mValue);
		exLog("Pushed %s", mValue);
	}
	
}
