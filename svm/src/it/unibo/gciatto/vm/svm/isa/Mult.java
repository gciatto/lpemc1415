package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

class Mult extends BinaryWordOperator implements IInstruction {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 5909284981398669695L;
	
	public Mult(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public Mult(final int code, final byte... data) {
		super(code, data);
	}
	
	public Mult(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public Mult(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected int calculate(final int first, final int second) {
		return first * second;
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.MULT.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.MULT.getName();
	}
	
}
