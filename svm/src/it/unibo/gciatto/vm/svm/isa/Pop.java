package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class Pop extends Command {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -8836739546505358589L;
	
	public Pop(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public Pop(final int code, final byte... data) {
		super(code, data);
	}
	
	public Pop(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public Pop(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.POP.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.POP.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		final int word = cpu.getStack().popWord();
		exLog("Popped %s", word);
	}
	
}
