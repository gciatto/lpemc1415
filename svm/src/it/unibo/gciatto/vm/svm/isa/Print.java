package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class Print extends Command {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -5094688779700325620L;
	private int mValue;
	
	public Print(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public Print(final int code, final byte... data) {
		super(code, data);
	}
	
	public Print(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public Print(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.PRINT.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.PRINT.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		mValue = cpu.getStack().peekWord();
		cpu.getPrintStream().printf("%s\n", mValue);
		exLog("Printed %s", mValue);
	}
	
}
