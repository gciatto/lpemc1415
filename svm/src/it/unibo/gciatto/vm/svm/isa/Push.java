package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.utils.ByteUtils;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

class Push extends Command {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 5972047894770285895L;
	protected int mWord;
	
	private String mLabelParam;
	
	public Push(final byte... data) {
		super(data);
		if (data.length >= 5) {
			mWord = ByteUtils.bytesToWord(data[1], data[2], data[3], data[4]);
		}
		setSize(2);
	}
	
	public Push(final int code, final byte... data) {
		super(code, data);
		setSize(2);
	}
	
	public Push(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		setSize(2);
	}
	
	public Push(final IStackBasedCpu cpu) {
		super(cpu);
		setSize(2);
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.PUSH.getCode();
	}
	
	@Override
	public String getLabelParam() {
		return mLabelParam;
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.PUSH.getName();
	}
	
	@Override
	public byte[] getParams() {
		return ByteUtils.wordToBytes(mWord);
	}
	
	@Override
	public boolean hasLabelParam() {
		return mLabelParam != null;
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		mWord = getParamWord(cpu);
		cpu.getStack().push(mWord);
		exLog("Pushed %s", mWord);
	}
	
	@Override
	public void setLabelParam(final String label) {
		mLabelParam = label;
	}

	@Override
	public void setParams(final byte[] data) {
		mWord = ByteUtils.bytesToWord(data);
	}

	@Override
	public byte[] toByteArray() {
		final byte[] word = ByteUtils.wordToBytes(mWord);
		return new byte[] {
				(byte) 0, (byte) 0, (byte) 0, getCodeAsByte(), word[0], word[1], word[2], word[3],
		};
	}
	
	@Override
	public String toString() {
		return String.format("%s %s", super.toString(), hasLabelParam() ? mLabelParam : mWord);
	}
	
	@Override
	public int[] toWordsArray() {
		return new int[] {
				getCode(), mWord
		};
	}
	
}
