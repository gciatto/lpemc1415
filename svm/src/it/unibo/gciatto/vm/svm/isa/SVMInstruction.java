package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.utils.ByteUtils;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;
import it.unibo.gciatto.vm.stack.impl.AbstractStackBasedInstruction;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class SVMInstruction extends AbstractStackBasedInstruction {
	
	protected static void exLog(final String msg, final Object... params) {
		if (DEBUG) {
			L.log(Level.INFO, String.format(msg + "\n", params));
		}
	}
	
	/**
	 *
	 */
	private static final long serialVersionUID = 4594176769158558167L;
	protected static final Logger L = Logger.getGlobal(); // .getLogger(Instruction.class.getSimpleName());
	
	protected static final boolean DEBUG = false;
	
	public SVMInstruction(final byte... data) {
		super(data);
	}
	
	public SVMInstruction(final int code, final byte... data) {
		super(code, data);
	}
	
	public SVMInstruction(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
	}
	
	public SVMInstruction(final IStackBasedCpu cpu) {
		super(cpu.getPrimaryMemory(), cpu.getInstructionPointer());
	}
	
	protected byte getCodeAsByte() {
		return (byte) getCode();
	}
	
	@Override
	public String getLabelParam() {
		return null;
	}
	
	protected int getParamWord(final IStackBasedCpu cpu) {
		return cpu.getPrimaryMemory().loadWord(getIndex() + ISA.ISTRUCTION_SIZE / cpu.getPrimaryMemory().getGranularity());
	}
	
	@Override
	public boolean hasLabelParam() {
		return false;
	}
	
	@Override
	public void onFetch(final IStackBasedCpu cpu) throws Exception {
		setIndex(cpu.getInstructionPointer().getValue());
	}
	
	@Override
	public void setLabelParam(final String label) {

	}
	
	@Override
	public byte[] toByteArray() {
		return ByteUtils.wordToBytes(getCode());
	}
	
	@Override
	public int[] toWordsArray() {
		return new int[] {
			getCode()
		};
	}
	
}
