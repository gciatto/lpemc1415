package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class StoreFP extends StoreR {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 8990760795848543788L;
	
	public StoreFP(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public StoreFP(final int code, final byte... data) {
		super(code, data);
	}
	
	public StoreFP(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public StoreFP(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.STOREFP.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.STOREFP.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		storeR(cpu.getStack(), cpu.getFramePointer());
	}
	
}
