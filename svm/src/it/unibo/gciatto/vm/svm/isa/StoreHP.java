package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class StoreHP extends StoreR {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 7568751798072884977L;
	
	public StoreHP(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public StoreHP(final int code, final byte... data) {
		super(code, data);
	}
	
	public StoreHP(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public StoreHP(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.STOREHP.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.STOREHP.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		storeR(cpu.getStack(), cpu.getHeapPointer());
	}
	
}
