package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStack;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

abstract class StoreR extends Command {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -2188329758879751374L;
	private int mValue;
	
	public StoreR(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public StoreR(final int code, final byte... data) {
		super(code, data);
	}
	
	public StoreR(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public StoreR(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	protected void storeR(final IStack stack, final IRegister register) {
		mValue = stack.popWord();
		exLog("Popped %s", mValue);
		register.setValue(mValue);
		exLog("%s set to %s", register.getName(), mValue);
	}
	
}
