package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class StoreRA extends StoreR {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -3566427603331979067L;
	
	public StoreRA(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public StoreRA(final int code, final byte... data) {
		super(code, data);
	}
	
	public StoreRA(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public StoreRA(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.STORERA.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.STORERA.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		storeR(cpu.getStack(), cpu.getReturnAddress());
	}
	
}
