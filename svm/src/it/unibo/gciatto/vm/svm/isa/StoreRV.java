package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

public class StoreRV extends StoreR {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -4479567567748701323L;
	
	public StoreRV(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public StoreRV(final int code, final byte... data) {
		super(code, data);
	}
	
	public StoreRV(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public StoreRV(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.STORERV.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.STORERV.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		storeR(cpu.getStack(), cpu.getReturnValue());
	}
	
}
