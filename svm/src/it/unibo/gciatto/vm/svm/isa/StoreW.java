package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

class StoreW extends Command {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -7408891880576895864L;
	private int mValue, mAddress;
	
	public StoreW(final byte... data) {
		super(data);
		// TODO Auto-generated constructor stub
	}
	
	public StoreW(final int code, final byte... data) {
		super(code, data);
	}
	
	public StoreW(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
		// TODO Auto-generated constructor stub
	}
	
	public StoreW(final IStackBasedCpu cpu) {
		super(cpu);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.STOREW.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.STOREW.getName();
	}
	
	@Override
	public void onExecute(final IStackBasedCpu cpu) throws Exception {
		mAddress = cpu.getStack().popWord();
		
		mValue = cpu.getStack().popWord();
		
		cpu.getPrimaryMemory().storeWord(mAddress, mValue);
		exLog("Popped %s", mAddress);
		exLog("Popped %s", mValue);
		exLog("Mem[%s] set to %s", mAddress, mValue);
	}
	
}
