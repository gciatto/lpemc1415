package it.unibo.gciatto.vm.svm.isa;

import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.IRegister;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;

class Xor extends BinaryWordOperator implements IInstruction {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -4241861420110313465L;
	
	public Xor(final byte... data) {
		super(data);
	}
	
	public Xor(final int code, final byte... data) {
		super(code, data);
	}
	
	public Xor(final IRandomAccessMemory codeMemory, final IRegister istructionPointer) {
		super(codeMemory, istructionPointer);
	}
	
	public Xor(final IStackBasedCpu cpu) {
		super(cpu);
	}
	
	@Override
	protected int calculate(final int first, final int second) {
		return first ^ second;
	}
	
	@Override
	public int getCode() {
		return ISA.Instructions.XOR.getCode();
	}
	
	@Override
	public String getName() {
		return ISA.Instructions.XOR.getName();
	}
	
}
