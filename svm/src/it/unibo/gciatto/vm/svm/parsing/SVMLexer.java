// $ANTLR 3.4 /home/gciatto/Scrivania/svm-git/svm/res/SVM.g 2015-02-20 12:58:43

package it.unibo.gciatto.vm.svm.parsing;

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.DFA;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.Lexer;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;

@SuppressWarnings({
		"all", "warnings", "unchecked"
})
public class SVMLexer extends Lexer {
	class DFA7 extends DFA {
		
		public DFA7(final BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			decisionNumber = 7;
			eot = DFA7_eot;
			eof = DFA7_eof;
			min = DFA7_min;
			max = DFA7_max;
			accept = DFA7_accept;
			special = DFA7_special;
			transition = DFA7_transition;
		}
		
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( PUSH | POP | ADD | SUB | MULT | DIV | STOREW | LOADW | BRANCH | BRANCHEQ | BRANCHLESS | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT | AND | OR | XOR | BRANCHNOTEQ | BRANCHEQLESS | BRANCHEQMORE | BRANCHMORE | COL | LABEL | NUMBER | WHITESP | COMMENT );";
		}
	}
	
	public static final int EOF = -1;
	public static final int ADD = 4;
	public static final int AND = 5;
	public static final int BRANCH = 6;
	public static final int BRANCHEQ = 7;
	public static final int BRANCHEQLESS = 8;
	public static final int BRANCHEQMORE = 9;
	public static final int BRANCHLESS = 10;
	public static final int BRANCHMORE = 11;
	public static final int BRANCHNOTEQ = 12;
	public static final int COL = 13;
	public static final int COMMENT = 14;
	public static final int COPYFP = 15;
	public static final int DIV = 16;
	public static final int HALT = 17;
	public static final int JS = 18;
	public static final int LABEL = 19;
	public static final int LOADFP = 20;
	public static final int LOADHP = 21;
	public static final int LOADRA = 22;
	public static final int LOADRV = 23;
	public static final int LOADW = 24;
	public static final int MULT = 25;
	public static final int NUMBER = 26;
	public static final int OR = 27;
	public static final int POP = 28;
	public static final int PRINT = 29;
	public static final int PUSH = 30;
	public static final int STOREFP = 31;
	public static final int STOREHP = 32;
	public static final int STORERA = 33;
	public static final int STORERV = 34;
	public static final int STOREW = 35;
	public static final int SUB = 36;
	public static final int WHITESP = 37;
	
	public static final int XOR = 38;
	
	protected DFA7 dfa7 = new DFA7(this);
	static final String DFA7_eotS = "\1\uffff\6\16\1\46\5\16\5\uffff\6\16\1\62\5\16\1\71\7\16\1\uffff" + "\1\104\2\16\1\107\2\16\1\112\1\16\1\114\1\115\1\116\1\uffff\1\117" + "\1\120\1\121\1\122\1\16\1\124\1\uffff\1\125\1\126\1\127\1\130\1" + "\131\5\16\1\uffff\1\137\1\16\1\uffff\1\141\1\142\1\uffff\1\16\7" + "\uffff\1\144\6\uffff\3\16\1\150\1\16\1\uffff\1\152\2\uffff\1\153" + "\1\uffff\2\16\1\156\1\uffff\1\157\2\uffff\1\160\1\161\4\uffff";
	static final String DFA7_eofS = "\162\uffff";
	static final String DFA7_minS = "\1\11\1\157\1\144\1\146\1\165\1\151\1\146\1\44\1\163\1\146\1\141" + "\1\162\1\157\5\uffff\1\163\1\160\1\151\2\144\1\142\1\44\1\141\2" + "\160\1\154\1\166\1\44\1\141\2\160\1\154\2\145\1\157\1\uffff\1\44" + "\1\160\1\154\1\44\1\162\1\150\1\44\1\156\3\44\1\uffff\4\44\1\164" + "\1\44\1\uffff\5\44\1\145\1\157\1\163\1\161\1\162\1\uffff\1\44\1" + "\164\1\uffff\2\44\1\uffff\1\164\7\uffff\1\44\6\uffff\1\163\1\162" + "\1\163\1\44\1\145\1\uffff\1\44\2\uffff\1\44\1\uffff\1\163\1\145" + "\1\44\1\uffff\1\44\2\uffff\2\44\4\uffff";
	
	static final String DFA7_maxS = "\1\172\1\165\1\156\1\167\1\165\1\151\1\167\1\172\1\163\1\146\1\141" + "\1\162\1\157\5\uffff\1\163\1\160\1\151\2\144\1\142\1\172\1\166\2" + "\160\1\154\1\166\1\172\1\166\2\160\1\161\2\145\1\157\1\uffff\1\172" + "\1\160\1\154\1\172\1\162\1\150\1\172\1\156\3\172\1\uffff\4\172\1" + "\164\1\172\1\uffff\5\172\1\145\1\157\1\163\1\161\1\162\1\uffff\1" + "\172\1\164\1\uffff\2\172\1\uffff\1\164\7\uffff\1\172\6\uffff\1\163" + "\1\162\1\163\1\172\1\145\1\uffff\1\172\2\uffff\1\172\1\uffff\1\163" + "\1\145\1\172\1\uffff\1\172\2\uffff\2\172\4\uffff";
	
	static final String DFA7_acceptS = "\15\uffff\1\37\1\40\1\41\1\42\1\43\24\uffff\1\11\13\uffff\1\7\6" + "\uffff\1\10\12\uffff\1\14\2\uffff\1\31\2\uffff\1\2\1\uffff\1\3\1" + "\30\1\4\1\16\1\20\1\22\1\25\1\uffff\1\6\1\15\1\17\1\21\1\24\1\12" + "\5\uffff\1\23\1\uffff\1\32\1\1\1\uffff\1\5\3\uffff\1\33\1\uffff" + "\1\27\1\26\2\uffff\1\13\1\36\1\34\1\35";
	
	static final String DFA7_specialS = "\162\uffff}>";
	
	static final String[] DFA7_transitionS = {
			"\2\20\2\uffff\1\20\22\uffff\1\20\3\uffff\1\16\10\uffff\1\17" + "\2\uffff\12\17\1\15\6\uffff\32\16\1\21\3\uffff\1\16\1\uffff" + "\1\2\1\7\1\11\1\5\3\16\1\12\1\16\1\10\1\16\1\6\1\4\1\16\1\13" + "\1\1\2\16\1\3\4\16\1\14\2\16",
			"\1\23\2\uffff\1\24\2\uffff\1\22",
			"\1\25\11\uffff\1\26",
			"\1\32\1\uffff\1\33\11\uffff\1\31\2\uffff\1\27\1\uffff\1\30",
			"\1\34",
			"\1\35",
			"\1\40\1\uffff\1\41\11\uffff\1\37\4\uffff\1\36",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\4\16" + "\1\42\6\16\1\43\1\45\1\44\14\16",
			"\1\47",
			"\1\50",
			"\1\51",
			"\1\52",
			"\1\53",
			"",
			"",
			"",
			"",
			"",
			"\1\54",
			"\1\55",
			"\1\56",
			"\1\57",
			"\1\60",
			"\1\61",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\63\24\uffff\1\64",
			"\1\65",
			"\1\66",
			"\1\67",
			"\1\70",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\72\24\uffff\1\73",
			"\1\74",
			"\1\75",
			"\1\77\1\100\3\uffff\1\76",
			"\1\101",
			"\1\102",
			"\1\103",
			"",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\105",
			"\1\106",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\110",
			"\1\111",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\113",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\123",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\132",
			"\1\133",
			"\1\134",
			"\1\135",
			"\1\136",
			"",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\140",
			"",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"",
			"\1\143",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\145",
			"\1\146",
			"\1\147",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\151",
			"",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"",
			"",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"",
			"\1\154",
			"\1\155",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"",
			"",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"\1\16\13\uffff\12\16\7\uffff\32\16\4\uffff\1\16\1\uffff\32" + "\16",
			"",
			"",
			"",
			""
	};
	
	static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
	
	static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
	
	static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
	
	static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
	
	static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
	
	static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
	
	static final short[][] DFA7_transition;
	
	static {
		final int numStates = DFA7_transitionS.length;
		DFA7_transition = new short[numStates][];
		for (int i = 0; i < numStates; i++) {
			DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
		}
	}
	
	public SVMLexer() {}
	
	public SVMLexer(final CharStream input) {
		this(input, new RecognizerSharedState());
	}
	
	public SVMLexer(final CharStream input, final RecognizerSharedState state) {
		super(input, state);
	}
	
	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}
	
	@Override
	public String getGrammarFileName() {
		return "/home/gciatto/Scrivania/svm-git/svm/res/SVM.g";
	}
	
	// $ANTLR start "ADD"
	public final void mADD() throws RecognitionException {
		try {
			final int _type = ADD;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:108:6: ( 'add' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:108:8: 'add'
			{
				match("add");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "ADD"
	
	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			final int _type = AND;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:129:5: ( 'and' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:129:7: 'and'
			{
				match("and");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "AND"
	
	// $ANTLR start "BRANCH"
	public final void mBRANCH() throws RecognitionException {
		try {
			final int _type = BRANCH;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:114:9: ( 'b' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:114:11: 'b'
			{
				match('b');
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "BRANCH"
	
	// $ANTLR start "BRANCHEQ"
	public final void mBRANCHEQ() throws RecognitionException {
		try {
			final int _type = BRANCHEQ;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:115:10: ( 'beq' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:115:12: 'beq'
			{
				match("beq");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "BRANCHEQ"
	
	// $ANTLR start "BRANCHEQLESS"
	public final void mBRANCHEQLESS() throws RecognitionException {
		try {
			final int _type = BRANCHEQLESS;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:133:13: ( 'beless' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:133:14: 'beless'
			{
				match("beless");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "BRANCHEQLESS"
	
	// $ANTLR start "BRANCHEQMORE"
	public final void mBRANCHEQMORE() throws RecognitionException {
		try {
			final int _type = BRANCHEQMORE;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:134:13: ( 'bemore' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:134:14: 'bemore'
			{
				match("bemore");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "BRANCHEQMORE"
	
	// $ANTLR start "BRANCHLESS"
	public final void mBRANCHLESS() throws RecognitionException {
		try {
			final int _type = BRANCHLESS;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:116:11: ( 'bless' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:116:12: 'bless'
			{
				match("bless");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "BRANCHLESS"
	
	// $ANTLR start "BRANCHMORE"
	public final void mBRANCHMORE() throws RecognitionException {
		try {
			final int _type = BRANCHMORE;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:135:11: ( 'bmore' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:135:12: 'bmore'
			{
				match("bmore");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "BRANCHMORE"
	
	// $ANTLR start "BRANCHNOTEQ"
	public final void mBRANCHNOTEQ() throws RecognitionException {
		try {
			final int _type = BRANCHNOTEQ;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:132:13: ( 'bneq' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:132:15: 'bneq'
			{
				match("bneq");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "BRANCHNOTEQ"
	
	// $ANTLR start "COL"
	public final void mCOL() throws RecognitionException {
		try {
			final int _type = COL;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:137:6: ( ':' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:137:8: ':'
			{
				match(':');
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "COL"
	
	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			final int _type = COMMENT;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:143:10: ( '[' ( options {greedy=false; } : . )* ']' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:143:12: '[' ( options {greedy=false; } : . )* ']'
			{
				match('[');
				
				// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:143:16: ( options {greedy=false; } : . )*
				loop6: do {
					int alt6 = 2;
					final int LA6_0 = input.LA(1);
					
					if (LA6_0 == ']') {
						alt6 = 2;
					} else if (LA6_0 >= '\u0000' && LA6_0 <= '\\' || LA6_0 >= '^' && LA6_0 <= '\uFFFF') {
						alt6 = 1;
					}
					
					switch (alt6) {
						case 1:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:143:43: .
						{
							matchAny();
							
						}
							break;
						
						default:
							break loop6;
					}
				} while (true);
				
				match(']');
				
				skip();
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "COMMENT"
	
	// $ANTLR start "COPYFP"
	public final void mCOPYFP() throws RecognitionException {
		try {
			final int _type = COPYFP;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:124:10: ( 'cfp' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:124:12: 'cfp'
			{
				match("cfp");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "COPYFP"
	
	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			final int _type = DIV;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:111:6: ( 'div' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:111:8: 'div'
			{
				match("div");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "DIV"
	
	// $ANTLR start "HALT"
	public final void mHALT() throws RecognitionException {
		try {
			final int _type = HALT;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:128:7: ( 'halt' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:128:9: 'halt'
			{
				match("halt");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "HALT"
	
	// $ANTLR start "JS"
	public final void mJS() throws RecognitionException {
		try {
			final int _type = JS;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:117:5: ( 'js' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:117:7: 'js'
			{
				match("js");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "JS"
	
	// $ANTLR start "LABEL"
	public final void mLABEL() throws RecognitionException {
		try {
			final int _type = LABEL;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:138:8: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '$' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' | '$' )* )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:138:10: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '$' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' | '$' )*
			{
				if (input.LA(1) == '$' || input.LA(1) >= 'A' && input.LA(1) <= 'Z' || input.LA(1) == '_' || input.LA(1) >= 'a' && input.LA(1) <= 'z') {
					input.consume();
				} else {
					final MismatchedSetException mse = new MismatchedSetException(null, input);
					recover(mse);
					throw mse;
				}
				
				// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:138:37: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' | '$' )*
				loop1: do {
					int alt1 = 2;
					final int LA1_0 = input.LA(1);
					
					if (LA1_0 == '$' || LA1_0 >= '0' && LA1_0 <= '9' || LA1_0 >= 'A' && LA1_0 <= 'Z' || LA1_0 == '_' || LA1_0 >= 'a' && LA1_0 <= 'z') {
						alt1 = 1;
					}
					
					switch (alt1) {
						case 1:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:
						{
							if (input.LA(1) == '$' || input.LA(1) >= '0' && input.LA(1) <= '9' || input.LA(1) >= 'A' && input.LA(1) <= 'Z' || input.LA(1) == '_' || input.LA(1) >= 'a' && input.LA(1) <= 'z') {
								input.consume();
							} else {
								final MismatchedSetException mse = new MismatchedSetException(null, input);
								recover(mse);
								throw mse;
							}
							
						}
							break;
						
						default:
							break loop1;
					}
				} while (true);
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "LABEL"
	
	// $ANTLR start "LOADFP"
	public final void mLOADFP() throws RecognitionException {
		try {
			final int _type = LOADFP;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:122:9: ( 'lfp' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:122:11: 'lfp'
			{
				match("lfp");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "LOADFP"
	
	// $ANTLR start "LOADHP"
	public final void mLOADHP() throws RecognitionException {
		try {
			final int _type = LOADHP;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:125:9: ( 'lhp' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:125:11: 'lhp'
			{
				match("lhp");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "LOADHP"
	
	// $ANTLR start "LOADRA"
	public final void mLOADRA() throws RecognitionException {
		try {
			final int _type = LOADRA;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:118:9: ( 'lra' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:118:11: 'lra'
			{
				match("lra");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "LOADRA"
	
	// $ANTLR start "LOADRV"
	public final void mLOADRV() throws RecognitionException {
		try {
			final int _type = LOADRV;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:120:9: ( 'lrv' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:120:11: 'lrv'
			{
				match("lrv");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "LOADRV"
	// $ANTLR start "LOADW"
	public final void mLOADW() throws RecognitionException {
		try {
			final int _type = LOADW;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:113:8: ( 'lw' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:113:10: 'lw'
			{
				match("lw");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "LOADW"
	// $ANTLR start "MULT"
	public final void mMULT() throws RecognitionException {
		try {
			final int _type = MULT;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:110:7: ( 'mult' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:110:9: 'mult'
			{
				match("mult");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "MULT"
	// $ANTLR start "NUMBER"
	public final void mNUMBER() throws RecognitionException {
		try {
			final int _type = NUMBER;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:139:9: ( '0' | ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* ) )
			int alt4 = 2;
			final int LA4_0 = input.LA(1);
			
			if (LA4_0 == '0') {
				alt4 = 1;
			} else if (LA4_0 == '-' || LA4_0 >= '1' && LA4_0 <= '9') {
				alt4 = 2;
			} else {
				final NoViableAltException nvae = new NoViableAltException("", 4, 0, input);
				
				throw nvae;
				
			}
			switch (alt4) {
				case 1:
				// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:139:11: '0'
				{
					match('0');
					
				}
					break;
				case 2:
				// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:139:17: ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* )
				{
					// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:139:17: ( '-' )?
					int alt2 = 2;
					final int LA2_0 = input.LA(1);
					
					if (LA2_0 == '-') {
						alt2 = 1;
					}
					switch (alt2) {
						case 1:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:139:18: '-'
						{
							match('-');
							
						}
							break;
					
					}
					
					// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:139:23: ( ( '1' .. '9' ) ( '0' .. '9' )* )
					// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:139:24: ( '1' .. '9' ) ( '0' .. '9' )*
					{
						if (input.LA(1) >= '1' && input.LA(1) <= '9') {
							input.consume();
						} else {
							final MismatchedSetException mse = new MismatchedSetException(null, input);
							recover(mse);
							throw mse;
						}
						
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:139:34: ( '0' .. '9' )*
						loop3: do {
							int alt3 = 2;
							final int LA3_0 = input.LA(1);
							
							if (LA3_0 >= '0' && LA3_0 <= '9') {
								alt3 = 1;
							}
							
							switch (alt3) {
								case 1:
								// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:
								{
									if (input.LA(1) >= '0' && input.LA(1) <= '9') {
										input.consume();
									} else {
										final MismatchedSetException mse = new MismatchedSetException(null, input);
										recover(mse);
										throw mse;
									}
									
								}
									break;
								
								default:
									break loop3;
							}
						} while (true);
						
					}
					
				}
					break;
			
			}
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "NUMBER"
	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			final int _type = OR;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:130:4: ( 'or' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:130:6: 'or'
			{
				match("or");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "OR"
	// $ANTLR start "POP"
	public final void mPOP() throws RecognitionException {
		try {
			final int _type = POP;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:107:6: ( 'pop' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:107:8: 'pop'
			{
				match("pop");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "POP"
	// $ANTLR start "PRINT"
	public final void mPRINT() throws RecognitionException {
		try {
			final int _type = PRINT;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:127:8: ( 'print' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:127:10: 'print'
			{
				match("print");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "PRINT"
	// $ANTLR start "PUSH"
	public final void mPUSH() throws RecognitionException {
		try {
			final int _type = PUSH;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:106:7: ( 'push' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:106:9: 'push'
			{
				match("push");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "PUSH"
	
	// $ANTLR start "STOREFP"
	public final void mSTOREFP() throws RecognitionException {
		try {
			final int _type = STOREFP;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:123:10: ( 'sfp' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:123:12: 'sfp'
			{
				match("sfp");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "STOREFP"
	// $ANTLR start "STOREHP"
	public final void mSTOREHP() throws RecognitionException {
		try {
			final int _type = STOREHP;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:126:10: ( 'shp' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:126:12: 'shp'
			{
				match("shp");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "STOREHP"
	// $ANTLR start "STORERA"
	public final void mSTORERA() throws RecognitionException {
		try {
			final int _type = STORERA;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:119:10: ( 'sra' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:119:12: 'sra'
			{
				match("sra");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "STORERA"
	// $ANTLR start "STORERV"
	public final void mSTORERV() throws RecognitionException {
		try {
			final int _type = STORERV;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:121:10: ( 'srv' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:121:12: 'srv'
			{
				match("srv");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "STORERV"
	// $ANTLR start "STOREW"
	public final void mSTOREW() throws RecognitionException {
		try {
			final int _type = STOREW;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:112:9: ( 'sw' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:112:11: 'sw'
			{
				match("sw");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "STOREW"
	// $ANTLR start "SUB"
	public final void mSUB() throws RecognitionException {
		try {
			final int _type = SUB;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:109:6: ( 'sub' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:109:8: 'sub'
			{
				match("sub");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "SUB"
	@Override
	public void mTokens() throws RecognitionException {
		// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:8: ( PUSH | POP | ADD | SUB | MULT | DIV | STOREW | LOADW | BRANCH | BRANCHEQ | BRANCHLESS | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT | AND | OR | XOR | BRANCHNOTEQ | BRANCHEQLESS | BRANCHEQMORE | BRANCHMORE | COL | LABEL | NUMBER | WHITESP | COMMENT )
		int alt7 = 35;
		alt7 = dfa7.predict(input);
		switch (alt7) {
			case 1:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:10: PUSH
			{
				mPUSH();
				
			}
				break;
			case 2:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:15: POP
			{
				mPOP();
				
			}
				break;
			case 3:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:19: ADD
			{
				mADD();
				
			}
				break;
			case 4:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:23: SUB
			{
				mSUB();
				
			}
				break;
			case 5:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:27: MULT
			{
				mMULT();
				
			}
				break;
			case 6:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:32: DIV
			{
				mDIV();
				
			}
				break;
			case 7:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:36: STOREW
			{
				mSTOREW();
				
			}
				break;
			case 8:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:43: LOADW
			{
				mLOADW();
				
			}
				break;
			case 9:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:49: BRANCH
			{
				mBRANCH();
				
			}
				break;
			case 10:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:56: BRANCHEQ
			{
				mBRANCHEQ();
				
			}
				break;
			case 11:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:65: BRANCHLESS
			{
				mBRANCHLESS();
				
			}
				break;
			case 12:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:76: JS
			{
				mJS();
				
			}
				break;
			case 13:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:79: LOADRA
			{
				mLOADRA();
				
			}
				break;
			case 14:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:86: STORERA
			{
				mSTORERA();
				
			}
				break;
			case 15:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:94: LOADRV
			{
				mLOADRV();
				
			}
				break;
			case 16:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:101: STORERV
			{
				mSTORERV();
				
			}
				break;
			case 17:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:109: LOADFP
			{
				mLOADFP();
				
			}
				break;
			case 18:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:116: STOREFP
			{
				mSTOREFP();
				
			}
				break;
			case 19:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:124: COPYFP
			{
				mCOPYFP();
				
			}
				break;
			case 20:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:131: LOADHP
			{
				mLOADHP();
				
			}
				break;
			case 21:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:138: STOREHP
			{
				mSTOREHP();
				
			}
				break;
			case 22:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:146: PRINT
			{
				mPRINT();
				
			}
				break;
			case 23:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:152: HALT
			{
				mHALT();
				
			}
				break;
			case 24:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:157: AND
			{
				mAND();
				
			}
				break;
			case 25:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:161: OR
			{
				mOR();
				
			}
				break;
			case 26:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:164: XOR
			{
				mXOR();
				
			}
				break;
			case 27:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:168: BRANCHNOTEQ
			{
				mBRANCHNOTEQ();
				
			}
				break;
			case 28:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:180: BRANCHEQLESS
			{
				mBRANCHEQLESS();
				
			}
				break;
			case 29:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:193: BRANCHEQMORE
			{
				mBRANCHEQMORE();
				
			}
				break;
			case 30:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:206: BRANCHMORE
			{
				mBRANCHMORE();
				
			}
				break;
			case 31:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:217: COL
			{
				mCOL();
				
			}
				break;
			case 32:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:221: LABEL
			{
				mLABEL();
				
			}
				break;
			case 33:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:227: NUMBER
			{
				mNUMBER();
				
			}
				break;
			case 34:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:234: WHITESP
			{
				mWHITESP();
				
			}
				break;
			case 35:
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:1:242: COMMENT
			{
				mCOMMENT();
				
			}
				break;
		
		}
		
	}
	
	// $ANTLR start "WHITESP"
	public final void mWHITESP() throws RecognitionException {
		try {
			final int _type = WHITESP;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:141:10: ( ( '\\t' | ' ' | '\\r' | '\\n' )+ )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:141:12: ( '\\t' | ' ' | '\\r' | '\\n' )+
			{
				// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:141:12: ( '\\t' | ' ' | '\\r' | '\\n' )+
				int cnt5 = 0;
				loop5: do {
					int alt5 = 2;
					final int LA5_0 = input.LA(1);
					
					if (LA5_0 >= '\t' && LA5_0 <= '\n' || LA5_0 == '\r' || LA5_0 == ' ') {
						alt5 = 1;
					}
					
					switch (alt5) {
						case 1:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:
						{
							if (input.LA(1) >= '\t' && input.LA(1) <= '\n' || input.LA(1) == '\r' || input.LA(1) == ' ') {
								input.consume();
							} else {
								final MismatchedSetException mse = new MismatchedSetException(null, input);
								recover(mse);
								throw mse;
							}
							
						}
							break;
						
						default:
							if (cnt5 >= 1) {
								break loop5;
							}
							final EarlyExitException eee = new EarlyExitException(5, input);
							throw eee;
					}
					cnt5++;
				} while (true);
				
				skip();
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	
	// $ANTLR end "WHITESP"
	
	// $ANTLR start "XOR"
	public final void mXOR() throws RecognitionException {
		try {
			final int _type = XOR;
			final int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:131:5: ( 'xor' )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:131:7: 'xor'
			{
				match("xor");
				
			}
			
			state.type = _type;
			state.channel = _channel;
		} finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "XOR"
	
}