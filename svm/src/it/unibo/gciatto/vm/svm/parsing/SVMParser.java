// $ANTLR 3.4 /home/gciatto/Scrivania/svm-git/svm/res/SVM.g 2015-02-20 12:58:43

package it.unibo.gciatto.vm.svm.parsing;

import it.unibo.gciatto.vm.ICompiler;
import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.svm.SVMCompiler;

import org.antlr.runtime.BitSet;
import org.antlr.runtime.Parser;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenStream;

@SuppressWarnings({
		"all", "warnings", "unchecked"
})
public class SVMParser extends Parser {
	public static final String[] tokenNames = new String[] {
			"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ADD", "AND", "BRANCH", "BRANCHEQ", "BRANCHEQLESS", "BRANCHEQMORE", "BRANCHLESS", "BRANCHMORE", "BRANCHNOTEQ", "COL", "COMMENT", "COPYFP", "DIV", "HALT", "JS", "LABEL", "LOADFP", "LOADHP", "LOADRA", "LOADRV", "LOADW", "MULT", "NUMBER", "OR", "POP", "PRINT", "PUSH", "STOREFP", "STOREHP", "STORERA", "STORERV", "STOREW", "SUB", "WHITESP", "XOR"
	};
	
	public static final int EOF = -1;
	public static final int ADD = 4;
	public static final int AND = 5;
	public static final int BRANCH = 6;
	public static final int BRANCHEQ = 7;
	public static final int BRANCHEQLESS = 8;
	public static final int BRANCHEQMORE = 9;
	public static final int BRANCHLESS = 10;
	public static final int BRANCHMORE = 11;
	public static final int BRANCHNOTEQ = 12;
	public static final int COL = 13;
	public static final int COMMENT = 14;
	public static final int COPYFP = 15;
	public static final int DIV = 16;
	public static final int HALT = 17;
	public static final int JS = 18;
	public static final int LABEL = 19;
	public static final int LOADFP = 20;
	public static final int LOADHP = 21;
	public static final int LOADRA = 22;
	public static final int LOADRV = 23;
	public static final int LOADW = 24;
	public static final int MULT = 25;
	public static final int NUMBER = 26;
	public static final int OR = 27;
	public static final int POP = 28;
	public static final int PRINT = 29;
	public static final int PUSH = 30;
	public static final int STOREFP = 31;
	public static final int STOREHP = 32;
	public static final int STORERA = 33;
	public static final int STORERV = 34;
	public static final int STOREW = 35;
	public static final int SUB = 36;
	public static final int WHITESP = 37;
	public static final int XOR = 38;
	
	private ICompiler mCompiler = new SVMCompiler();
	
	// delegators
	
	private String mLabel = null;
	public static final BitSet FOLLOW_PUSH_in_assembly42 = new BitSet(new long[] {
		0x0000000004000000L
	});
	
	public static final BitSet FOLLOW_NUMBER_in_assembly48 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_PUSH_in_assembly64 = new BitSet(new long[] {
		0x0000000000080000L
	});
	
	public static final BitSet FOLLOW_LABEL_in_assembly70 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_POP_in_assembly88 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	
	public static final BitSet FOLLOW_ADD_in_assembly106 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	
	public static final BitSet FOLLOW_SUB_in_assembly124 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	
	public static final BitSet FOLLOW_MULT_in_assembly142 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	
	public static final BitSet FOLLOW_DIV_in_assembly160 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	
	// Delegated rules
	
	public static final BitSet FOLLOW_STOREW_in_assembly178 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_LOADW_in_assembly201 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_LABEL_in_assembly223 = new BitSet(new long[] {
		0x0000000000002000L
	});
	public static final BitSet FOLLOW_COL_in_assembly225 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_BRANCH_in_assembly244 = new BitSet(new long[] {
		0x0000000000080000L
	});
	public static final BitSet FOLLOW_LABEL_in_assembly250 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_BRANCHEQ_in_assembly268 = new BitSet(new long[] {
		0x0000000000080000L
	});
	public static final BitSet FOLLOW_LABEL_in_assembly274 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_BRANCHLESS_in_assembly294 = new BitSet(new long[] {
		0x0000000000080000L
	});
	public static final BitSet FOLLOW_LABEL_in_assembly300 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_JS_in_assembly319 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_LOADRA_in_assembly341 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_STORERA_in_assembly359 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_LOADRV_in_assembly377 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_STORERV_in_assembly400 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_LOADFP_in_assembly421 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_STOREFP_in_assembly444 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_COPYFP_in_assembly466 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_LOADHP_in_assembly485 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_STOREHP_in_assembly503 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_PRINT_in_assembly525 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_HALT_in_assembly548 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_BRANCHEQLESS_in_assembly568 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_BRANCHEQMORE_in_assembly581 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_BRANCHMORE_in_assembly594 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_BRANCHNOTEQ_in_assembly607 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_AND_in_assembly620 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_OR_in_assembly633 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	public static final BitSet FOLLOW_XOR_in_assembly646 = new BitSet(new long[] {
		0x0000005FFBFF9FF2L
	});
	
	public SVMParser(final TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	
	public SVMParser(final TokenStream input, final RecognizerSharedState state) {
		super(input, state);
	}
	
	// $ANTLR start "assembly"
	// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:39:1: assembly : (push1= PUSH number1= NUMBER |push2= PUSH label1= LABEL |pop= POP |add= ADD |sub= SUB |mult= MULT |div= DIV |sw= STOREW |lw= LOADW |lbl= LABEL COL |b= BRANCH label2= LABEL |beq= BRANCHEQ label3= LABEL |bless= BRANCHLESS label4= LABEL |js= JS |lra= LOADRA |sra= STORERA |lrv= LOADRV |srv= STORERV |lfp= LOADFP |sfp= STOREFP |cfp= COPYFP |lhp= LOADHP |shp= STOREHP |print= PRINT |halt= HALT |beless= BRANCHEQLESS |bemore= BRANCHEQMORE |bmore= BRANCHMORE |bneq= BRANCHNOTEQ |and= AND |or= OR |xor= XOR )* ;
	public final void assembly() throws RecognitionException {
		Token push1 = null;
		Token number1 = null;
		Token push2 = null;
		Token label1 = null;
		Token pop = null;
		Token add = null;
		Token sub = null;
		Token mult = null;
		Token div = null;
		Token sw = null;
		Token lw = null;
		Token lbl = null;
		Token b = null;
		Token label2 = null;
		Token beq = null;
		Token label3 = null;
		Token bless = null;
		Token label4 = null;
		Token js = null;
		Token lra = null;
		Token sra = null;
		Token lrv = null;
		Token srv = null;
		Token lfp = null;
		Token sfp = null;
		Token cfp = null;
		Token lhp = null;
		Token shp = null;
		Token print = null;
		Token halt = null;
		Token beless = null;
		Token bemore = null;
		Token bmore = null;
		Token bneq = null;
		Token and = null;
		Token or = null;
		Token xor = null;
		
		try {
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:39:9: ( (push1= PUSH number1= NUMBER |push2= PUSH label1= LABEL |pop= POP |add= ADD |sub= SUB |mult= MULT |div= DIV |sw= STOREW |lw= LOADW |lbl= LABEL COL |b= BRANCH label2= LABEL |beq= BRANCHEQ label3= LABEL |bless= BRANCHLESS label4= LABEL |js= JS |lra= LOADRA |sra= STORERA |lrv= LOADRV |srv= STORERV |lfp= LOADFP |sfp= STOREFP |cfp= COPYFP |lhp= LOADHP |shp= STOREHP |print= PRINT |halt= HALT |beless= BRANCHEQLESS |bemore= BRANCHEQMORE |bmore= BRANCHMORE |bneq= BRANCHNOTEQ |and= AND |or= OR |xor= XOR )* )
			// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:39:11: (push1= PUSH number1= NUMBER |push2= PUSH label1= LABEL |pop= POP |add= ADD |sub= SUB |mult= MULT |div= DIV |sw= STOREW |lw= LOADW |lbl= LABEL COL |b= BRANCH label2= LABEL |beq= BRANCHEQ label3= LABEL |bless= BRANCHLESS label4= LABEL |js= JS |lra= LOADRA |sra= STORERA |lrv= LOADRV |srv= STORERV |lfp= LOADFP |sfp= STOREFP |cfp= COPYFP |lhp= LOADHP |shp= STOREHP |print= PRINT |halt= HALT |beless= BRANCHEQLESS |bemore= BRANCHEQMORE |bmore= BRANCHMORE |bneq= BRANCHNOTEQ |and= AND |or= OR |xor= XOR )*
			{
				// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:39:11: (push1= PUSH number1= NUMBER |push2= PUSH label1= LABEL |pop= POP |add= ADD |sub= SUB |mult= MULT |div= DIV |sw= STOREW |lw= LOADW |lbl= LABEL COL |b= BRANCH label2= LABEL |beq= BRANCHEQ label3= LABEL |bless= BRANCHLESS label4= LABEL |js= JS |lra= LOADRA |sra= STORERA |lrv= LOADRV |srv= STORERV |lfp= LOADFP |sfp= STOREFP |cfp= COPYFP |lhp= LOADHP |shp= STOREHP |print= PRINT |halt= HALT |beless= BRANCHEQLESS |bemore= BRANCHEQMORE |bmore= BRANCHMORE |bneq= BRANCHNOTEQ |and= AND |or= OR |xor= XOR )*
				loop1: do {
					int alt1 = 33;
					switch (input.LA(1)) {
						case PUSH: {
							final int LA1_2 = input.LA(2);
							
							if (LA1_2 == NUMBER) {
								alt1 = 1;
							} else if (LA1_2 == LABEL) {
								alt1 = 2;
							}
							
						}
							break;
						case POP: {
							alt1 = 3;
						}
							break;
						case ADD: {
							alt1 = 4;
						}
							break;
						case SUB: {
							alt1 = 5;
						}
							break;
						case MULT: {
							alt1 = 6;
						}
							break;
						case DIV: {
							alt1 = 7;
						}
							break;
						case STOREW: {
							alt1 = 8;
						}
							break;
						case LOADW: {
							alt1 = 9;
						}
							break;
						case LABEL: {
							alt1 = 10;
						}
							break;
						case BRANCH: {
							alt1 = 11;
						}
							break;
						case BRANCHEQ: {
							alt1 = 12;
						}
							break;
						case BRANCHLESS: {
							alt1 = 13;
						}
							break;
						case JS: {
							alt1 = 14;
						}
							break;
						case LOADRA: {
							alt1 = 15;
						}
							break;
						case STORERA: {
							alt1 = 16;
						}
							break;
						case LOADRV: {
							alt1 = 17;
						}
							break;
						case STORERV: {
							alt1 = 18;
						}
							break;
						case LOADFP: {
							alt1 = 19;
						}
							break;
						case STOREFP: {
							alt1 = 20;
						}
							break;
						case COPYFP: {
							alt1 = 21;
						}
							break;
						case LOADHP: {
							alt1 = 22;
						}
							break;
						case STOREHP: {
							alt1 = 23;
						}
							break;
						case PRINT: {
							alt1 = 24;
						}
							break;
						case HALT: {
							alt1 = 25;
						}
							break;
						case BRANCHEQLESS: {
							alt1 = 26;
						}
							break;
						case BRANCHEQMORE: {
							alt1 = 27;
						}
							break;
						case BRANCHMORE: {
							alt1 = 28;
						}
							break;
						case BRANCHNOTEQ: {
							alt1 = 29;
						}
							break;
						case AND: {
							alt1 = 30;
						}
							break;
						case OR: {
							alt1 = 31;
						}
							break;
						case XOR: {
							alt1 = 32;
						}
							break;
					
					}
					
					switch (alt1) {
						case 1:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:39:13: push1= PUSH number1= NUMBER
						{
							push1 = (Token) match(input, PUSH, FOLLOW_PUSH_in_assembly42);
							
							number1 = (Token) match(input, NUMBER, FOLLOW_NUMBER_in_assembly48);
							
							putInstruction(push1 != null ? push1.getText() : null, Integer.parseInt(number1 != null ? number1.getText() : null));
							
						}
							break;
						case 2:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:41:6: push2= PUSH label1= LABEL
						{
							push2 = (Token) match(input, PUSH, FOLLOW_PUSH_in_assembly64);
							
							label1 = (Token) match(input, LABEL, FOLLOW_LABEL_in_assembly70);
							
							putInstruction(push2 != null ? push2.getText() : null, label1 != null ? label1.getText() : null);
							
						}
							break;
						case 3:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:43:6: pop= POP
						{
							pop = (Token) match(input, POP, FOLLOW_POP_in_assembly88);
							
							putInstruction(pop != null ? pop.getText() : null);
							
						}
							break;
						case 4:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:45:6: add= ADD
						{
							add = (Token) match(input, ADD, FOLLOW_ADD_in_assembly106);
							
							putInstruction(add != null ? add.getText() : null);
							
						}
							break;
						case 5:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:47:6: sub= SUB
						{
							sub = (Token) match(input, SUB, FOLLOW_SUB_in_assembly124);
							
							putInstruction(sub != null ? sub.getText() : null);
							
						}
							break;
						case 6:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:49:6: mult= MULT
						{
							mult = (Token) match(input, MULT, FOLLOW_MULT_in_assembly142);
							
							putInstruction(mult != null ? mult.getText() : null);
							
						}
							break;
						case 7:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:51:6: div= DIV
						{
							div = (Token) match(input, DIV, FOLLOW_DIV_in_assembly160);
							
							putInstruction(div != null ? div.getText() : null);
							
						}
							break;
						case 8:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:53:6: sw= STOREW
						{
							sw = (Token) match(input, STOREW, FOLLOW_STOREW_in_assembly178);
							
							putInstruction(sw != null ? sw.getText() : null);
							
						}
							break;
						case 9:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:56:6: lw= LOADW
						{
							lw = (Token) match(input, LOADW, FOLLOW_LOADW_in_assembly201);
							
							putInstruction(lw != null ? lw.getText() : null);
							
						}
							break;
						case 10:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:59:6: lbl= LABEL COL
						{
							lbl = (Token) match(input, LABEL, FOLLOW_LABEL_in_assembly223);
							
							match(input, COL, FOLLOW_COL_in_assembly225);
							
							mLabel = lbl != null ? lbl.getText() : null;
							
						}
							break;
						case 11:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:61:6: b= BRANCH label2= LABEL
						{
							b = (Token) match(input, BRANCH, FOLLOW_BRANCH_in_assembly244);
							
							label2 = (Token) match(input, LABEL, FOLLOW_LABEL_in_assembly250);
							
							putInstruction(b != null ? b.getText() : null, label2 != null ? label2.getText() : null);
							
						}
							break;
						case 12:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:63:6: beq= BRANCHEQ label3= LABEL
						{
							beq = (Token) match(input, BRANCHEQ, FOLLOW_BRANCHEQ_in_assembly268);
							
							label3 = (Token) match(input, LABEL, FOLLOW_LABEL_in_assembly274);
							
							putInstruction(beq != null ? beq.getText() : null, label3 != null ? label3.getText() : null);
							
						}
							break;
						case 13:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:65:6: bless= BRANCHLESS label4= LABEL
						{
							bless = (Token) match(input, BRANCHLESS, FOLLOW_BRANCHLESS_in_assembly294);
							
							label4 = (Token) match(input, LABEL, FOLLOW_LABEL_in_assembly300);
							
							putInstruction(bless != null ? bless.getText() : null, label4 != null ? label4.getText() : null);
							
						}
							break;
						case 14:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:67:6: js= JS
						{
							js = (Token) match(input, JS, FOLLOW_JS_in_assembly319);
							
							putInstruction(js != null ? js.getText() : null);
							
						}
							break;
						case 15:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:70:6: lra= LOADRA
						{
							lra = (Token) match(input, LOADRA, FOLLOW_LOADRA_in_assembly341);
							
							putInstruction(lra != null ? lra.getText() : null);
							
						}
							break;
						case 16:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:72:6: sra= STORERA
						{
							sra = (Token) match(input, STORERA, FOLLOW_STORERA_in_assembly359);
							
							putInstruction(sra != null ? sra.getText() : null);
							
						}
							break;
						case 17:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:74:6: lrv= LOADRV
						{
							lrv = (Token) match(input, LOADRV, FOLLOW_LOADRV_in_assembly377);
							
							putInstruction(lrv != null ? lrv.getText() : null);
							
						}
							break;
						case 18:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:76:6: srv= STORERV
						{
							srv = (Token) match(input, STORERV, FOLLOW_STORERV_in_assembly400);
							
							putInstruction(srv != null ? srv.getText() : null);
							
						}
							break;
						case 19:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:78:6: lfp= LOADFP
						{
							lfp = (Token) match(input, LOADFP, FOLLOW_LOADFP_in_assembly421);
							
							putInstruction(lfp != null ? lfp.getText() : null);
							
						}
							break;
						case 20:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:80:6: sfp= STOREFP
						{
							sfp = (Token) match(input, STOREFP, FOLLOW_STOREFP_in_assembly444);
							
							putInstruction(sfp != null ? sfp.getText() : null);
							
						}
							break;
						case 21:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:82:6: cfp= COPYFP
						{
							cfp = (Token) match(input, COPYFP, FOLLOW_COPYFP_in_assembly466);
							
							putInstruction(cfp != null ? cfp.getText() : null);
							
						}
							break;
						case 22:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:84:6: lhp= LOADHP
						{
							lhp = (Token) match(input, LOADHP, FOLLOW_LOADHP_in_assembly485);
							
							putInstruction(lhp != null ? lhp.getText() : null);
							
						}
							break;
						case 23:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:86:6: shp= STOREHP
						{
							shp = (Token) match(input, STOREHP, FOLLOW_STOREHP_in_assembly503);
							
							putInstruction(shp != null ? shp.getText() : null);
							
						}
							break;
						case 24:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:88:6: print= PRINT
						{
							print = (Token) match(input, PRINT, FOLLOW_PRINT_in_assembly525);
							
							putInstruction(print != null ? print.getText() : null);
							
						}
							break;
						case 25:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:90:6: halt= HALT
						{
							halt = (Token) match(input, HALT, FOLLOW_HALT_in_assembly548);
							
							putInstruction(halt != null ? halt.getText() : null);
							
						}
							break;
						case 26:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:92:6: beless= BRANCHEQLESS
						{
							beless = (Token) match(input, BRANCHEQLESS, FOLLOW_BRANCHEQLESS_in_assembly568);
							
							putInstruction(beless != null ? beless.getText() : null);
							
						}
							break;
						case 27:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:93:6: bemore= BRANCHEQMORE
						{
							bemore = (Token) match(input, BRANCHEQMORE, FOLLOW_BRANCHEQMORE_in_assembly581);
							
							putInstruction(bemore != null ? bemore.getText() : null);
							
						}
							break;
						case 28:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:94:6: bmore= BRANCHMORE
						{
							bmore = (Token) match(input, BRANCHMORE, FOLLOW_BRANCHMORE_in_assembly594);
							
							putInstruction(bmore != null ? bmore.getText() : null);
							
						}
							break;
						case 29:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:95:6: bneq= BRANCHNOTEQ
						{
							bneq = (Token) match(input, BRANCHNOTEQ, FOLLOW_BRANCHNOTEQ_in_assembly607);
							
							putInstruction(bneq != null ? bneq.getText() : null);
							
						}
							break;
						case 30:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:96:6: and= AND
						{
							and = (Token) match(input, AND, FOLLOW_AND_in_assembly620);
							
							putInstruction(and != null ? and.getText() : null);
							
						}
							break;
						case 31:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:97:6: or= OR
						{
							or = (Token) match(input, OR, FOLLOW_OR_in_assembly633);
							
							putInstruction(or != null ? or.getText() : null);
							
						}
							break;
						case 32:
						// /home/gciatto/Scrivania/svm-git/svm/res/SVM.g:98:6: xor= XOR
						{
							xor = (Token) match(input, XOR, FOLLOW_XOR_in_assembly646);
							
							putInstruction(xor != null ? xor.getText() : null);
							
						}
							break;
						
						default:
							break loop1;
					}
				} while (true);
				
			}
			
		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		}
		
		finally {
			// do for sure before leaving
		}
		return;
	}
	
	// $ANTLR end "assembly"
	public ICompiler getCompiler() {
		return mCompiler;
	}
	
	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}
	
	@Override
	public String getGrammarFileName() {
		return "/home/gciatto/Scrivania/svm-git/svm/res/SVM.g";
	}
	
	@Override
	public String[] getTokenNames() {
		return SVMParser.tokenNames;
	}
	
	private IInstruction putInstruction(final String name, final Object... args) {
		final IInstruction i = mCompiler.putInstruction(name, args);
		if (mLabel != null) {
			mCompiler.assignLabel(mLabel, i);
			mLabel = null;
		}
		return i;
	}
	
	public void setCompiler(final ICompiler compiler) {
		mCompiler = compiler;
	}
	
}