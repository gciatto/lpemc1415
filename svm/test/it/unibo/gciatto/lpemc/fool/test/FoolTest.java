package it.unibo.gciatto.lpemc.fool.test;

import it.unibo.gciatto.sdt.adt.IASTNode;
import it.unibo.gciatto.sdt.parsing.FOOLLexer;
import it.unibo.gciatto.sdt.parsing.FOOLParser;
import it.unibo.gciatto.utils.SizeUnit;
import it.unibo.gciatto.vm.ICompiler;
import it.unibo.gciatto.vm.IProgram;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.impl.Ram32Bit;
import it.unibo.gciatto.vm.stack.IStack;
import it.unibo.gciatto.vm.stack.IStack.Direction;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;
import it.unibo.gciatto.vm.stack.impl.Stack32Bit;
import it.unibo.gciatto.vm.svm.SVMCompiler;
import it.unibo.gciatto.vm.svm.SVMCpu;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class FoolTest {
	
	@Parameters(name = "test {index} code={0}")
	public static Iterable<Object[]> data() {
		return TestStrings.getTests();
	}

	private final String mCode;
	private final ANTLRStringStream mCodeStream;
	private final FOOLLexer mLexer;
	private final CommonTokenStream mTokenStream;
	private final FOOLParser mParser;
	private IASTNode<?> mAstRoot;
	private final ICompiler mCompiler = new SVMCompiler();
	private IProgram mProgram;
	private final IRandomAccessMemory mRam = new Ram32Bit(1, SizeUnit.MiB);
	private IStack mStack;
	private IStackBasedCpu mCpu;
	private final ByteArrayOutputStream mOutput = new ByteArrayOutputStream();
	private final PrintStream mOutputPrinter = new PrintStream(mOutput);
	private final int[] mExpected;
	
	private Throwable mThrown = null;
	
	public FoolTest(final String code, final int[] expected) {
		Logger.getGlobal().setLevel(Level.OFF);
		
		mCode = code;
		mCodeStream = new ANTLRStringStream(code);
		mLexer = new FOOLLexer(mCodeStream);
		mTokenStream = new CommonTokenStream(mLexer);
		mParser = new FOOLParser(mTokenStream);
		mExpected = expected;
		try {
			mAstRoot = mParser.prog();
			mProgram = mAstRoot.generate(mCompiler).compile();
			mStack = new Stack32Bit(mRam, Direction.POSITIVE, mProgram.getLength());
			mCpu = new SVMCpu(mRam, mStack, mOutputPrinter);
		} catch (final RecognitionException ex) {
			mThrown = ex;
		}
		
	}
	
	protected boolean checkResult() {
		if (isExpectingError()) {
			return mThrown != null;
		} else {
			return mOutput.toString().equals(generateExpectedString());
		}
	}
	
	protected String generateExpectedString() {
		if (isExpectingNoResult()) {
			return "";
		} else {
			final StringBuilder sb = new StringBuilder();
			sb.append(mExpected[0]);
			for (int i = 1; i < mExpected.length; i++) {
				sb.append("\n");
				sb.append(mExpected[i]);
			}
			sb.append("\n");
			return sb.toString();
		}
	}
	
	protected boolean isExpectingError() {
		return mExpected == null;
	}
	
	protected boolean isExpectingNoResult() {
		return mExpected != null && mExpected.length == 0;
	}
	
	@Test
	public void test() {
		if (mProgram != null && mCpu != null) {
			mProgram.execute(mCpu);
		}
		checkResult();
	}
	
}
