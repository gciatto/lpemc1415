package it.unibo.gciatto.lpemc.fool.test;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class TestStrings {
	
	public static Iterable<Object[]> getTests() {
		final List<Object[]> list = new LinkedList<Object[]>();
		List<Field> fields = Arrays.asList(TestStrings.class.getFields());
		fields = fields.stream().sorted((f1, f2) -> f1.getName().compareTo(f2.getName())).filter(f -> f.getName().startsWith("TEST_")).collect(Collectors.toList());
		
		for (int i = 0; i < fields.size(); i += 2) {
			try {
				final String code = (String) fields.get(i).get(mInstance);
				final int[] expected = (int[]) fields.get(i + 1).get(mInstance);
				list.add(new Object[] {
						code, expected
				});
			} catch (IllegalArgumentException | IllegalAccessException ex) {
				ex.printStackTrace();
			}
			
		}
		
		return list;
	}
	
	private static final TestStrings mInstance = new TestStrings();
	
	public static final String TEST_INT_BOOL = "let var x:int = 5+3; fun f:bool (n:int, m:int) let var x:bool = false;   in     x==(n==m);    in    print (    if f(7,8) then { false }     else { true } ) ;";
	public static final int[] TEST_INT_BOOL_EXP = new int[] {
		0
	};
	
	public static final String TEST_FUN_NEST = "let fun cube:int (n:int) let fun square:int (n:int) let in n*n;  in n*square(n); in print(cube(5));";
	public static final int[] TEST_FUN_NEST_EXP = new int[] {
		125
	};
	
	public static final String TEST_FUNCTIONAL_1 = "let fun cube:int (n:int) let in n*n*n; fun f:int() cube(5); fun g:int(x:()->int) print(x()); in g(f);";
	public static final int[] TEST_FUNCTIONAL_1_EXP = TEST_FUN_NEST_EXP;
	
	public static final String TEST_PROVA_ZAVATTARO = "let class Integer (i:int) { fun getInt:int () i; } class Double extends Integer (j:int) { fun getDec:int () j; } class AddInt (n: Integer) { fun get:Integer () n; fun add:Integer (m:Double) new Integer(n.getInt()+m.getInt()); } class AddDouble extends AddInt (n: Double) { fun add:Double (l:Integer) new Double(n.getInt()+l.getInt(),n.getDec()); } var x:Double = new Double(11,26); var y:AddInt = new AddDouble(x); var z:Integer = y.add(x); in print(z.getInt());";
	public static final int[] TEST_PROVA_ZAVATTARO_EXP = new int[] {
		22
	};
	
	public static final String TEST_QUICKSORT_ZAVATTARO = "let class List (f:int, r:List) { fun first:int() f; fun rest:List() r; } fun printList:List (l:List) let fun makeList:List (l:List, i:int) new List (i,l); fun printL:List (l:List) if (l == null) then {null} else {makeList(printL(l.rest()),print(l.first()))}; in printL(l); fun append:List (l1:List, l2:List) if (l1 == null) then {l2} else {new List(l1.first(), append(l1.rest(),l2))} ; fun filter:List (l:List,check:(int)->bool) if (l == null) then {null} else {if (check(l.first())) then {new List(l.first(),filter(l.rest(),check))} else {filter(l.rest(),check)}}; fun quicksort:List (l:List, rel:(int,int)->bool) let var pivot:int = if (l==null) then {0} else {l.first()}; fun prec:bool (x:int) rel(x,pivot); fun succ:bool (x:int) rel(pivot,x); in if (l == null) then {null} else {append( quicksort(filter(l,prec),rel), new List(pivot, quicksort(filter(l,succ),rel)) )}; fun increasing:List(l:List) let fun inc:bool (x:int,y:int) not(y<=x); in quicksort(l,inc); fun decreasing:List(l:List) let fun dec:bool (x:int,y:int) not(x<=y); in quicksort(l,dec); var l:List = new List (3, new List(4, new List(6, new List (1, new List(2, new List(5,null)))))); in printList(increasing(l)); ";
	public static final int[] TEST_QUICKSORT_ZAVATTARO_EXP = new int[] {
		1, 2, 3, 4, 5, 6
	};
	
	public static final String TEST_GCIATTO = "let var showValue:bool = true; fun cube:int(n:int) let fun square:int(x:int) n * n; in n * square(n); fun showFunction:int (x:(int)->int, val:int) let fun identity:int(n:int) n * showValue; in x(val)*identity(2); fun showFunction1:int (x:(int)->int, val:int) let fun identity:int(n:int) n * showValue; in x(val)*identity(3); in print(showFunction1(cube, 4));";
	public static final int[] TEST_GCIATTO_EXP = new int[] {
		4 * 4 * 4 * 3
	};
	
	public static final String TEST_COMPLEX = "let class Complex (re:int, im:int) { fun real:int() re; fun immaginary:int() im; fun add:Complex(c:Complex) new Complex(re + c.real(), im + c.immaginary()); } var com:Complex = new Complex(0, 0); var unit:Complex = new Complex(7, 7); var com1:Complex = com.add(unit); var com2:Complex = com1.add(unit); in print(com2.real());";
	public static final int[] TEST_COMPLEX_EXP = new int[] {
		14
	};

	public static final String TEST_FUNCTIONAL_2 = "let fun sum:int(x:int, y:int) x + y; fun printFun:int(v:int, f:(int)->int) print(f(v)); fun printBin:int(val:int, binary:(int, int) -> int) let var pivot:int = 7; fun internal:int(x:int) binary(print(x), print(pivot)); in printFun(val, internal); var number:int = 10; in printBin(number, sum);";
	public static final int[] TEST_FUNCTIONAL_2_EXP = new int[] {
		7, 10, 17
	};

	public static final String TEST_OOP_1 = "let class A (a:int) { fun getA:int() a; } var obj1:A = new A(11); in print(obj1.getA());";
	public static final int[] TEST_OOP_1_EXP = new int[] {
		11
	};

	public static final String TEST_OOP_2 = "let class A (a:int) { fun getA:int() a; } class B extends A() { fun getA:int() 0 - 1; } var obj1:A = new A(11); var obj2:B = new B(11); in print(obj2.getA());";
	public static final int[] TEST_OOP_2_EXP = new int[] {
		-1
	};

	public static final String TEST_OOP_3 = "let class A (a:int) { fun getA:int() a; } class B extends A() { fun getA:int() 5; } var val:int = 11; var obj1:A = new A(val); var obj2:B = new B(val); fun test:bool() (obj1.getA() == val) && (obj2.getA() == 5); in print(test());";
	public static final int[] TEST_OOP_3_EXP = new int[] {
		1
	};

	private TestStrings() {}
}
