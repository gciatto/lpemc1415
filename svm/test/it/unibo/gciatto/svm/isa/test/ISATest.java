package it.unibo.gciatto.svm.isa.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.unibo.gciatto.utils.SizeUnit;
import it.unibo.gciatto.vm.ICompiler;
import it.unibo.gciatto.vm.IInstruction;
import it.unibo.gciatto.vm.IProgram;
import it.unibo.gciatto.vm.IRandomAccessMemory;
import it.unibo.gciatto.vm.impl.Ram;
import it.unibo.gciatto.vm.stack.IStack;
import it.unibo.gciatto.vm.stack.IStackBasedCpu;
import it.unibo.gciatto.vm.stack.impl.Stack;
import it.unibo.gciatto.vm.svm.SVMCompiler;
import it.unibo.gciatto.vm.svm.SVMCpu;
import it.unibo.gciatto.vm.svm.isa.ISA;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class ISATest {
	private IRandomAccessMemory mRam;
	private IStack mStack;
	private ICompiler mCompiler;
	private IStackBasedCpu mCpu;
	private IProgram mProg;
	private int mCount = 0;
	private final Random mRand = new Random();
	private int mTemp = 0;
	
	protected void execute() {
		mProg.execute(mCpu);
	}
	
	@Before
	public void setUp() throws Exception {
		mCompiler = new SVMCompiler();
		mRam = new Ram(1, SizeUnit.KiB);
		mStack = new Stack(mRam);
		mCpu = new SVMCpu(mRam, mStack);
	}
	
	@Test
	public void testAdd() {
		final int val1 = mRand.nextInt();
		final int val2 = mRand.nextInt();
		try {
			mProg = mCompiler.compile(String.format("push %s push %s add halt", val1, val2));
			mCpu.setListener(new SVMCpu.CpuListener() {
				
				@Override
				public boolean isInstructionInteresting(final IInstruction instruction) {
					return instruction.getCode() == ISA.Instructions.ADD.getCode();
				}
				
				@Override
				public void onExecuted(final IInstruction fetched, final IStackBasedCpu cpu, final int ip) {
					mCount++;
					assertTrue(cpu.getStack().peekWord() == val1 + val2);
				}
				
			});
			mProg.execute(mCpu);
			assertTrue(mCount == 1);
		} catch (final Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test
	public void testBranch() {
		final String lbl = "lbl";
		try {
			mProg = mCompiler.compile(String.format("b %s halt push 1 halt %s: push 2 halt", lbl, lbl));
			mCpu.setListener(new SVMCpu.CpuListener() {
				
				@Override
				public boolean isInstructionInteresting(final IInstruction instruction) {
					return instruction.getCode() == ISA.Instructions.BRANCH.getCode();
				}
				
				@Override
				public void onExecuted(final IInstruction executed, final IStackBasedCpu cpu, final int ip) {
					mCount++;
					
					mCpu.setListener(new SVMCpu.CpuListener() {
						
						@Override
						public boolean isInstructionInteresting(final IInstruction instruction) {
							return instruction.getCode() == ISA.Instructions.PUSH.getCode();
						}
						
						@Override
						public void onExecuted(final IInstruction executed, final IStackBasedCpu cpu, final int ip) {
							mCount++;
						}
						
					});
				}
				
			});
			mProg.execute(mCpu);
			assertTrue(mCount == 2);
		} catch (final Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test
	public void testBranchEq() {
		try {
			mProg = mCompiler.compile("push 1 push 2 first: push 1 beq end push 50 push 50 beq first end: halt");
			mCpu.setListener(new SVMCpu.CpuListener() {
				
				@Override
				public boolean isInstructionInteresting(final IInstruction instruction) {
					return instruction.getCode() == ISA.Instructions.BRANCHEQ.getCode();
				}
				
				@Override
				public void onExecuted(final IInstruction executed, final IStackBasedCpu cpu, final int ip) {
					mCount++;
					
				}
				
			});
			mProg.execute(mCpu);
			assertTrue(mCount == 3);
		} catch (final Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test
	public void testBranchLess() {
		try {
			mProg = mCompiler.compile("push 70 push 0 push 5 push 2 bless end print push 1 push 2 here: bless here end: halt");
			mCpu.setListener(new SVMCpu.CpuListener() {
				
				@Override
				public boolean isInstructionInteresting(final IInstruction instruction) {
					return instruction.getCode() == ISA.Instructions.BRANCHLESS.getCode();
				}
				
				@Override
				public void onExecuted(final IInstruction executed, final IStackBasedCpu cpu, final int ip) {
					mCount++;
					
				}
				
			});
			mProg.execute(mCpu);
			assertTrue(mCount == 3);
		} catch (final Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test
	public void testCreation() {
		assertTrue(mCpu.getInstructionPointer().getValue() == 0);
		assertTrue(mCpu.getStackBase().getValue() == mRam.getSize() - 1);
		assertTrue(mCpu.getStack().isEmpty());
		assertTrue(mCpu.getHeapPointer().getValue() == 0);
		assertTrue(mCpu.getFramePointer().getValue() == mCpu.getStackPointer().getValue());
		assertTrue(mCpu.getReturnAddress().getValue() == 0);
		assertTrue(mCpu.getReturnValue().getValue() == 0);
	}
	
	@Test
	public void testDiv() {
		final int val1 = mRand.nextInt();
		final int val2 = mRand.nextInt();
		try {
			mProg = mCompiler.compile(String.format("push %s push %s div halt", val1, val2));
			mCpu.setListener(new SVMCpu.CpuListener() {
				
				@Override
				public boolean isInstructionInteresting(final IInstruction instruction) {
					return instruction.getCode() == ISA.Instructions.DIV.getCode();
				}
				
				@Override
				public void onExecuted(final IInstruction fetched, final IStackBasedCpu cpu, final int ip) {
					mCount++;
					assertTrue(cpu.getStack().peekWord() == val1 / val2);
				}
				
			});
			mProg.execute(mCpu);
			assertTrue(mCount == 1);
		} catch (final Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test
	public void testHalt() {
		try {
			mProg = mCompiler.compile("halt");
			mCpu.setListener(new SVMCpu.CpuListener() {
				
				@Override
				public void onExecuted(final IInstruction fetched, final IStackBasedCpu cpu, final int ip) {
					mCount++;
				}
				
			});
			mProg.execute(mCpu);
			assertTrue(mCount == 1);
		} catch (final Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test
	public void testJs() {
		final int addr = 50;
		final int val = mRand.nextInt();
		try {
			mProg = mCompiler.compile(String.format("push 1 push end print js print print print js end: halt", val, addr));
			mCpu.setListener(new SVMCpu.CpuListener() {
				
				@Override
				public void onExecuted(final IInstruction fetched, final IStackBasedCpu cpu, final int ip) {
					if (fetched.getCode() == ISA.Instructions.PRINT.getCode()) {
						mCount++;
					} else if (fetched.getCode() == ISA.Instructions.JS.getCode()) {
						assertTrue(cpu.getReturnAddress().getValue() == mTemp);
					}
				}
				
				@Override
				public void onFetched(final IInstruction fetched, final IStackBasedCpu cpu, final int ip) {
					if (fetched.getCode() == ISA.Instructions.JS.getCode()) {
						mTemp = cpu.getInstructionPointer().getValue() + fetched.getSize();
					}
				}
				
			});
			mProg.execute(mCpu);
			assertTrue(mCount == 1);
		} catch (final Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test
	public void testLoadW() {
		final int addr = 50;
		final int val = mRand.nextInt();
		mRam.storeWord(addr, val);
		try {
			mProg = mCompiler.compile(String.format("push %s lw halt", addr));
			mCpu.setListener(new SVMCpu.CpuListener() {
				
				@Override
				public boolean isInstructionInteresting(final IInstruction instruction) {
					return instruction.getCode() == ISA.Instructions.LOADW.getCode();
				}
				
				@Override
				public void onExecuted(final IInstruction fetched, final IStackBasedCpu cpu, final int ip) {
					mCount++;
					assertTrue(cpu.getStack().peekWord() == val);
				}
				
			});
			mProg.execute(mCpu);
			assertTrue(mCount == 1);
		} catch (final Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test
	public void testMult() {
		final int val1 = mRand.nextInt();
		final int val2 = mRand.nextInt();
		try {
			mProg = mCompiler.compile(String.format("push %s push %s mult halt", val1, val2));
			mCpu.setListener(new SVMCpu.CpuListener() {
				
				@Override
				public boolean isInstructionInteresting(final IInstruction instruction) {
					return instruction.getCode() == ISA.Instructions.MULT.getCode();
				}
				
				@Override
				public void onExecuted(final IInstruction fetched, final IStackBasedCpu cpu, final int ip) {
					mCount++;
					assertTrue(cpu.getStack().peekWord() == val1 * val2);
				}
				
			});
			mProg.execute(mCpu);
			assertTrue(mCount == 1);
		} catch (final Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test
	public void testPop() {
		final int val = mRand.nextInt();
		try {
			mProg = mCompiler.compile(String.format("push %s pop halt", val));
			mCpu.setListener(new SVMCpu.CpuListener() {
				
				@Override
				public boolean isInstructionInteresting(final IInstruction instruction) {
					return instruction.getCode() == ISA.Instructions.POP.getCode();
				}
				
				@Override
				public void onExecuted(final IInstruction fetched, final IStackBasedCpu cpu, final int ip) {
					mCount++;
					assertTrue(cpu.getStack().isEmpty());
				}
				
				@Override
				public void onFetched(final IInstruction fetched, final IStackBasedCpu cpu, final int ip) {
					assertTrue(cpu.getStack().peekWord() == val);
				}
				
			});
			mProg.execute(mCpu);
			assertTrue(mCount == 1);
		} catch (final Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test
	public void testPush() {
		final int val = mRand.nextInt();
		try {
			mProg = mCompiler.compile(String.format("push %s halt", val));
			mCpu.setListener(new SVMCpu.CpuListener() {
				
				@Override
				public boolean isInstructionInteresting(final IInstruction instruction) {
					return instruction.getCode() == ISA.Instructions.PUSH.getCode();
				}
				
				@Override
				public void onExecuted(final IInstruction fetched, final IStackBasedCpu cpu, final int ip) {
					mCount++;
					assertTrue(cpu.getStack().popWord() == val);
				}
				
			});
			mProg.execute(mCpu);
			assertTrue(mCount == 1);
		} catch (final Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test
	public void testStoreW() {
		final int addr = 50;
		final int val = mRand.nextInt();
		try {
			mProg = mCompiler.compile(String.format("push %s push %s sw halt", val, addr));
			mCpu.setListener(new SVMCpu.CpuListener() {
				
				@Override
				public boolean isInstructionInteresting(final IInstruction instruction) {
					return instruction.getCode() == ISA.Instructions.STOREW.getCode();
				}
				
				@Override
				public void onExecuted(final IInstruction fetched, final IStackBasedCpu cpu, final int ip) {
					mCount++;
					assertTrue(cpu.getPrimaryMemory().loadWord(addr) == val);
				}
				
			});
			mProg.execute(mCpu);
			assertTrue(mCount == 1);
		} catch (final Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test
	public void testSub() {
		final int val1 = mRand.nextInt();
		final int val2 = mRand.nextInt();
		try {
			mProg = mCompiler.compile(String.format("push %s push %s sub halt", val1, val2));
			mCpu.setListener(new SVMCpu.CpuListener() {
				
				@Override
				public boolean isInstructionInteresting(final IInstruction instruction) {
					return instruction.getCode() == ISA.Instructions.SUB.getCode();
				}
				
				@Override
				public void onExecuted(final IInstruction fetched, final IStackBasedCpu cpu, final int ip) {
					mCount++;
					assertTrue(cpu.getStack().peekWord() == val1 - val2);
				}
				
			});
			mProg.execute(mCpu);
			assertTrue(mCount == 1);
		} catch (final Exception ex) {
			fail(ex.getMessage());
		}
	}
	
}
