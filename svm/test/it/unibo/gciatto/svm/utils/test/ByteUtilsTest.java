/**
 *
 */
package it.unibo.gciatto.svm.utils.test;

import static it.unibo.gciatto.utils.ByteUtils.bytesToDoubleWord;
import static it.unibo.gciatto.utils.ByteUtils.bytesToHalfWord;
import static it.unibo.gciatto.utils.ByteUtils.bytesToWord;
import static it.unibo.gciatto.utils.ByteUtils.doubleWordToBytes;
import static it.unibo.gciatto.utils.ByteUtils.halfWordToBytes;
import static it.unibo.gciatto.utils.ByteUtils.wordToBytes;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Test;

/**
 * @author gciatto
 *
 */
public class ByteUtilsTest {
	final Random r = new Random();
	
	@Test
	public void testDoubleWord() {
		final long value = r.nextLong();
		assertTrue("Wrong W conversion", value == bytesToDoubleWord(doubleWordToBytes(value)));
	}
	
	@Test
	public void testHalfWord() {
		final short value = (short) r.nextInt();
		assertTrue("Wrong HW conversion", value == bytesToHalfWord(halfWordToBytes(value)));
	}
	
	@Test
	public void testWord() {
		final int value = r.nextInt();
		assertTrue("Wrong W conversion", value == bytesToWord(wordToBytes(value)));
	}
	
}
